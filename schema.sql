-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for debian-linux-gnueabihf (armv7l)
--
-- Host: localhost    Database: sirius
-- ------------------------------------------------------
-- Server version	10.1.26-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `TODO`
--

DROP TABLE IF EXISTS `TODO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TODO` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thing` text NOT NULL,
  `detail` text NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `controllers`
--

DROP TABLE IF EXISTS `controllers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `controllers` (
  `uuid` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `name` tinytext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='List of crontroller boards for devices';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_modes`
--

DROP TABLE IF EXISTS `device_modes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_modes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` tinytext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Possible device roles values';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_roles`
--

DROP TABLE IF EXISTS `device_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` tinytext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Possible device roles values';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_types`
--

DROP TABLE IF EXISTS `device_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` tinytext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Possible device roles values';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controller` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Human readable name of device',
  `type` int(11) NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `pin` int(11) NOT NULL DEFAULT '-1' COMMENT 'Concrete pin number on controller board',
  `role` int(11) NOT NULL COMMENT 'input/output/input_pullup',
  `mode` int(11) NOT NULL COMMENT 'Analog/pwm/digital',
  `active_value` int(11) NOT NULL DEFAULT '1' COMMENT 'Value on which is device active',
  `inactive_value` int(11) NOT NULL DEFAULT '0' COMMENT 'Value on which is the device shut off',
  `actual_value` int(11) NOT NULL DEFAULT '0' COMMENT 'Last known value of device ',
  PRIMARY KEY (`id`),
  KEY `FK_devices_controllers` (`controller`),
  KEY `FK_devices_device_modes` (`mode`),
  KEY `FK_devices_device_roles` (`role`),
  KEY `FK_devices_device_types` (`type`),
  CONSTRAINT `FK_devices_controllers` FOREIGN KEY (`controller`) REFERENCES `controllers` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `FK_devices_device_modes` FOREIGN KEY (`mode`) REFERENCES `device_modes` (`id`),
  CONSTRAINT `FK_devices_device_roles` FOREIGN KEY (`role`) REFERENCES `device_roles` (`id`),
  CONSTRAINT `FK_devices_device_types` FOREIGN KEY (`type`) REFERENCES `device_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Info about connected devices';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fsm`
--

DROP TABLE IF EXISTS `fsm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fsm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(2) DEFAULT 'cs',
  `class` varchar(50) DEFAULT NULL,
  `state` int(11) DEFAULT '0',
  `name` tinytext,
  `commands` tinytext,
  `next_state` int(11) DEFAULT '0',
  `end_state` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime DEFAULT NULL,
  `text` text COLLATE utf8_unicode_ci,
  `done` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `showed` tinytext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='list of all notifications';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sensors`
--

DROP TABLE IF EXISTS `sensors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) DEFAULT NULL,
  `name` text CHARACTER SET utf32 COMMENT 'place of sensor',
  `type` varchar(50) DEFAULT NULL COMMENT 'type of value (humidity,temp...)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sensor_id` (`uuid`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=906 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slovnik`
--

DROP TABLE IF EXISTS `slovnik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slovnik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `base` varchar(50) COLLATE utf32_unicode_ci DEFAULT NULL,
  `variation` text COLLATE utf32_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `base` (`base`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `telemetry_electricity`
--

DROP TABLE IF EXISTS `telemetry_electricity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telemetry_electricity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place` text,
  `data` float DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `uuid` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32 ROW_FORMAT=COMPACT COMMENT='delay value from electric measure';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `telemetry_heat_index`
--

DROP TABLE IF EXISTS `telemetry_heat_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telemetry_heat_index` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place` text,
  `data` float DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `uuid` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2244 DEFAULT CHARSET=utf32 ROW_FORMAT=COMPACT COMMENT='data z teplotnich senzoru ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `telemetry_humidity`
--

DROP TABLE IF EXISTS `telemetry_humidity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telemetry_humidity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place` text CHARACTER SET utf32,
  `data` float DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `uuid` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `telemetry_temperature`
--

DROP TABLE IF EXISTS `telemetry_temperature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telemetry_temperature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place` text,
  `data` float DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `uuid` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=190 DEFAULT CHARSET=utf32 ROW_FORMAT=COMPACT COMMENT='data z teplotnich senzoru ';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-20 23:53:30
