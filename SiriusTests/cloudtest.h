#ifndef CLOUDTEST_H
#define CLOUDTEST_H

#include <QObject>
#include "cloudactions.h"
#include <QTest>

class CloudTest : public QObject
{
    Q_OBJECT
public:
    explicit CloudTest(QObject *parent = nullptr);
    ~CloudTest();
    cloudActions *cloud;
signals:

private Q_SLOTS:
    void testOnline();
};

#endif // CLOUDTEST_H
