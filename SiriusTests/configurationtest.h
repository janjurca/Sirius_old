#ifndef CONFIGURATIONTEST_H
#define CONFIGURATIONTEST_H

#include <QObject>
#include <QTest>

class ConfigurationTest : public QObject
{
    Q_OBJECT
public:
    explicit ConfigurationTest(QObject *parent = nullptr);

signals:

public slots:
    void writeConf();
    void readConf();
};

#endif // CONFIGURATIONTEST_H
