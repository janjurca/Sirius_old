#include <QTest>
#include "cloudtest.h"

CloudTest::CloudTest(QObject *parent) : QObject(parent)
{
    cloud = new cloudActions();
}

CloudTest::~CloudTest(){
    delete cloud;
}

void CloudTest::testOnline(){
    QVERIFY(cloud->testNetworkConnect());
}
