#ifndef NETWORK_TESTS_H
#define NETWORK_TESTS_H

#include <QString>
#include <QtTest/QtTest>
#include <QCoreApplication>
#include <networkhandler.h>
#include "global.h"
#include <iostream>

using namespace std;
class NetworkTest : public QObject
{
    Q_OBJECT

public:
    NetworkTest();

private Q_SLOTS:
    void testTCPNetwork();
    void testTCPConnection();
    void testUDPrecieve();
};


#endif // NETWORK_TESTS_H
