#ifndef TEXTPARSERTEST_H
#define TEXTPARSERTEST_H

#include <QObject>
#include <QTest>

class TextParserTest : public QObject
{
    Q_OBJECT
public:
    explicit TextParserTest(QObject *parent = nullptr);

signals:

private Q_SLOTS:
    void dateTest();
    void timeTest();
    void dateTimeTest();

};

#endif // TEXTPARSERTEST_H
