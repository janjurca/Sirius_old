#include "textparsertest.h"
#include "textparser.h"

TextParserTest::TextParserTest(QObject *parent) : QObject(parent)
{

}

void TextParserTest::dateTest(){
    TextParser p;
    QDate date = QDate::fromString("28.5.2003", "dd.M.yyyy");
    QCOMPARE(p.getDate("28.5.2003"),date);
    QCOMPARE(p.getDate("28. 5. 2003"),date);
    QCOMPARE(p.getDate("   28. 5. 2003    "),date);
    QCOMPARE(p.getDate("ahoj 28. 5. 2003 neoin"),date);
    QCOMPARE(p.getDate("28.vfvf 5. 2003"),QDate());
    QCOMPARE(p.getDate("28 . 5 . 2003"),date);
    QCOMPARE(p.getDate("28 5 2003"),date);
    QCOMPARE(p.getDate("kklk28. 5. 2003fsfsfs"),date);
    QCOMPARE(p.getDate("Ahoj pridej notifikaci na 28. 5. 2003 ve 13:00 hodin"),date);

    date = QDate::fromString("8.11.2003", "d.MM.yyyy");
    QCOMPARE(p.getDate("8.11.2003"),date);
    QCOMPARE(p.getDate("8. 11. 2003"),date);
    QCOMPARE(p.getDate("   8. 11. 2003    "),date);
    QCOMPARE(p.getDate("ahoj 8. 11. 2003 neoin"),date);
    QCOMPARE(p.getDate("8.vfvf 11. 2003"),QDate());
    QCOMPARE(p.getDate("8 . 11 . 2003"),date);
    //QCOMPARE(p.getDate("8 11 2003"),date);
    QCOMPARE(p.getDate("kklk8. 11. 2003fsfsfs"),date);
}

void TextParserTest::timeTest(){
    TextParser p;
    QTime time = QTime::fromString("10:24", "hh:mm");
    QCOMPARE(p.getTime("10:24"),time);
    QCOMPARE(p.getTime("10 : 24"),time);
    QCOMPARE(p.getTime("10 :24"),time);
    QCOMPARE(p.getTime("10: 24"),time);
    QCOMPARE(p.getTime("csdcs10:24laca"),time);
    QCOMPARE(p.getTime("csdcs 10:24 laca"),time);
    QCOMPARE(p.getTime("csdcs 10 :24 laca"),time);
    QCOMPARE(p.getTime("csdcs 10: 24 laca"),time);

}

void TextParserTest::dateTimeTest(){
    TextParser p;
    QDateTime dt = QDateTime::fromString("8.11.2003 10:24", "d.MM.yyyy hh:mm");
    QCOMPARE(p.getDateTime("Ahoj pridej notifikaci na 8. 11. 2003 ve 10:24 hodin"),dt);

}
