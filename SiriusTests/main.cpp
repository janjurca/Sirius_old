#include <QCoreApplication>
#include "textparsertest.h"
#include "networktest.h"
#include "cloudtest.h"
#include "aestest.h"

#define ADD_TEST(className) {className tc;status |= QTest::qExec(&tc, argc, argv);}

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    //app.setAttribute(Qt::AA_Use96Dpi, true);

    int status = 0;

      //-- run all tests
    ADD_TEST(NetworkTest)
    ADD_TEST(CloudTest)
    ADD_TEST(AesTest)
    ADD_TEST(TextParserTest)


    return status;
}
