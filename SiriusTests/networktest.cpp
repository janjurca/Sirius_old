#include "networktest.h"
#include "../defines.h"

#define TESTMESSAGE "number 42"


NetworkTest::NetworkTest()
{
    network = new NetworkHandler(this);
    connect(network,SIGNAL(connected()),this,SLOT(testTCPConnection()));
    connect(network,SIGNAL(receivedInput(QString,int,QHostAddress)),this,SLOT(testUDPrecieve(QString,int,QHostAddress)));
    network->send(TESTMESSAGE,INGOING_CLIENT_PORT);
}

void NetworkTest::testTCPNetwork()
{
    network->startClient("localhost");


}

void NetworkTest::testTCPConnection(){
    network->waitForTcpConnection();
    QVERIFY(network->isConnected());
    disconnect(network,SIGNAL(connected()),this,SLOT(testTCPConnection()));
}

void NetworkTest::testUDPrecieve(){
    //TODO
}
//QTEST_MAIN(NetworkTest)
