#-------------------------------------------------
#
# Project created by QtCreator 2017-05-02T22:14:02
#
#-------------------------------------------------

QT       += testlib
QT -= gui quick qml
TARGET = tst_sirius_testcpp
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += \
    main.cpp \
    networktest.cpp \
    cloudtest.cpp \
    textparsertest.cpp \
    aestest.cpp \
    configurationtest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"
DEFINES += CLIENT=1

include(../include_libs.pri)



HEADERS += \
    networktest.h \
    cloudtest.h \
    textparsertest.h \
    aestest.h \
    configurationtest.h
