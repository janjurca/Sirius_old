#include "serialvisualise.h"
#include "ui_serialvisualise.h"


serialVisualise::serialVisualise(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::serialVisualise)
{
    ui->setupUi(this);


    window = new QMainWindow;
    serial = new QSerialPort(this);
    ui->serials->clear();
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
           QString coms = info.portName();
           qDebug() << coms;
           ui->serials->addItem(coms);
    }

    connect(serial, &QSerialPort::readyRead, this, &serialVisualise::readData);
    ui->rychlost->setValue(rychlost);
    ui->rozsahY->setValue(rozpeti/1000);
    ui->rozsahY_label->setText(QString::number(rozpeti));
    ui->rychlost_label->setText(QString::number(rychlost));
    QStringList bauds;
    bauds << "1200" << "2400" << "4800" << "9600" << "19200" << "38400" << "57600" << "115200";
    ui->baud->addItems(bauds);
    statusBar()->showMessage("Připraveno");
}

serialVisualise::~serialVisualise()
{
    disconnect();
    delete ui;
}



void serialVisualise::on_connect_clicked()
{
    serial->setPortName(ui->serials->currentText());
    if(ui->baud->currentText() == "1200")serial->setBaudRate(QSerialPort::Baud1200);
    if(ui->baud->currentText() == "2400")serial->setBaudRate(QSerialPort::Baud2400);
    if(ui->baud->currentText() == "4800")serial->setBaudRate(QSerialPort::Baud4800);
    if(ui->baud->currentText() == "9600")serial->setBaudRate(QSerialPort::Baud9600);
    if(ui->baud->currentText() == "19200")serial->setBaudRate(QSerialPort::Baud19200);
    if(ui->baud->currentText() == "38400")serial->setBaudRate(QSerialPort::Baud38400);
    if(ui->baud->currentText() == "57600")serial->setBaudRate(QSerialPort::Baud57600);
    if(ui->baud->currentText() == "115200")serial->setBaudRate(QSerialPort::Baud115200);

    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);
    if(!serial->open(QIODevice::ReadWrite)){
        QString error_serial = serial->errorString();
        qWarning() << error_serial;
        statusBar()->showMessage(error_serial);
        return;
    }else{

        chart->addSeries(series);
        //chart->setTitle("Zoom in/out example");
        chart->setAnimationOptions(QChart::NoAnimation);
        chart->legend()->hide();
        chart->createDefaultAxes();

        chartView->setRenderHint(QPainter::Antialiasing);

        window->setCentralWidget(chartView);
        window->resize(400, 300);
        window->grabGesture(Qt::PanGesture);
        window->grabGesture(Qt::PinchGesture);
        if(fullscreen)
            window->showFullScreen();
        else
            window->show();

        connect(window,SIGNAL(destroyed(QObject*)),this,SLOT(disconnect()));
    }
}

void serialVisualise::closeEvent(QCloseEvent *event){
    disconnect();
}

void serialVisualise::readData()
{
    ///Reading data from serial port
    QByteArray requestData = serial->readAll();
    while (serial->waitForReadyRead(1)){
        requestData += serial->readAll();///wait for full message
    }
    qreal y = QString(requestData).trimmed().toDouble();
    QPointF p((qreal) x, y);
    series->append(p);
    x += rychlost;

    if(y > max_value)
        max_value = y + y/10;
    chart->axisY()->setRange(QVariant(0),QVariant(max_value));
    chart->axisX()->setRange(QVariant(x - (rozpeti / 2)), QVariant(x + (rozpeti / 2)));
    //TODO solve the ranges
}

void serialVisualise::on_refresh_clicked()
{
    ui->serials->clear();
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
           QString coms = info.portName();
           ui->serials->addItem(coms);
    }
}

void serialVisualise::on_rozsahY_actionTriggered(int action)
{
    (void)action;
    rozpeti = ui->rozsahY->value() * 1000;
    ui->rozsahY_label->setText(QString::number(rozpeti));
}

void serialVisualise::on_rychlost_actionTriggered(int action)
{
    (void)action;
    rychlost = ui->rychlost->value();
    ui->rychlost_label->setText(QString::number(rychlost));
}

void serialVisualise::on_disconnect_clicked()
{
    disconnect();
}

void serialVisualise::disconnect(){
    window->close();
    serial->flush();
    serial->close();
    series->clear();
    x = 0;
}

void serialVisualise::on_checkBox_clicked(bool checked)
{
    fullscreen = checked;
    if(checked && window->isVisible())
        window->showFullScreen();
    else if(!checked && window->isVisible())
        window->showNormal();
}

void serialVisualise::command(QString command){
    if(db != NULL && db->isOpen()){
        ///the value of operation what would be done
        int state = lang->findCommandstate(command,"serialVisualise");

        switch (state) {
        case 0:
            on_checkBox_clicked(true);
            break;
        case 1:
            on_checkBox_clicked(false);
            break;
        default:
            break;
        }
    }
}
