#ifndef SERIALVISUALISE_H
#define SERIALVISUALISE_H

#include <QMainWindow>
#include <QCloseEvent>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include "chart.h"
#include "chartview.h"
#include "databasehandling.h"
#include <QList>
#include <QColor>
#include <QCloseEvent>
#include <QtCore/QtMath>
#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>
#include <QtCharts>
#include "global.h"

namespace Ui {
class serialVisualise;
}

class serialVisualise : public QMainWindow
{
    Q_OBJECT

public:
    explicit serialVisualise(QWidget *parent = 0);
    ~serialVisualise();
    void closeEvent(QCloseEvent *event);
    void command(QString command);

private:
    Ui::serialVisualise *ui;

    QSplineSeries *series = new QSplineSeries();
    Chart *chart = new Chart();
    ChartView *chartView = new ChartView(chart);
    QMainWindow *window;
    qreal max_value = 0;

    int x = 0;
    QSerialPort *serial;
    bool fullscreen = false;
    int rozpeti = 5000;
    int rychlost = 50;

private slots:
    void on_connect_clicked();

    void on_refresh_clicked();

    void readData();

    void on_rozsahY_actionTriggered(int action);

    void on_rychlost_actionTriggered(int action);

    void on_disconnect_clicked();

    void on_checkBox_clicked(bool checked);

public slots:
    void disconnect();

};

#endif // SERIALVISUALISE_H
