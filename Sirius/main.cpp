#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "sirius.h"
#include <QApplication>
#include <QQmlContext>
#include <iostream>
#include <QtWebView/QtWebView>
#include <QtCore/QUrl>
#include <QtCore/QCommandLineOption>
#include <QtCore/QCommandLineParser>
#include <QGuiApplication>
#include <QStyleHints>
#include <QScreen>
#include <QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtWebView/QtWebView>
#include "global.h"
#include "world_controll/device.h"
#include "world_controll/controllerboard.h"

QQmlApplicationEngine *engine;

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QtWebView::initialize();
    engine = new QQmlApplicationEngine();



    Sirius *sirius = new Sirius;
    { // connect this class with qml
        QQmlContext *context = engine->rootContext();
        context->setContextProperty("filebasics",&filebasics);
    }
    { // connect this class with qml
        QQmlContext *context = engine->rootContext();
        context->setContextProperty("network", network);
    }
    devices = new Devices();
    { // connect this class with qml
        QQmlContext *context = engine->rootContext();
        context->setContextProperty("DevicesControll", devices);
    }
    qmlRegisterType<Device>("worldcontroll.device", 1, 0, "Device");
    qmlRegisterType<ControllerBoard>("worldcontroll.controller", 1, 0, "Controller");
    qmlRegisterSingletonType( QUrl("qrc:/ui/Globals.qml"), "main.globals", 1, 0, "Globals" );

    //engine->addImportPath("qrc:/ui/components/notifications/");
    engine->load(QUrl(QLatin1String("qrc:/ui/main.qml")));
    printing.readyToPrint();

    return app.exec();
}
