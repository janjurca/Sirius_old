#include "notification.h"


Notification::Notification()
{
}

/**
 * Add ne notification to db (id is uniq to table)
 * @brief Notification::addNotification
 * @param time
 * @param text
 * @return return true if db query were pushed
 */
bool Notification::addNotification(QDateTime time,QString text){
    return db->insertRow("notifications",
                  QStringList() << "datetime" << "text",
                  QStringList() << time.toString("yyyy-MM-dd hh:mm:ss") << text);
}

/**
 * Checks db for new notifications to notify
 * @brief Notification::checkNotifications
 */
void Notification::checkNotifications(){
    QTextStream(stdout) <<"checking for notifications" << endl;
    QList < QStringList > data =  db->readValue("notifications",QStringList() << "id" << "datetime" << "text","done" ,"");
    //qDebug() << data.size();
    for(int i = 0; i < data.size(); i++){
        QDateTime date = QDateTime::fromString(data.at(i).at(1),"yyyy-MM-ddThh:mm:ss");
        //qDebug() << data.at(i).at(1) << " datum " << date.toString("yyyy-MM-dd hh:mm:ss");
        if(date < QDateTime::currentDateTime()){
            emit notify(data.at(i).at(2),data.at(i).at(0));
        }
    }

}

/**
 * Start notification thread that controls new notification in db
 * @brief Notification::run
 */
void Notification::run(){
    timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(checkNotifications()));
    timer->setInterval(60000);

    timer->start();
    QTextStream(stdout) <<"timer for notification started" << endl;
    checkNotifications();
    QThread::exec();
}

/**
 * Marks notification in db as done
 * @brief Notification::notifyDone
 * @param id notification id
 * @return if db opearation were ok
 */
bool Notification::notifyDone(QString id){
    return db->updateRow("notifications","done","OK",id);
}

/**
 * In db change notifiacation time of notification with "id" default suspend is 3 hrs
 * @brief Notification::suspendNotification
 * @param id notif id in db
 * @param newTime new time of notification
 * @return if db operation were ok
 */
bool Notification::suspendNotification(QString id, QDateTime newTime){
    QDateTime date = QDateTime::currentDateTime();
    date = date.addSecs(10800);
    return db->updateRow("notifications",
                         "datetime",
                         date.toString("yyyy-MM-dd hh:mm:ss"),
                         "id",
                         id);
}
