#ifndef CUSTOMTIMER_H
#define CUSTOMTIMER_H


#include <QtCore>
#include <QTimer>
#include <QObject>
#include <QLabel>
#include "global.h"
#include <QDateTime>

class CustomTimer :  public QObject
{
    Q_OBJECT
public:
    CustomTimer(QDateTime time, QObject *parent = 0);
    ~CustomTimer();

private:
    QTimer *casovac;
    QDateTime *notifTime;

public slots:
    void update();

signals:
    void timeout(void);
    void message(QString);

};

#endif // CUSTOMTIMER_H
