QT += quick qml core multimedia network sql gui widgets serialport printsupport webview svg xml charts


QMAKE_CXXFLAGS+= -std=c++11
QMAKE_LFLAGS +=  -std=c++11

CONFIG += c++14 qml_debug declarative_debug
RC_FILE = Sirius.rc
DEFINES += CLIENT=1
SOURCES += main.cpp \
    visualisations/chart.cpp \
    visualisations/chartview.cpp \
    visualisations/serialvisualise.cpp \
    customtimer.cpp \
    sirius.cpp \
    notification.cpp

RESOURCES += qml.qrc\
            media.qrc


# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    visualisations/chart.h \
    visualisations/chartview.h \
    visualisations/serialvisualise.h \
    customtimer.h \
    sirius.h \
    notification.h

FORMS += \
    visualisations/serialvisualise.ui

contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
ANDROID_EXTRA_LIBS = \
    # modify the path
    $$PWD/libmariadb.so
}


include(../include_libs.pri)

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android


#VARIABLES
isEmpty(PREFIX) {
PREFIX = /usr
}
BINDIR = $$PREFIX/bin
DATADIR =$$PREFIX/share

#MAKE INSTALL
target.path = $$BINDIR

desktop.path = $$DATADIR/applications
desktop.files = $${TARGET}.desktop
INSTALLS += desktop

pixmap.path = $$DATADIR/pixmaps
pixmap.files = ../Logo/Sirius.ico
INSTALLS += pixmap


INSTALLS += target desktop
