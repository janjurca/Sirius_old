#ifndef NOTIFICATION_H
#define NOTIFICATION_H

#include <QObject>
#include <QThread>
#include "databasehandling.h"
#include <QDateTime>
#include "../defines.h"
#include <QTimer>
#include <QMessageBox>
#include <QAbstractButton>

extern DatabaseHandling *db;

class Notification : public QThread
{
    Q_OBJECT
public:
    explicit Notification();

signals:
    void notify(QString message,QString id);
private:
    QTimer *timer = NULL;

private slots:
    void checkNotifications();
public slots:
    bool addNotification(QDateTime time, QString text);
    bool notifyDone(QString id);
    bool suspendNotification(QString id, QDateTime newTime = QDateTime::currentDateTime());
protected:
    void run();
};

#endif // NOTIFICATION_H
