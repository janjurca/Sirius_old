#include "sirius.h"
#include <QMediaPlayer>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickView>
#include <iostream>
#include "youtubeapi.h"
#include <QFileDialog>
#include <textparser.h>
#include <QVector>
#include "defines.h"
extern QQmlApplicationEngine *engine;

Sirius::Sirius() {

    config = new Configuration(WORK_DIR + "sirius.json"); //TODO make the configuration cyphered
    try {
        config->load();
    } catch (const std::runtime_error& e) {
        printing.print(QString(e.what()));
    }
    { // connect this class with qml
        QQmlContext *context = engine->rootContext();
        context->setContextProperty("configClass", config);
    }

    { // connect this class with qml
        QQmlContext *context = engine->rootContext();
        context->setContextProperty("siriusClass", this);
    }
    locker = new QTimer(this);
    connect(locker,SIGNAL(timeout()),this,SLOT(lockState()));
    lang = new Language();


    network = new NetworkHandler(this);
    QQmlContext *context_net = engine->rootContext();
    context_net->setContextProperty("netClass", network);
    network->startClient(config->getServerIp());


    db = new DatabaseHandling(config->getDBAdress(),config->getDBUserName(),config->getDBUserName(),config->getDBPassword());

    systemCont = new systemControll;

    musicmixer = new musicMixer(this,config->getMusicDir());
    locker->start(LOCK_TIME);
    notif = new Notification();
    {
        QQmlContext *context = engine->rootContext();
        context->setContextProperty("notifClass", notif);
        notif->start();
    }

    smtp = new SmtpClient(config->getSmtpServer(), config->getSmtpPort(), SmtpClient::SslConnection);
    smtp->setUser(config->getMailUsername());
    smtp->setPassword(config->getMailPassword());



    printing.print("Zdravím Vás pane");
    connect(&printing,SIGNAL(userPrint(QString)),this,SLOT(sayMe(QString)));


}

Sirius::~Sirius()
{
    //TODO clean it
}


void Sirius::processInput(QString vstup){
    vstup = vstup.toLower();
    inputProc(vstup);
    emit stateChange(stav);
}

void Sirius::inputProc(QString vstup){
    vstup = vstup.trimmed();
    vstup = vstup + " ";
    currentSentence += vstup + " ";
    locker->setInterval(LOCK_TIME);
    QStringList input = vstup.split(" ");


    if(continuous == 0){
        for(int i = 0; i < input.size(); i++){
            if(input[i] == "konec"){//TODO
                lockState();
                break;
            }
            //-------------------komunikace s jiz spustenymi procesy
            if(musicmixer != NULL)
                if(musicmixer->isActive())
                    musicmixer->command(input.at(i));
            if(vizualizace != NULL)
                if(vizualizace->isVisible())
                    vizualizace->command(input.at(i));
            //------------------------------------------------------
            QString prikaz;
            if(stav < 10000)
                stav = lang->findNextState(input.at(i),stav);
            else if(stav >= 10000){
                for(int j = i; j < input.size(); j++){
                    if(input.at(j) != "s" && input.at(j) != "nazvem" && input.at(j) != "názvem"){//TODO zavest blacklist slov bez duleziteho vyznamu
                        prikaz += input.at(j);
                        if(j != (input.size()-1)){ // pokud jsou jeste nejaka slova dej mezi ne mezeru
                            prikaz += " ";
                        }
                    }
                }
                runStatus(prikaz);
                break;
            }
        }
    }else{
        runStatus(vstup);
    }
}

void Sirius::runStatus(QString input){
    qWarning() << "continuos" <<continuous << "stav" << stav << "input" << input;

    switch(stav){
    case 10000:{ //hledej na netu
        if(input != ""){
            emit showWebPage("http://www.google.com/search?q=" + input);
            //QString link = "http://www.google.com/search?q=" + input + "";
            //QDesktopServices::openUrl(QUrl(link));
            reset_fsm();
        }
    }
        break;
    case 10001:

        break;
    case 10002:

        break;
    case 10003:

        break;
    case 1004:{ // vytvor novy projekt
        QString command = NULL;
        qDebug() << "104" << continuous << stav << input;
        if(continuous == 0){
            input = input.replace(" ", "_");
            command = "mkdir " + config->getProjectDir() + "/"+ input;
            system(qPrintable(command));
            command = "touch " + config->getProjectDir() + "/"+ input + "/poznamky_" + input + ".txt";

            system(qPrintable(command));
            askUser("Jaký typ projektu?",input);
        }else if(continuous == 1){
            if(input.contains("arduino")){
                QTextStream(stdout) << "zapinam arduino IDE" << endl;
                command = "";
                command += "echo '//author: Jan Jurča, created by: Sirius\n"
                           "void setup() {  \n"
                           "\n"
                           "}\n"
                           "void loop() {\n"
                           "}' > "+ config->getProjectDir() + "/"+ inputAskUser + "/" + inputAskUser + ".ino";
                system(qPrintable(command));
                command = "arduino " + config->getProjectDir() + "/"+ inputAskUser + "/" + inputAskUser + ".ino";
                system(qPrintable(command));
                continuous = 0;
                reset_fsm();
            }
        }
    }
        break;
    case 10005:{ // hudebni mixer
        sayMe("spoustim music mixer");
        musicmixer->mixMusic(2);
        //emit mixMusic(2); //TODO make a research about calling qthread with methods vs signals
        reset_fsm();
        break;
    }
    case 10006:{ // nastaveni casovace
        int input_cas = 0;
        QStringList sentenceList = currentSentence.split(" ");
        for(int i = 0; i <  sentenceList.size(); i++ ){
            input_cas += QString(sentenceList.at(i)).toInt();
        }

        if( input == "" || input_cas  < 1 ){ //bude se ptát tak dlouho dokud nezíská platný udaj o case
            askUser("Na jak dlouho?",input);
            continuous = 1;
        }else{
            timerList << new CustomTimer(QDateTime(QDateTime::currentDateTime()).addSecs(input_cas*60),this); //TODO
            printing.print("Časovač nastaven na "+ QString::number(input_cas) + " minut.");
            reset_fsm();
            continuous = 0;
        }
        break;
    }
    case 10007:{ // tvorba videa
        reset_fsm();
        break;
    }

    case 10008:{ // spuštění vizualizace
#ifdef DESKTOP
        if( vizualizace != NULL)
            delete vizualizace;
        vizualizace = new serialVisualise();

        if(!vizualizace->isVisible())
            vizualizace->show();
#else
        printing.print(tr("Nepodporovaná funkce na tomto zařízeni"));
#endif
        reset_fsm();
        break;
    }
    case 10009:{ // vypnout pocitac
        reset_fsm();
        systemCont->shutDown(); //TODO make it for android too
        break;
    }

    case 10010:{ // uspat pocitac
        reset_fsm();
        systemCont->sleepMode();//TODO make it for android too
        break;
    }
    case 10011:{ // pridej poznamku
        if(continuous == 0){
            poznamka = "\n";
            QDateTime datumPoznamky = QDateTime::currentDateTime();
            poznamka += datumPoznamky.toString();
            poznamka += "\n";
            poznamka += input;
            continuous = 1;
            printing.print("Přidávám poznámku:");
            printing.print(poznamka);

        }else{
            poznamka += " ";
            poznamka += input;
            printing.print("Pokračuji v poznámce");
            printing.print(input);
            QStringList inputList = input.split(" ");
            for(int i = 0; i < inputList.size(); i++){
                if(inputList[i] == "konec"){
                    QFile notes(config->getNotesFile());
                    if(notes.open(QIODevice::Append | QIODevice::Text)){
                        QTextStream stream(&notes);
                        stream << poznamka;
                        notes.close();
                    }
                    printing.print("Ukončuji poznámku");
                    reset_fsm();
                    continuous = 0;
                }
            }
        }
        break;
    }
    case 10012:{ // aktualizuj se
        reset_fsm();
        break;
    }
    case 10013:{ // Otevři projekt
        reset_fsm();
        break;
    }
    case 10014:{ // Otevři konsoli
        reset_fsm();
        QProcess *process = new QProcess;
        process->start(config->getConsoleDir());
        if(!process->waitForStarted()){
            printing.print("nepodařilo se spustit konzoli");
        }
        break;
    }
    case 10015:{ //reboot pc
        reset_fsm();
        systemCont->reboot();
        break;
    }
    case 10016:{ //aktualizuj db hudby
        reset_fsm();
        //musicmixer->aktualizaceMusicDB();
        break;
    }
    case 10017:{ //otevri poznamky
        emit showFile(config->getNotesFile());
        reset_fsm();
        break;
    }
    case 10018:{ //otevri port a poslouchej na nem
        if(input.toInt() != 0){
            network->listenOnPort(input.toInt());
            emit showFile("udp://"+QString::number(input.toInt()));
            reset_fsm();
        } else {
            askUser("Jaký port?",input);
        }
        break;
    }
    case 10019:{ // poslat email
        static QString subject;
        static QString mail_content;
        static QString recipient;

        if(continuous == 0){
            askUser("Jaký předmět:",input);
        }
        else if(continuous == 1){
            subject = input;
            askUser("Text zprávy:",input);
        }
        else if(continuous == 2){
            mail_content = input;
            askUser("Komu:",input);

        } else if (continuous == 3){
            recipient = input;
            MimeMessage message;
            message.setSender(new EmailAddress(config->getMailUsername(), "My name")); // TODO add my name to config file
            message.addRecipient(new EmailAddress(recipient));
            message.setSubject(subject);
            MimeText text;
            text.setText(mail_content + "\n");
            message.addPart(&text);

            if(!smtp->connectToHost())
                printing.print("Can't connect to mail host");
            if(!smtp->login())
                printing.print("Cant login to  mail");
            if(!smtp->sendMail(message))
                printing.print("Cant send mail");
            smtp->quit();
            reset_fsm();
        }//TODO udelej skript ketry stahne kontaktní knížku z googlu a nahodí ji do mé databáze ze které se potom budou vybírat kontakty pro siriuse jak prosté a geniální že :-)
        break;
    }
    case 10020:{ // pridej notifikaci
        static QString notifText = "";
        if(continuous == 0){
            askUser("Jaky je obsah upozorneni?",input);
            notifText = "";
        } else if(continuous == 1) {
            askUser("A kdy?",input);
            notifText = input;
        } else if (continuous == 5){
            QDateTime date = QDateTime::currentDateTime();
            date = date.addDays(1);
            QTime time = QTime::fromString("08:00","hh:mm");
            date.setTime(time);
            printing.print("Notifikace nastavena na "+ date.toString());
            notif->addNotification(date,notifText);
            reset_fsm();
        } else {
            QTime time = QTime::currentTime().addSecs(3600);
            QDateTime date = QDateTime::currentDateTime();

            if (TextParser::getTime(input) != QTime()) {
                time = TextParser::getTime(input);
                date.setTime(time);
            }
            if(TextParser::getDate(input) != QDate()){
                date.setDate(TextParser::getDate(input));
            }
            notif->addNotification(date,notifText);
        }
        break;
    }
    case 10021:{ // aktualizuj konfiguracni soubor
        printing.print(input);
        input = input.trimmed();
        if(continuous == 0){
            askUser("Jaka je cesta ke consoli?",input,"folder");
        } else if(continuous == 1) {
            config->setConsoleDir(input);
            askUser("IP adresa DB serveru:",input);
        } else if(continuous == 2) {
            config->setDBAdress(input);
            askUser("Heslo k DB:",input);
        } else if(continuous == 3) {
            config->setDBPassword(input);
            askUser("Uzivatelske jmeno k DB:",input);
        } else if(continuous == 4) {
            config->setDBUserName(input);
            askUser("Mailove heslo:",input);
        } else if(continuous == 5) {
            config->setMailPassword(input);
            askUser("Mailova adresa:",input);
        } else if(continuous == 6) {
            config->setMailUsername(input);
            askUser("Slozka s hudbou:",input,"folder");
        } else if(continuous == 7) {
            config->setMusicDir(input);
            askUser("Soubor s poznamkami:",input);
        } else if(continuous == 8) {
            config->setNotesFile(input);
            askUser("Slozka s projekty:",input,"folder");
        } else if(continuous == 9) {
            config->setProjectDir(input);
            askUser("SMTP server:",input);
        } else if(continuous == 10) {
            config->setSmtpServer(input);
            askUser("SMTP port:",input);
        } else if(continuous == 11) {
            config->setSmtpPort(input.toInt());
            if(config->saveConfiguration())
                printing.print("Configuration succesfuly saved");
            reset_fsm();
        }
        break;
    }
    case 10022:{ // otevri soubor
        emit showFile(QFileDialog::getOpenFileName(nullptr,tr("Open file"), QDir::homePath()));
        reset_fsm();
        break;
    }
    default:{
        printing.print("Stav " + QString::number(stav) + " nenalezen");
        break;
    }
    }
}

void Sirius::askUser(QString dotaz, QString input, QString type){
    printing.print(dotaz);
    emit s_askUser(dotaz,type);
    continuous++;
    inputAskUser = input;
}

void Sirius::lockState(){
    if(stav != 0){
        printing.print("Zamčeno");
        stav = 0;
    }
    reset_fsm();
}

void Sirius::sayMe(QString text){
    QTextStream(stdout) << __FUNCTION__ << ": " << text << endl;
    emit messageOutput(text);
}

bool Sirius::fileExists(QString path) {
    QFileInfo check_file(path);
    return check_file.exists() && check_file.isFile();
}

void Sirius::reset_fsm(){
    this->stav = 1;
    this->currentSentence = "";
    this->continuous = 0;
}

void Sirius::sayMe(QUrl url){
    sayMe(url.toString());
}

