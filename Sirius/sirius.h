#ifndef SIRIUS_H
#define SIRIUS_H

#include <QObject>
#include <QtNetwork>
#include "speechrecognition.h"
#include "networkhandler.h"
#include "databasehandling.h"
#include "media/mediaplayer.h"
#include "media/musicmixer.h"
#include "customtimer.h"
#include "visualisations/serialvisualise.h"
#include "system/systemcontroll.h"
#include "../defines.h"
#include <QMovie>
#include <iostream>
#include <QTimer>
#include <QList>
#include <QLabel>
#include <QDir>
#include <QtCore/QtMath>
#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>
#include "notification.h"
#include "global.h"
#include "language.h"
#include "QFileDialog"


QT_CHARTS_USE_NAMESPACE
using namespace std;

class Sirius : public QObject
{
    Q_OBJECT

public:
    explicit Sirius();
    ~Sirius();
    bool fileExists(QString path);

    int stav = 0;
    int continuous = 0;
    QString vstup;
    string vstup_std, vstup_std_ref;
    QString poznamka;

private:
    QList< Chart* > charts;
    QList< ChartView* > chartViews;

    Notification *notif = NULL;
    QString currentSentence;

    musicMixer *musicmixer = NULL;
    serialVisualise *vizualizace = NULL;
    systemControll *systemCont = NULL;

    QTimer *locker = NULL;
    QString inputAskUser;
    QList< CustomTimer* >  timerList;



public slots:
    void processInput(QString vstup);
    void lockState();
    void sayMe(QString text);
    void sayMe(QUrl url);

private slots:
    void inputProc(QString vstup);
    void runStatus(QString input);
    void askUser(QString dotaz, QString input,QString type = "string");
    void reset_fsm();

signals:
    void mixMusic(int count);
    void stateChange(int state);
    void messageOutput(QString msg);
    void s_askUser(QString dotaz,QString type);
    void showFile(QString filename);
    void showWebPage(QString link);
};

#endif // SIRIUS_H
