#include "customtimer.h"
#include <QMediaPlayer>
#include <QFile>
#include <QMessageBox>

CustomTimer::CustomTimer(QDateTime time, QObject *parent) : QObject(parent)
{
    notifTime = new QDateTime(time);
    casovac = new QTimer(this);
    connect(casovac,SIGNAL(timeout()),this,SLOT(update()));
    casovac->start(10000);
    emit message("Timer nastaven na " + notifTime->toString("dd.MM.yyyy hh:mm:ss") + " minut");

}

CustomTimer::~CustomTimer(){
    delete casovac;
    delete notifTime;
}

void CustomTimer::update(){
    if(QDateTime::currentDateTime() > *notifTime){
        emit message("Vypršel čas timeru");
        emit timeout();
        casovac->stop();
        QMediaPlayer *player = new QMediaPlayer;
        player->setMedia(QUrl("qrc:/audio/resources/media/audio/notif/timerNotif.mp3"));
        player->setVolume(100);
        player->play(); //FIXME memory leak jako prase
        player->deleteLater();
#ifdef DESKTOP
        QMessageBox msgBox;
        msgBox.setText("Vypršel časovač");
        msgBox.exec();
#endif
    }
}
