import QtQuick 2.4
import Qt.labs.platform 1.0

TextEditorForm {
    id:t1
    property int udpPort: 0 //if 0 then udp is disabled

    Connections{
        target: network
        onReceivedInput: {
            console.error(port);
            console.error(data);
            if(port == udpPort){
                textArea.text = data + "\n" +textArea.text;
            }
        }
    }

    function readFile(filename){
        var file = filename;
        console.error(file)
        if(filename.indexOf("udp://") !== -1){
            udpPort = filename.split("udp://")[1]
            textArea.readOnly = true; 
            textArea.text=""
        }else{
            textArea.text = filebasics.readFile(filename);
        }
    }


    save.onClicked: {
        filebasics.saveFile(file,textArea.text);
    }
    load.onClicked: {
        fileDialog.open();
    }

    FileDialog {
        id: fileDialog
        currentFile: document.source
        folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
        onAccepted: {
            file = fileDialog.file;
            readFile(file);
        }
    }

}
