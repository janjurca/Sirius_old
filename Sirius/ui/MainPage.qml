import QtQuick 2.7
import Qt.labs.platform 1.0
import "components/system/filedialog" as FP
import "componentCreation.js" as ComponentCreation
import main.globals 1.0

MainPageForm {
    property var fileDialogObject: 0

    Connections{
        target: siriusClass
        onStateChange: {
            stateLabel.text = state;
        }
        onMessageOutput: {
            outputArea.text = msg + "\n" + outputArea.text;
        }
        onS_askUser: {
            input.placeholderText = dotaz
            if(type == "folder"){
                folder.visible = true
            } else {
                folder.visible = false
            }
        }
    }


    function fileDirSelected(itemName){
        if(fileDialogObject != 0){
            input.text = itemName.replace("file:///","/")
            fileDialogObject.destroy()
            fileDialogObject = 0
        }

    }


    folderSelect.onClicked: {
        var object = ComponentCreation.createComponent("components/system/filedialog/FileDialog.qml",
                                                       this,function(object){
                                                           fileDialogObject = object;
                                                           object.fileSelected.connect(fileDirSelected)
                                                       } )
    }


    Connections {
        target: netClass
        onConnected:{
            connectionStatus.source = "qrc:/img/resources/media/images/check.svg";
            connect.enabled = true;
        }
        onDisconnected:{
            connectionStatus.source = "qrc:/img/resources/media/images/uncheck.svg";
            connect.enabled = false;
        }
    }
    connect.onClicked: {
        netClass.startClient();
    }


    send.onClicked: {
        processInput();
    }
    Keys.onPressed: {
        if(event.key == Qt.Key_Enter || event.key == Qt.Key_Return ){
            processInput();
        }
    }
    function processInput() {
        input.placeholderText = qsTr("Řekni mi něco")
        var t = input.text;
        input.text = "";
        siriusClass.processInput(t);
    }
    generalFunctions.model: ListModel {
        ListElement {
            name: "Devices"
            componentFile: "./components/Sroom/Devices.qml"
        }
        ListElement {
            name: "Weather"
            componentFile: "Weather"
        }
        ListElement {
            name: "Config"
            componentFile: "./components/configuration/Configuration.qml"
        }
        ListElement {
            name: "Web"
            componentFile: "./components/web/Web.qml"
        }

    }
    generalFunctions.delegate: Item {
        width: 80
        height: 40
        Rectangle {
            anchors.fill: parent
            color: "lightgrey"
            Text {
                text: name
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
            }
            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor;
                onClicked: {
                    var object = Globals.mainSwipeView.addPage(componentFile)
                    Globals.mainSwipeView.focusPage(object)
                }
            }
        }
    }
}
