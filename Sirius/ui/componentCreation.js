
function createComponent(templateName, parent, func) {
    var component = Qt.createComponent(templateName);
    if (component.status == Component.Ready){
       var object = component.createObject(parent);
       if (object != null) {
           func(object);
           return object
       } else {
           console.error("Error creating object");
       }
    }else{
        component.statusChanged.connect(function finishCreation() {
            if (component.status == Component.Ready) {
                   var object = component.createObject(parent);
                   if (object != null) {
            func(object);
                   } else {
                       console.error("Error creating object");
                   }
               } else if (component.status == Component.Error) {
                   console.error("Error loading component:", component.errorString());
               }
        });
    }
}



