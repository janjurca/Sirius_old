import QtQuick 2.4
import QtQuick.Controls 2.0

Item {
    width: 400
    height: 400
    property alias pokoj_svetlo: pokoj_svetlo

    SwitchDelegate {
        id: pokoj_svetlo
        x: 22
        y: 34
        text: qsTr("Pokoj světlo")
    }
}
