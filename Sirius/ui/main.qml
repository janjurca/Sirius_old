import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import "componentCreation.js" as ComponentCreation
import "components/Sroom" as Sroom
import "./utils.js" as Utils
import "components/music" as MP
import main.globals 1.0

ApplicationWindow {
    id: app
    visible: true
    width: 1000
    height: 700
    title: qsTr("Sirius")

    Component.onCompleted: {
        Globals.mainSwipeView = swipeView
        Globals.mainRoot = app
    }

    function openWeb(link) {
        var object = ComponentCreation.createComponent(
                    "./components/web/Web.qml", this, swipeView.addPage)
        object.setUrl(link)
    }

    Connections {
        target: siriusClass
        onShowFile: {
            var object = ComponentCreation.createComponent("TextEditor.qml",
                                                           this,
                                                           swipeView.addPage)
            object.readFile(filename)
        }
        onShowWebPage: {
            openWeb(link)
        }
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        function toolUpdate() {
            try {
                var tools = swipeView.currentItem.getTools()
                tools.forEach(function (tool) {
                    toolsModel.append({
                                          image: tool[0],
                                          toolid: tool[1]
                                      })
                })
            } catch (err) {


                // Handle error(s) here
            }
        }

        onCurrentIndexChanged: {
            swipeView.currentIndex = currentIndex
            if (swipeView.currentIndex == 0) {
                close.visible = false
            } else {
                close.visible = true
            }
            toolsModel.clear()
            toolUpdate()
            try {
                swipeView.currentItem.toolChanged.connect(toolUpdate)
            } catch (err) {


                //Signal for update doesnt exists on current page object
            }
            try {
                swipeView.currentItem.addPage.connect(swipeView.addPageHandler)
            } catch (err) {

            }
        }
        MainPage {
        }

        Sroom.Devices {
        }


        function focusPage(page) {
            for (var i = 0; i < count; i++) {
                if (page === itemAt(i)) {
                    currentIndex = i
                }
            }
        }

        function addPage(page) {
            var component = Qt.createComponent(page)
            var object = component.createObject(swipeView)
            return object
        }

            function removePage(page) {
                for (var n = 0; n < count; n++) {
                    if (page === itemAt(n)) {
                        removeItem(n)
                    }
                }
                page.visible = false
            }
        }

    footer: RowLayout {
        PageIndicator {
            id: indicator
            count: swipeView.count
            currentIndex: swipeView.currentIndex
            anchors.bottom: parent.bottom
        }
        ListView {
            id: toolbar
            spacing: 10
            orientation: ListView.Horizontal
            flickableDirection: Flickable.HorizontalFlick
            Layout.fillHeight: true
            Layout.fillWidth: true

            layoutDirection: Qt.RightToLeft
            delegate: Rectangle {
                width: 35
                height: 35
                Image {
                    anchors.fill: parent
                    source: image
                    antialiasing: true
                }
                MouseArea {
                    anchors.fill: parent
                    property int toolID: toolid
                    onClicked: {
                        switch (toolID) {
                        case 1:
                            swipeView.currentItem.tool1()
                            break
                        case 2:
                            swipeView.currentItem.tool2()
                            break
                        case 3:
                            swipeView.currentItem.tool3()
                            break
                        case 4:
                            swipeView.currentItem.tool4()
                            break
                        }
                    }
                }
            }
            model: ListModel {
                id: toolsModel
            }
        }

        Rectangle {
            id: close
            width: 35
            height: 35
            visible: false

            scale: visible ? 1.0 : 0.1
            Behavior on scale {
                NumberAnimation {
                    duration: 90
                    easing.type: Easing.bezierCurve
                }
            }

            Image {
                anchors.fill: parent
                source: "qrc:/img/resources/media/images/close.svg"
                antialiasing: true
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    var object = swipeView.currentItem
                    object.destroy()
                }
            }
        }
    }
}
