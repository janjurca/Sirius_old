import QtQuick 2.4
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    width: 400
    height: 400
    property alias textArea: textArea
    property alias save: save
    property alias load: load
    property var file: ""

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent

        Rectangle {
            color: "#00000000"
            border.color: "#00000000"
            Layout.fillHeight: true
            Layout.fillWidth: true
            clip: true
            ScrollView {
                id: scrollView
                anchors.fill: parent

                TextArea {
                    id: textArea
                    text: qsTr("Text Area")
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                    anchors.fill: parent
                }
            }
        }

        RowLayout {
            id: rowLayout
            width: 100
            height: 100
            transformOrigin: Item.Center
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft | Qt.AlignBottom

            Button {
                id: load
                height: 25
                text: qsTr("Load")
            }

            Button {
                id: save
                height: 25
                text: qsTr("Save")
            }
        }
    }
}
