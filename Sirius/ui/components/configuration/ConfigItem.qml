import QtQuick 2.0
import QtQuick.Layouts 1.2
Item {
            x: 5
            width: parent.width
            height: 40
            RowLayout {
                anchors.rightMargin: 10
                anchors.bottomMargin: 0
                anchors.leftMargin: 0
                anchors.topMargin: 0
                anchors.fill: parent
                Text {
                    text: valueName + ": "
                    Layout.preferredWidth: 150
                }
                Rectangle{
                    height: 25

                    color: "white"
                    Layout.fillWidth: true
                    TextEdit{
                        anchors.fill: parent
                        id: value
                        text: currentVal
                        onTextChanged: model.currentVal = text

                    }
                }
                spacing: 10
            }
}

