import QtQuick 2.4
import QtQuick.Layouts 1.2
Item {
    Component.onCompleted: {
        var keys = configClass.getValuesKeys();
        keys.forEach(function(key){
            var val = configClass.getValue(key)
            itemsModel.append({"valueName":key,"currentVal":val})
        })
    }
    function getTools(){
        return [["qrc:/img/resources/media/images/check.svg", 1]];
    }
    function tool1(){
        console.error("Saving")
        for(var i = 0; i < itemsModel.count;i++){
            configClass.setValue(itemsModel.get(i).valueName,itemsModel.get(i).currentVal);
        }
        configClass.saveConfiguration();
    }

    ListView {
        id: listView
        anchors.fill: parent
        delegate: Item {
            x: 5
            width: parent.width
            height: 40
            RowLayout {
                anchors.rightMargin: 20
                anchors.leftMargin: 10
                anchors.fill: parent
                Text {
                    text: valueName + ": "
                    Layout.preferredWidth: 150
                }
                Rectangle{
                    height: 25

                    color: "white"
                    Layout.fillWidth: true
                    TextEdit{
                        anchors.fill: parent
                        id: value
                        text: currentVal
                        onTextChanged: model.currentVal = text

                    }
                }
                spacing: 10
            }
        }

        model: ListModel {
           id: itemsModel
        }
    }
}
