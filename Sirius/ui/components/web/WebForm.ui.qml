import QtQuick 2.4
import QtQuick.Controls 1.1
import QtWebView 1.1
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.2

Item {
    id: item1
    property alias webView: webView
    WebView {
        id: webView
        anchors.fill: parent
    }
}
