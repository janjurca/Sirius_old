import QtQuick 2.4
import QtWebView 1.1
import QtWebKit 3.0

WebForm {
    signal toolChanged()
    function setUrl(url){
        webView.url = url
    }

    function getTools(){
        return [["qrc:/img/resources/media/images/youtube.svg", 1]];
    }
    function tool1(){
        setUrl("http://www.youtube.com")

    }

    Component.onCompleted: {
        setUrl("http://www.google.com")
    }
}
