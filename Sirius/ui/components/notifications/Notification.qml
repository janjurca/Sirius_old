import QtQuick 2.4

NotificationForm { 
    id: notificationInstance
    label.text: notifText
    key.text: notifID

    ok_b.onClicked: {
        notifClass.notifyDone(key.text);
        notifListModel.removeNotif(key.text);
    }

    wait_b.onClicked: {
        notifClass.suspendNotification(key.text);
        notifListModel.removeNotif(key.text);
    }
}
