import QtQuick 2.0

Item {
    id:notificationList
    Connections {
        target: notifClass
        onNotify: {
            var i;
            for(i = 0; i < notifListModel.count; i++){
                if(notifListModel.get(i).notifID === id)
                    return; //if there is already the notif
            }
            if(notifListModel.count == 0){
                notificationList.visible = true
            }

            notifListModel.append({"notifID": id , "notifText": message, "Layout.fillWidth": true})
        }
    }

    ListView {
        id: listView
        anchors.fill: parent
        delegate: Notification{}
        model: ListModel {
           id:notifListModel

           function removeNotif(id){
               var i
                for(i = 0; i < notifListModel.count; i++){
                    if(notifListModel.get(i).notifID == id){
                        notifListModel.remove(i);
                        if(notifListModel.count == 0){
                            notificationList.visible = false;
                        }

                        break;
                    }
                }
           }
        }
    }
}
