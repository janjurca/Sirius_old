import QtQuick 2.4
import QtQuick.Layouts 1.3

Item {
    id: item1
    height: 30
    anchors.right: parent.right
    anchors.left: parent.left
    property alias label: label
    property alias key: key
    property alias wait_b: wait_b
    property alias ok_b: ok_b
    property alias rowLayout: rowLayout

    RowLayout {
        id: rowLayout
        anchors.bottomMargin: 2
        anchors.topMargin: 2
        anchors.fill: parent

        Text {
            id: key
            text: qsTr("id")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            Layout.fillHeight: true
            Layout.fillWidth: false
            font.pixelSize: 12
        }

        Text {
            id: label
            text: qsTr("label")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            font.weight: Font.ExtraLight
            Layout.fillWidth: true
            Layout.fillHeight: true
            font.pixelSize: 12
        }

        Image {
            id: wait
            width: 100
            height: 100
            fillMode: Image.PreserveAspectFit
            Layout.fillWidth: false
            Layout.fillHeight: true
            source: "qrc:/img/resources/media/images/uncheck.svg"

            MouseArea {
                id: wait_b
                anchors.fill: parent
            }
        }

        Image {
            id: ok
            width: 87
            height: 100
            fillMode: Image.PreserveAspectFit
            Layout.fillWidth: false
            Layout.fillHeight: true
            source: "qrc:/img/resources/media/images/check.svg"

            MouseArea {
                id: ok_b
                anchors.fill: parent
            }
        }
    }
}
