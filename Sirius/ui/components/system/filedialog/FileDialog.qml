import QtQuick 2.0
import "utils.js" as Utils
import Qt.labs.folderlistmodel 2.1
import QtQuick.Window 2.0
import QtQuick.Layouts 1.3


Item {
    id: dialog
    signal fileSelected(string fileName)
    property string selected: folderListModel.folder

    Component.onCompleted: {
        folderListModel.folder = "file:///"
    }

    function isFolder(fileName) {
        return folderListModel.isFolder(
                    folderListModel.indexOf(
                        folderListModel.folder + "/" + fileName))
    }
    function canMoveUp() {
        return folderListModel.folder.toString() !== "file:///"
    }

    function onItemSelect(filename){
        if (fileName === ".." && canMoveUp()) {
            folderListModel.folder = folderListModel.parentFolder
        } else if (fileName !== ".") {
            if (folderListModel.folder.toString() === "file:///") {
                folderListModel.folder += fileName
            } else {
                folderListModel.folder += "/" + fileName
            }
        }
    }
    function selectItem(itemName){
        selected = itemName
    }

    function onItemClick(fileName) {
        if(isFolder(fileName)){
            if (fileName === ".." && canMoveUp()) {
                folderListModel.folder = folderListModel.parentFolder
            } else if (fileName !== ".") {
                if (folderListModel.folder.toString() === "file:///") {
                    folderListModel.folder += fileName
                } else {
                    folderListModel.folder += "/" + fileName
                }
            }
        }
    }
    anchors.fill: parent
    Rectangle {
        id: rectangle
        color: "#ffffff"
        anchors.fill: parent

        ListView {
            id: listV
            anchors.top: controlls.bottom
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.topMargin: 0
            anchors.bottom: parent.bottom
            highlightMoveDuration : 200
            highlightMoveVelocity : 1000
            highlight: Component{
                id: listHighlight
                Rectangle{
                    color: "lightblue"
                }
            }

            FolderListModel {
                id: folderListModel
                showDotAndDotDot: true
                showHidden: true
                showDirsFirst: true

                //        nameFilters: ["*.qml"]
            }
            Component {
                id: fileDelegate
                Item {
                    id: item1
                    width: listV.width
                    height: 40

                    Image {
                        id: image
                        width: 40
                        clip: false
                        fillMode: Image.PreserveAspectFit
                        anchors.left: parent.left
                        anchors.leftMargin: 0
                        anchors.top: parent.top
                        anchors.topMargin: 0
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 0
                        source: isFolder(itemName.text) ? "qrc:/img/resources/media/images/folder_ico.png" : "qrc:/img/resources/media/images/file_ico.png"
                    }

                    Text {
                        id: itemName
                        text: qsTr(fileName)
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft
                        anchors.left: image.right
                        anchors.right: parent.right
                        anchors.bottom: parent.bottom
                        anchors.top: parent.top
                        anchors.leftMargin: 0
                        font.pixelSize: 12
                    }

                    MouseArea {
                        id: mouseArea
                        anchors.fill: parent
                        onClicked: {
                            selectItem(folderListModel.folder + "/" + itemName.text)
                            listV.currentIndex = index
                        }
                        onDoubleClicked: {
                            onItemClick(itemName.text)
                        }
                    }
                }
            }
            model: folderListModel
            delegate: fileDelegate
        }

        Rectangle {
            id: controlls
            height: 40
            color: "#ffffff"
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0

            Flickable {
                id: path_flickable
                clip: true
                flickableDirection: Flickable.HorizontalFlick
                anchors.right: cancel_rect.left
                anchors.rightMargin: 10
                anchors.left: ok_rect.right
                anchors.leftMargin: 10
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0

                Text {
                    id: path
                    text: folderListModel.folder.toString().replace("file:///"," ► ").replace(new RegExp("/", 'g'), " ► ")
                    anchors.fill: parent
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    font.pixelSize: 12
                }
            }

            Rectangle {
                id: ok_rect
                width: 67
                color: "#ffffff"
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0

                Text {
                    id: ok
                    text: qsTr("OK")
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.fill: parent
                    font.pixelSize: 12
                }

                MouseArea {
                    id: ok_mouse
                    anchors.fill: parent
                    onClicked: {
                        fileSelected(selected)
                    }
                }
            }

            Rectangle {
                id: cancel_rect
                x: 563
                width: 67
                color: "#ffffff"
                anchors.top: parent.top
                anchors.topMargin: 0
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 0

                Text {
                    id: cancel
                    text: qsTr("Cancel")
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.fill: parent
                    font.pixelSize: 12
                }

                MouseArea {
                    id: cancel_mouse
                    anchors.fill: parent
                    onClicked: {
                        fileSelected("r")
                    }
                }
            }


        }
    }

}
