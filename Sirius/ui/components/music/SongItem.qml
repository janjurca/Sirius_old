import QtQuick 2.0

Item {
    id: root
    height: 40

    Rectangle {
        id: rectangle
        color: "#ffffff"
        anchors.leftMargin: 10
        anchors.fill: parent

        Text {
            id: song
            x: 50
            y: 0
            text: songName
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: cover.right
            anchors.leftMargin: 10
            font.pixelSize: 20
        }

        Image {
            id: cover
            width: 40
            fillMode: Image.PreserveAspectFit
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0
            source: "qrc:/img/resources/media/images/song_cover.svg"
        }
    }

}
