import QtQuick 2.4
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    width: 400
    height: 400
    property alias songsList: songsList

    ListView {
        id: songsList
        anchors.fill: parent
    }
}
