import QtQuick 2.4
import QtQuick.Controls 2.0
import main.globals 1.0
import "./deviceDelegates"
DevicesForm {


    Connections {
        target: DevicesControll

        onDeviceAdded:{
            var role = DevicesControll.deviceRole(id)
            if(role === "1"){
                monitordeviceModel.append({d_id:id});
            } else if (role ==="2") {
                controlldeviceModel.append({d_id: id})
            } else if (role === "3"){
                telemetrydeviceModel.append({d_id: id})
            }
        }
        onClear: {
            monitordeviceModel.clear()
            controlldeviceModel.clear()
            telemetrydeviceModel.clear()
        }
    }
    function getTools() {
        return [["qrc:/img/resources/media/images/refresh.svg", 1],
                ["qrc:/img/resources/media/images/plus.svg", 2],
                ["qrc:/img/resources/media/images/controller.svg", 3],
                ]
    }
    function tool1() {
        DevicesControll.loadDevices()
    }
    function tool2() {
        var component = Qt.createComponent("DeviceConfig.qml")
        var object = component.createObject(Globals.mainSwipeView, {
                                                m_id: ""
                                            })
    }
    function tool3() {
        var component = Qt.createComponent("ControllerConfig.qml")
        var object = component.createObject(Globals.mainSwipeView)
    }


    deviceList.model: ListModel {
        id: controlldeviceModel
    }

    ListModel {
        id: monitordeviceModel
    }
    ListModel {
        id: telemetrydeviceModel
    }

    Component.onCompleted: {
        DevicesControll.loadDevices()
        if(cdevices.checked){
            deviceList.model = controlldeviceModel
            deviceList.delegate = controllDeviceDelegate
        }else if(mdevices.checked){
            deviceList.model = monitordeviceModel
            deviceList.delegate = monitorDeviceDelegate
        }else if(tdevices.checked){
            deviceList.model = telemetrydeviceModel
            deviceList.delegate = telemetryDeviceDelegate
        }
    }


     tabBar.onCurrentIndexChanged: {
         if(cdevices.checked){
             deviceList.model = controlldeviceModel
             deviceList.delegate = controllDeviceDelegate
         }else if(mdevices.checked){
             deviceList.model = monitordeviceModel
             deviceList.delegate = monitorDeviceDelegate
         }else if(tdevices.checked){
             deviceList.model = telemetrydeviceModel
             deviceList.delegate = telemetryDeviceDelegate
         }

     }

    //Delegates
    Component{
        id:controllDeviceDelegate
        ControllDevice{
            anchors.left: parent.left
            anchors.right: parent.right
        }
    }
    Component{
        id:monitorDeviceDelegate
        MonitorDevice{
            anchors.left: parent.left
            anchors.right: parent.right
        }
    }
    Component{
        id:telemetryDeviceDelegate
        TelemetryDevice{
            anchors.left: parent.left
            anchors.right: parent.right
        }
    }
}
