import QtQuick 2.4
import QtQuick.Controls 2.0
import worldcontroll.device 1.0

Item {
    id: root
    width: 500
    height: 680
    signal saved

    property string m_id: ""
    Device {
        id: device
        uid: m_id
        Component.onCompleted: {
            if (uid !== "") {
                dform.idSet.text = uid
                dform.nameSet.text = name
                dform.noteSet.text = note
                dform.pinSet.value = pin
                for(var i =0;dform.roleSet.count;i++){
                    if(dform.roleSet.model.get(i).value === role){
                         dform.roleSet.currentIndex = i
                        break;
                    }
                }
                for(i =0;dform.modeSet.count;i++){
                    if(dform.modeSet.model.get(i).value === mode){
                         dform.modeSet.currentIndex = i
                        break;
                    }
                }
                for(i =0;dform.typeSet.count;i++){
                    if(dform.typeSet.model.get(i).value === type){
                         dform.typeSet.currentIndex = i
                        break;
                    }
                }
                for(i =0;dform.controllerSet.count;i++){
                    if(dform.controllerSet.model.get(i).value === controller){
                         dform.controllerSet.currentIndex = i
                        break;
                    }
                }

                dform.active_valueSet.value = active_value
                dform.inactive_valueSet.value = inactive_value
                dform.actual_valueSet.text = String(actual_value)
            }
        }
    }

    DeviceConfigForm {
        id: dform
        anchors.fill: parent
        remove.onClicked: {
            if(device.uid !== ""){
                if(device.remove())
                    root.destroy()
            }
        }

        ok.onClicked: {
            if(device.uid === ""){
                      device.uid = DevicesControll.createDevice(controllerModel.get(controllerSet.currentIndex).value,
                                                   typesModel.get(typeSet.currentIndex).value,
                                                   roleModel.get(roleSet.currentIndex).value,
                                                   modeModel.get(modeSet.currentIndex).value);
            }
            device.name =  nameSet.text
            device.type = typesModel.get(typeSet.currentIndex).value
            device.note = noteSet.text
            device.pin = pinSet.value
            device.role = roleModel.get(roleSet.currentIndex).value
            device.mode = modeModel.get(modeSet.currentIndex).value
            device.active_value = active_valueSet.value
            device.inactive_value = inactive_valueSet.value
            device.controller = controllerModel.get(controllerSet.currentIndex).value
            ok.text = "Hopefully saved!"
        }

        roleSet.textRole: "text"
        roleSet.model: ListModel {
            id: roleModel
        }
        modeSet.textRole: "text"
        modeSet.model: ListModel {
            id: modeModel
        }
        controllerSet.textRole: "text"
        controllerSet.model: ListModel {
            id: controllerModel
        }

        typeSet.textRole: "text"
        typeSet.model: ListModel {
            id: typesModel
        }

        Component.onCompleted: {
            var roles = DevicesControll.roles()
            for (var prop in roles) {
                roleModel.append({
                                     text: prop,
                                     value: roles[prop]
                                 })
            }
            roleSet.currentIndex = 0
            var modes = DevicesControll.modes()
            for (prop in modes) {
                modeModel.append({
                                     value: modes[prop],
                                     text: prop
                                 })
            }
            modeSet.currentIndex = 0
            var controllers = DevicesControll.controllers()
            for (prop in controllers) {
                controllerModel.append({
                                           text: controllers[prop] + " | " + prop,
                                           value: prop
                                       })
            }
            controllerSet.currentIndex = 0

            var types = DevicesControll.types()
            for (prop in types) {
                typesModel.append({
                                           value: types[prop],
                                           text: prop
                                       })
            }
            typeSet.currentIndex = 0

            active_valueSet.value = 1
            inactive_valueSet.value = 0
            actual_valueSet.text = ""
        }
    }
}
