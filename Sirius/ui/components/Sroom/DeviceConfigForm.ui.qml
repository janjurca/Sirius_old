import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0

Item {
    property int s_width: 150
    property alias ok: ok
    height: 600
    property alias remove: remove
    property alias typeSet: typeSet
    property alias actual_valueSet: actual_valueSet
    property alias inactive_valueSet: inactive_valueSet
    property alias active_valueSet: active_valueSet
    property alias modeSet: modeSet
    property alias roleSet: roleSet
    property alias pinSet: pinSet
    property alias controllerSet: controllerSet
    property alias noteSet: noteSet
    property alias idSet: idSet
    property alias nameSet: nameSet
    Rectangle {
        id: root
        anchors.fill: parent
        color: "#353637"

        Rectangle {
            id: rectangle
            color: root.color
            anchors.rightMargin: 10
            anchors.leftMargin: 10
            anchors.bottomMargin: 10
            anchors.topMargin: 10
            anchors.fill: parent
            ColumnLayout {
                id: columnLayout
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.rightMargin: 0
                anchors.leftMargin: 0
                anchors.topMargin: 0

                Item {
                    id: row
                    height: 40
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Text {
                        color: "#ffffff"
                        id: name_l
                        text: qsTr("Name")
                        verticalAlignment: Text.AlignVCenter
                        anchors.fill: parent
                        font.pixelSize: 12
                    }
                    Rectangle {
                        x: 0
                        width: s_width
                        height: 40
                        anchors.right: parent.right
                        TextEdit {
                            id: nameSet
                            text: qsTr("Name")
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: TextEdit.AlignVCenter
                            anchors.fill: parent
                            font.pixelSize: 12
                        }
                    }
                }
                Item {
                    id: item1
                    height: 40
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    Text {
                        color: "#ffffff"
                        id: id_l
                        text: qsTr("ID")
                        verticalAlignment: Text.AlignVCenter
                        anchors.fill: parent
                        font.pixelSize: 12
                    }
                    Text {
                        color: "#ffffff"
                        id: idSet
                        width: s_width
                        height: 40
                        anchors.right: parent.right
                        text: qsTr("id")
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: 12
                    }
                }
                Item {
                    height: 40
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Text {
                        color: "#ffffff"
                        id: note_l
                        text: qsTr("Note")
                        verticalAlignment: Text.AlignVCenter
                        anchors.fill: parent
                        font.pixelSize: 12
                    }

                    Rectangle {
                        x: 0
                        width: s_width
                        height: 40
                        anchors.right: parent.right
                        TextEdit {
                            id: noteSet
                            anchors.fill: parent
                            text: qsTr("Note")
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: TextEdit.AlignVCenter
                            font.pixelSize: 12
                        }
                    }
                }
                Item {
                    height: 40
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    Text {
                        color: "#ffffff"
                        id: type_l
                        text: qsTr("Type")
                        font.pixelSize: 12
                        verticalAlignment: Text.AlignVCenter
                        anchors.fill: parent
                    }
                    ComboBox {
                        id: typeSet
                        width: s_width
                        anchors.right: parent.right
                    }
                }
                Item {
                    height: 40
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Text {
                        color: "#ffffff"
                        id: contoller_l
                        text: qsTr("Controller board")
                        verticalAlignment: Text.AlignVCenter
                        anchors.fill: parent
                        font.pixelSize: 12
                    }
                    ComboBox {
                        id: controllerSet
                        width: s_width
                        anchors.right: parent.right
                    }
                }
                Item {
                    height: 40
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Text {
                        color: "#ffffff"
                        id: pin_l
                        text: qsTr("Pin")
                        verticalAlignment: Text.AlignVCenter
                        anchors.fill: parent
                        font.pixelSize: 12
                    }

                    SpinBox {
                        id: pinSet
                        width: s_width
                        anchors.right: parent.right
                        Layout.fillWidth: false
                    }
                }
                Item {
                    height: 40
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Text {
                        color: "#ffffff"
                        id: role_l
                        text: qsTr("Role")
                        verticalAlignment: Text.AlignVCenter
                        anchors.fill: parent
                        font.pixelSize: 12
                    }
                    ComboBox {
                        id: roleSet
                        width: s_width
                        anchors.right: parent.right
                    }
                }
                Item {
                    height: 40
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Text {
                        color: "#ffffff"
                        id: mode_l
                        text: qsTr("Mode")
                        verticalAlignment: Text.AlignVCenter
                        anchors.fill: parent
                        font.pixelSize: 12
                    }
                    ComboBox {
                        id: modeSet
                        width: s_width
                        anchors.right: parent.right
                        model: ListModel {
                            ListElement {
                                text: "DIGIGTAL"
                            }
                            ListElement {
                                text: "ANALOG"
                            }
                        }
                    }
                }
                Item {
                    height: 40
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Text {
                        color: "#ffffff"
                        id: active_value_l
                        text: qsTr("Active value")
                        verticalAlignment: Text.AlignVCenter
                        anchors.fill: parent
                        font.pixelSize: 12
                    }
                    SpinBox {
                        id: active_valueSet
                        width: s_width
                        anchors.right: parent.right
                    }
                }
                Item {
                    height: 40
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Text {
                        color: "#ffffff"
                        id: inactive_value_l
                        text: qsTr("Inactive value")
                        verticalAlignment: Text.AlignVCenter
                        anchors.fill: parent
                        font.pixelSize: 12
                    }
                    SpinBox {
                        id: inactive_valueSet
                        width: s_width
                        anchors.right: parent.right
                    }
                }
                Item {
                    height: 40
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    Text {
                        color: "#ffffff"
                        id: actual_value_l
                        text: qsTr("Actual value")
                        verticalAlignment: Text.AlignVCenter
                        anchors.fill: parent
                        font.pixelSize: 12
                    }
                    Text {
                        color: "#ffffff"
                        id: actual_valueSet
                        width: s_width
                        height: 40
                        anchors.right: parent.right
                        text: qsTr("Actual")
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: 12
                    }
                }

                RowLayout {
                    id: item2
                    height: 40
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Button {
                        id: remove
                        text: qsTr("Remove")
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                    }

                    Button {
                        id: ok
                        text: qsTr("Save")
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                    }
                }
            }
        }
    }
}
