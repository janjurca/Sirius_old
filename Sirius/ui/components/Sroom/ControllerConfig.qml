import QtQuick 2.4
import worldcontroll.controller 1.0

ControllerConfigForm {
    Controller{
        id: controller
        onUuidChanged: {
            nameSet.text = name
            for(var i = 0;i < typeModel.count; i++){
                if(typeModel.get(i).id === typeId()){
                    typeSet.currentIndex = i;
                    break;
                }
            }

        }
    }

    uuidSet.textRole: "label"
    uuidSet.model: ListModel{
        id:uuidModel
    }
    typeSet.textRole: "label"
    typeSet.model: ListModel{
        id:typeModel
    }

    uuidSet.onCurrentIndexChanged: {
        controller.uuid = uuidModel.get(uuidSet.currentIndex).uuid
    }

    Component.onCompleted: {
        var controllers = DevicesControll.controllers()
        for(var prop in controllers){
            uuidModel.append({
                                 label: controllers[prop] + " | " + prop,
                                 name: controllers[prop],
                                 uuid: prop
                             })
        }
        var types = DevicesControll.controllerTypes()
        for(var type in types){
            typeModel.append({
                                 label: type,
                                 id: types[type],
                                 name: type
                             })
        }
        typeSet.currentIndex = 0
        uuidSet.currentIndex = 0

    }

    ok.onClicked: {
        controller.name = nameSet.text
        controller.setType(typeModel.get(typeSet.currentIndex).id)
        ok.text = "Hopefully saved!"
    }

}
