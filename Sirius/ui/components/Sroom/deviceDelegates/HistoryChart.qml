import QtQuick 2.4

HistoryChartForm {
    property int i: 0
    Connections {
        target: rangeSlider.first
        onValueChanged: xaxis.min = new Date(rangeSlider.first.value)
    }
    Connections {
        target: rangeSlider.second
        onValueChanged: {xaxis.max = new Date(rangeSlider.second.value)}
    }

    Connections{
        target: Sensors
        onAddLeftSeries:{
            leftseries.clear()
            leftscatter.clear()
            var max = Number.MIN_VALUE;
            var min = Number.MAX_VALUE

              for (var i=0; i<x.length; i++){
                  var x_p = parseInt(x[i])
                  if(y[i] > max ){
                      max = y[i]
                  }
                  if(y[i] < min){
                      min = y[i]
                  }
                  if(x_p > toMsecsSinceEpoch(xaxis.max)){
                      xaxis.max = new Date(x_p+1)
                      rangeSlider.to = x_p

                  }
                  if(x_p < toMsecsSinceEpoch(xaxis.min)){
                      xaxis.min = new Date(x_p-1)
                      rangeSlider.from = x_p
                  }
                  rangeSlider.setValues(toMsecsSinceEpoch(xaxis.min),toMsecsSinceEpoch(xaxis.max))
                  leftseries.append(new Date(x_p),y[i]);
                  leftscatter.append(new Date(x_p),y[i]);
              }
              leftyaxis.max = max + ((max-min)*0.1)
              leftyaxis.min = min - ((max-min)*0.1)
              xaxis.visible = false
              xaxis.visible = true //there is a bug from Qt guys that the axis disapaer and this gets it back
        }
        onAddRightSeries:{
            rightseries.clear()
            rightscatter.clear()
            var max = Number.MIN_VALUE;
            var min = Number.MAX_VALUE
            for (var i=0; i<x.length; i++){
                var x_p = parseInt(x[i])
                if(y[i] > max ){
                    max = y[i]+1
                }
                if(y[i] < min){
                    min = y[i]-1
                }
                if(x_p > toMsecsSinceEpoch(xaxis.max)){
                    xaxis.max = new Date(x_p+1)
                    rangeSlider.to = x_p

                }
                if(x_p < toMsecsSinceEpoch(xaxis.min)){
                    xaxis.min = new Date(x_p-1)
                    rangeSlider.from = x_p
                }
                rangeSlider.setValues(toMsecsSinceEpoch(xaxis.min),toMsecsSinceEpoch(xaxis.max))
                rightseries.append(new Date(x_p),y[i]);
                rightscatter.append(new Date(x_p),y[i]);

            }
            rightyaxis.min = min
            rightyaxis.max = max
            xaxis.visible = false
            xaxis.visible = true //there is a bug from Qt guys that the axis disapaer and this gets it back
        }

        onClearSeries:{
            clear();
        }
    }

    function clear(){
        leftseries.clear()
        rightseries.clear()
        var now =  new Date(Date.now())
        rangeSlider.to = toMsecsSinceEpoch(now)
        rangeSlider.stepSize = 3600000
        leftyaxis.min = Number.MAX_VALUE
        leftyaxis.max = 0
        rightyaxis.min = Number.MAX_VALUE
        rightyaxis.max = 0
        xaxis.min = now
        xaxis.max = now
    }

     Component.onCompleted: clear()
    // DateTimeAxis is based on QDateTimes so we must convert our JavaScript dates to
    // milliseconds since epoch to make them match the DateTimeAxis values
    function toMsecsSinceEpoch(date) {
        var msecs = date.getTime()
        return msecs
    }
}
