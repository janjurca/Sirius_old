import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0
import worldcontroll.device 1.0
import main.globals 1.0

Item {
    id: root
    height: 50
    property var m_id: d_id
    property bool ready: false

    Device{
        id:device
        uid: m_id

        onUidChanged: {
            name.text = device.name
            note.text = device.note
            if(device.active_value === device.actual_value){
                m_switch.checked = true
            }
            ready = true
        }
    }

    Rectangle {
        id: pressRect
        anchors.fill: parent
        color: "transparent"
    }
    Image {
        id: image
        width: root.height
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        Layout.fillHeight: true
        fillMode: Image.PreserveAspectFit
        source: "qrc:/img/resources/media/images/device.svg"
    }

    Text {
        id: name
        text: qsTr("Device name")
        anchors.left: image.right
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        anchors.leftMargin: 10
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        Layout.fillHeight: true
        Layout.fillWidth: true
        font.pixelSize: 20
    }

    Text {
        id: note
        color: "#626262"
        text: qsTr("")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.right: m_switch.left
        anchors.rightMargin: 0
        anchors.left: name.right
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        font.pixelSize: 12
    }
    MouseArea{
        anchors.fill: parent
        hoverEnabled: true
        onPressed: {
            root.state = "pressed"
        }
        onReleased: {
            root.state = "normal"
        }
        onHoveredChanged: {
            root.state = "normal"
        }

        onPressAndHold: {
            var component = Qt.createComponent("../DeviceConfig.qml")
            var object = component.createObject(Globals.mainSwipeView, {
                                                    m_id: root.m_id
                                                })
        }
    }
    Switch {
        id: m_switch
        text: qsTr("")
        enabled: false
        checkable: true
        autoExclusive: false
        checked: false
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        onCheckedChanged: {
            if(ready){
                if(m_switch.checked){
                    device.turnOn()
                } else {
                    device.turnOff()
                }
            }
        }
    }


    states: [
        State {
            name: "pressed"

            PropertyChanges {
                target: name
                color: "#ffffff"
            }

            PropertyChanges {
                target: pressRect
                color: "#323232"
            }

            PropertyChanges {
                target: note
                color: "#c9c9c9"
            }
        },
        State {
            name: "normal"
        }
    ]
    transitions: [
           Transition {
               to: "pressed"
               ColorAnimation { target: name; duration: 1000}
               ColorAnimation { target: pressRect; duration: 1000}
               ColorAnimation { target: note; duration: 1000}
           },
        Transition {
            to: "normal"
            ColorAnimation { target: name; duration: 1000}
            ColorAnimation { target: pressRect; duration: 1000}
            ColorAnimation { target: note; duration: 1000}
        }

       ]
}
