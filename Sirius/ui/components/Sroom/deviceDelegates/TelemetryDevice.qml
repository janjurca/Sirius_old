import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0
import worldcontroll.device 1.0
import main.globals 1.0

Item {
    id: root
    height: 50
    property var m_id: d_id
    property bool ready: false

    function update() {
        var record = device.actual_value

        var unit = ""
        var img = ""
        var type = DevicesControll.mapTypeName(device.type)

        switch (type) {
        case "temperature":
        {
            unit = "°C"
            img = "qrc:/img/resources/media/images/telemetry/thermometer.svg"
            break
        }
        case "humidity":
        {
            unit = "%"
            img = "qrc:/img/resources/media/images/telemetry/humidity.svg"
            break
        }
        }
        var value = record + unit

        var milistime = Math.abs(Date.now() - device.value_age)
        var days = Math.floor(milistime / 86400000)
        var hours = Math.floor((milistime % 86400000) / 3600000)
        var minutes = Math.floor(((milistime % 86400000) % 3600000) / 60000)

        var time = ""
        if (days > 0) {
            time += days + " d "
        }
        if (hours > 0) {
            time += hours + " h "
        }
        if (minutes > 0) {
            time += minutes + " m "
        }
        if (days == 0 && hours == 0 && minutes == 0 ){
            time = "Now"
        }

        image.source = img
        actual_valueText.text = value
        timestampText.text = time
    }

    Device {
        id: device
        uid: m_id

        onUidChanged: {
            name.text = device.name
            note.text = device.note
            if (device.active_value === device.actual_value) {
                m_switch.checked = true
            }
            ready = true
            update()
        }
    }

    Rectangle {
        id: pressRect
        anchors.fill: parent
        color: "transparent"
    }
    Image {
        id: image
        width: root.height
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        Layout.fillHeight: true
        fillMode: Image.PreserveAspectFit
        source: "qrc:/img/resources/media/images/sensor.svg"
    }

    Text {
        id: name
        text: qsTr("Device name")
        anchors.left: image.right
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        anchors.leftMargin: 10
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        Layout.fillHeight: true
        Layout.fillWidth: true
        font.pixelSize: 20
    }

    Text {
        id: note
        color: "#626262"
        text: qsTr("Note")
        anchors.fill: parent
        verticalAlignment: Text.AlignBottom
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 12
    }
    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        onPressed: {
            root.state = "pressed"
        }
        onReleased: {
            root.state = "normal"
        }
        onHoveredChanged: {
            root.state = "normal"
        }

        onPressAndHold: {
            var component = Qt.createComponent("../DeviceConfig.qml")
            var object = component.createObject(Globals.mainSwipeView, {
                                                    m_id: root.m_id
                                                })
        }
    }
    Text {
        id: timestampText
        color: "#626262"
        text: qsTr("time")
        anchors.fill: parent
        Layout.fillHeight: true

        verticalAlignment: Text.AlignTop
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 12
    }

    Text {
        id: actual_valueText
        text: qsTr("N/A")
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 10
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 20
    }

    states: [
        State {
            name: "pressed"

            PropertyChanges {
                target: name
                color: "#ffffff"
            }

            PropertyChanges {
                target: pressRect
                color: "#323232"
            }

            PropertyChanges {
                target: note
                color: "#c9c9c9"
            }
        },
        State {
            name: "normal"
        }
    ]
    transitions: [
        Transition {
            to: "pressed"
            ColorAnimation {
                target: name
                duration: 1000
            }
            ColorAnimation {
                target: pressRect
                duration: 1000
            }
            ColorAnimation {
                target: note
                duration: 1000
            }
        },
        Transition {
            to: "normal"
            ColorAnimation {
                target: name
                duration: 1000
            }
            ColorAnimation {
                target: pressRect
                duration: 1000
            }
            ColorAnimation {
                target: note
                duration: 1000
            }
        }
    ]
}
