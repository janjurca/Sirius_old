import QtQuick 2.4
import QtCharts 2.0
import QtQuick.Extras 1.4
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    property alias leftseries: leftseries
    property alias rightseries: rightseries
    property alias xaxis: xaxis
    property alias leftyaxis: leftyaxis
    property alias rightyaxis: rightyaxis

    property alias leftscatter: leftscatter
    property alias rightscatter: rightscatter

    property alias rangeSlider: rangeSlider

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent

        RangeSlider {
            id: rangeSlider
            to: 1
            Layout.fillHeight: false
            Layout.fillWidth: true
            second.value: 1
            first.value: 0
        }
        DateTimeAxis {
            id: xaxis
            format: "HH"
            tickCount: 6
        }

        ChartView {
            id: chart
            Layout.fillHeight: true
            Layout.fillWidth: true
            antialiasing: true
            legend.visible: false

            LineSeries {
                id: leftseries
                axisX: xaxis
                axisY: ValueAxis {
                    id: leftyaxis
                    tickCount: 6
                    color: leftseries.color
                    labelsColor: leftseries.color
                }
            }
            LineSeries {
                id: rightseries
                axisX: xaxis
                axisYRight: ValueAxis {
                    id: rightyaxis
                    tickCount: 6
                    labelsColor: rightseries.color
                    color: rightseries.color
                }
            }
            ScatterSeries {
                id: leftscatter
                axisX: xaxis
                axisY: leftyaxis
                color: leftseries.color
                markerSize: 10
            }
            ScatterSeries {
                id: rightscatter
                axisX: xaxis
                axisYRight: rightyaxis
                color: rightseries.color
                markerSize: 10
            }
        }
    }
}
