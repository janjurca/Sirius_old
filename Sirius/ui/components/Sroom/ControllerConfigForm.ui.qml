import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0

Item {
    property int s_width: 150
    property alias ok: ok
    height: 600
    property alias uuidSet: uuidSet
    property alias typeSet: typeSet
    property alias nameSet: nameSet
    Rectangle {
        id: root
        anchors.fill: parent
        color: "#353637"

        Rectangle {
            id: rectangle
            color: root.color
            anchors.rightMargin: 10
            anchors.leftMargin: 10
            anchors.bottomMargin: 10
            anchors.topMargin: 10
            anchors.fill: parent

            Label {
                id: header
                color: "#ffffff"
                text: qsTr("Controller")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 20
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0
            }

            ColumnLayout {
                id: columnLayout
                spacing: 3
                anchors.bottom: ok.top
                anchors.bottomMargin: 0
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.top: header.bottom
                anchors.rightMargin: 0
                anchors.leftMargin: 0
                anchors.topMargin: 0

                Item {
                    id: item1
                    height: 40
                    Layout.fillHeight: false
                    Layout.fillWidth: true

                    Text {
                        color: "#ffffff"
                        id: id_l
                        text: qsTr("UUID")
                        verticalAlignment: Text.AlignVCenter
                        anchors.fill: parent
                        font.pixelSize: 12
                    }
                    ComboBox {
                        id: uuidSet
                        width: s_width
                        anchors.right: parent.right
                    }
                }
                Item {
                    id: row
                    height: 40
                    Layout.fillHeight: false
                    Layout.fillWidth: true
                    Text {
                        color: "#ffffff"
                        id: name_l
                        text: qsTr("Name")
                        verticalAlignment: Text.AlignVCenter
                        anchors.fill: parent
                        font.pixelSize: 12
                    }
                    Rectangle {
                        x: 0
                        width: s_width
                        height: 40
                        anchors.right: parent.right
                        TextEdit {
                            id: nameSet
                            text: qsTr("Name")
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: TextEdit.AlignVCenter
                            anchors.fill: parent
                            font.pixelSize: 12
                        }
                    }
                }
                Item {
                    height: 40
                    Layout.fillHeight: false
                    Layout.fillWidth: true

                    Text {
                        color: "#ffffff"
                        id: type_l
                        text: qsTr("Type")
                        font.pixelSize: 12
                        verticalAlignment: Text.AlignVCenter
                        anchors.fill: parent
                    }
                    ComboBox {
                        id: typeSet
                        width: s_width
                        anchors.right: parent.right
                    }
                }
            }

            Button {
                Layout.fillHeight: true
                Layout.fillWidth: true
                id: ok
                y: 504
                height: 40
                text: qsTr("Save")
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
            }
        }
    }
}
