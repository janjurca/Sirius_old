import QtQuick 2.0

Item {
    Image {
        id: img
        anchors.fill: parent
        source: "qrc:/img/resources/media/images/check.svg"
    }
}
