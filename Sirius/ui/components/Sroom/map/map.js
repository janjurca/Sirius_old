var blockSize = 40;
var Columns = 20;
var Rows = 20;
var maxIndex = maxColumn * maxRow;
var map = new Array(maxIndex);
var component;

//Index function used instead of a 2D array
function index(column, row) {
    return column + (row * maxColumn);
}

function render() {
    //Clear the map
    for (var i = 0; i < maxIndex; i++) {
        if (map[i] != null)
            map[i].destroy();
    }

    //TODO calculate for another resolution
    //maxColumn = Math.floor(background.width / blockSize);
    //maxRow = Math.floor(background.height / blockSize);
    //maxIndex = maxRow * maxColumn;

    //Initialize map
    map = new Array(maxIndex);
    for (var column = 0; column < Columns; column++) {
        for (var row = 0; row < Rows; row++) {
            map[index(column, row)] = null;
            createBlock(column, row);
        }
    }
}

function createBlock(column, row) {
    if (component == null)
        component = Qt.createComponent("Block.qml");

    // Note that if Block.qml was not a local file, component.status would be
    // Loading and we should wait for the component's statusChanged() signal to
    // know when the file is downloaded and ready before calling createObject().
    if (component.status == Component.Ready) {
        var dynamicObject = component.createObject(background);
        if (dynamicObject == null) {
            console.log("error creating block");
            console.log(component.errorString());
            return false;
        }
        dynamicObject.x = column * blockSize;
        dynamicObject.y = row * blockSize;
        dynamicObject.width = blockSize;
        dynamicObject.height = blockSize;
        board[index(column, row)] = dynamicObject;
    } else {
        console.log("error loading block component");
        console.log(component.errorString());
        return false;
    }
    return true;
}

function foo(){
    createBlock(10,10);
}

function parseRooms(str){

}

//![0]

