import QtQuick 2.4
import QtQuick.Controls 2.0

Item {
    id: item1
    property alias cdevices: cdevices
    property alias mdevices: mdevices
    property alias deviceList: deviceList
    property alias tdevices: tdevices
    property alias tabBar: tabBar

    TabBar {
        id: tabBar
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0

        TabButton {
            id: cdevices
            text: qsTr("Controll")
        }

        TabButton {
            id: mdevices
            text: qsTr("Monitoring")
        }
        TabButton {
            id: tdevices
            text: qsTr("Telemetry")
        }
    }

    ListView {
        id: deviceList
        anchors.top: tabBar.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.topMargin: 5
        spacing: 5
    }
}
