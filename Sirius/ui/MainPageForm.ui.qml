import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

import "./components/notifications" as Notifications
import "utils.js" as Utils
Item {
    id: root
    property alias send: send
    property alias input: input
    property alias stateLabel: stateLabel
    property alias outputArea: outputArea
    property alias connectionStatus: connectionStatus
    property alias connect: connect
    property alias folderSelect: folderSelect
    property alias folder: folder
    property alias generalFunctions: generalFunctions
    property alias root: root

    ColumnLayout {
        id: columnLayout1
        anchors.rightMargin: 5
        anchors.leftMargin: 5
        anchors.bottomMargin: 5
        anchors.topMargin: 5
        anchors.fill: parent

        RowLayout {
            spacing: 2
            Layout.preferredHeight: 40
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.rowSpan: 1
            transformOrigin: Item.Center
            Layout.fillHeight: false
            Layout.fillWidth: true

            Image {
                id: connectionStatus
                visible: true
                fillMode: Image.PreserveAspectFit
                Layout.fillHeight: true
                Layout.fillWidth: false
                source: "qrc:/img/resources/media/images/uncheck.svg"
                MouseArea {
                    id: connect
                    cursorShape: Qt.OpenHandCursor
                    anchors.fill: parent
                }
            }

            Image {
                id: folder
                fillMode: Image.PreserveAspectFit
                visible: false
                Layout.fillHeight: true
                source: "qrc:/img/resources/media/images/folder.svg"
                MouseArea {
                    id: folderSelect
                    cursorShape: Qt.OpenHandCursor
                    anchors.fill: parent
                }
            }

            Label {
                id: stateLabel
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                Layout.fillHeight: true
                Layout.fillWidth: false
            }
            TextField {
                id: input
                Layout.fillHeight: true
                Layout.fillWidth: true
                placeholderText: qsTr("Řekni mi něco")
            }
            Button {
                id: send
                text: qsTr("->")
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.fillWidth: false
                Layout.fillHeight: true
            }
        }

        Flickable {
            id: outputText
            Layout.fillWidth: true
            clip: false
            Layout.preferredHeight: 0
            Layout.preferredWidth: 0
            transformOrigin: Item.Center
            flickableDirection: Flickable.VerticalFlick
            Layout.fillHeight: true

            TextArea.flickable: TextArea {
                id: outputArea
                wrapMode: Text.WordWrap
                anchors.fill: parent
                verticalAlignment: Text.AlignTop
                font.pointSize: 10
                readOnly: true
            }
        }

        ListView {
            id: generalFunctions
            height: 50
            clip: true
            spacing: 5
            Layout.fillHeight: false
            orientation: ListView.Horizontal
            flickableDirection: Flickable.HorizontalFlick
            Layout.fillWidth: true

            Notifications.NotificationList {
                visible: false
                Layout.fillWidth: true
                Layout.fillHeight: true
            }
        }
    }
}
