#!/bin/bash
OK=$(git pull)

cd SiriusServer || exit 1

if [ "$OK" != "Already up-to-date." ]; then
  echo "Sirius has new version. So I trying to update"
  qmake SiriusServer.pro
  make

  PID=$(pgrep SiriusServer)
if [ ! -z $PID ]; then
  echo "Killing previous instance"
  kill -15 $PID
fi
  echo "Running new instance"
  ./SiriusServer

fi

PID=$(pgrep SiriusServer)
if [ -z $PID ]; then
    echo "Sirius not running. So I am trying to start it."
    ./SiriusServer
    PID=$(pgrep SiriusServer)
    if [ -z $PID ]; then
        echo "Cannot start sirius server, something is wrong."
    else
        echo "OK"
    fi

fi
