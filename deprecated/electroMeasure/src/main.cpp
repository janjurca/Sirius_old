/*
 * Time_NTP.pde
 * Example showing time sync to NTP time source
 *
 * This sketch uses the ESP8266WiFi library
 */

#include <TimeLib.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "main.h"
#include "limits.h"
const char ssid[] = "jarda";  //  your network SSID (name)
const char pass[] = "12345678900987654321abcdef";       // your network password

// NTP Servers:
IPAddress timeServer(195,113,144,201); // time-a.timefreq.bldrdoc.gov
const int timeZone = 0;     // Central European Time

WiFiUDP Udp;
unsigned int localPort = 8888;  // local port to listen for UDP packets
accurateTime_t actime;

void setup()
{
  Serial.begin(9600);
  while (!Serial) ; // Needed for Leonardo only
  delay(250);
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, pass);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }


  Serial.print("IP number assigned by DHCP is ");
  Serial.println(WiFi.localIP());
  Serial.println("Starting UDP");
  Udp.begin(localPort);
  Serial.print("Local port: ");
  Serial.println(Udp.localPort());
  Serial.println("waiting for sync");
  setSyncProvider(getNtpTime);

  actime.now = now();
  actime.now *= 1000;
  actime.lastRefresh = millis();
}

void loop()
{
    if (Serial.available()) {
        send(Serial.readString());
    }
}

void time_refresh(accurateTime_t *tm){
    unsigned long long now = millis();
    unsigned long long diff = now - tm->lastRefresh;
    if (diff < 0) {
        diff = LONG_MAX - tm->lastRefresh + now;
    }
    tm->now += diff;
    tm->lastRefresh = now;
}


/*-------- NTP code ----------*/

const int NTP_PACKET_SIZE = 48; // NTP time is in the first 48 bytes of message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets

time_t getNtpTime()
{
  while (Udp.parsePacket() > 0) ; // discard any previously received packets
  Serial.println("Transmit NTP Request");
  sendNTPpacket(timeServer);
  uint32_t beginWait = millis();
  while (millis() - beginWait < 15000) {
    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) {
      Serial.println("Receive NTP Response");
      Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
      unsigned long secsSince1900;
      // convert four bytes starting at location 40 to a long integer
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR;
    }
  }
  Serial.println("No NTP Response :-(");
  return 0; // return 0 if unable to get the time
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress &address)
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}

void send(String msg){
    //IPAddress broadcastIp(255, 255, 255, 255);
    //Serial.print(msg);
    IPAddress broadcastIp(192, 168, 1, 255);
    Udp.beginPacketMulticast(broadcastIp, 4547, WiFi.localIP());
    Udp.write(msg.c_str());
    Udp.endPacket();
    Udp.flush();
}

char *printLLNumber(char* buff,unsigned long long n, uint8_t base)
{
 unsigned char buf[16 * sizeof(long)]; // Assumes 8-bit chars.
  unsigned long long i = 0;

  if (n == 0) {
    buff[0] = '0';
    return buff;
  }

  while (n > 0) {
    buf[i++] = n % base;
    n /= base;
  }
  int j = i;
  for (; i > 0; i--)
    buff[j-i] = ((char) (buf[i - 1] < 10 ? '0' + buf[i - 1] :'A' + buf[i - 1] - 10));

  return buff;
}
