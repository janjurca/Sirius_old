#ifndef MAIN_H
#define MAIN_H value

#include <TimeLib.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

void sendNTPpacket(IPAddress &address);
time_t getNtpTime();
void send(String msg);

typedef struct accurateTime {
    unsigned long long  now;
    unsigned long lastRefresh;  /// milliseconds
} accurateTime_t;

char* printLLNumber(char* buff,unsigned long long n, uint8_t base);
void time_refresh(accurateTime_t *tm);
#endif
