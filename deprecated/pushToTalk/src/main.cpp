#include <SPI.h>
#include <Wire.h>
#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>

#define LED_WIFI_PIN D0
#define LED_LISTEN_PIN D5
#define BUTTON_PIN D4

extern "C" {
  #include "gpio.h"
}

extern "C" {
  #include "user_interface.h"
}


WiFiUDP Udp;
unsigned int localUdpPort = 4545;  // local port to listen on
char incomingPacket[255];  // buffer for incoming packets

const char* ssid = "jarda";
const char* password = "12345678900987654321abcdef";
int i = 0;
void startWifi();
void sleep();
void sendPacket(char str,int port);

bool clicked = false;
bool listening = false;
void setup() {
  pinMode(LED_WIFI_PIN,OUTPUT);
  pinMode(LED_LISTEN_PIN,OUTPUT);
  pinMode(BUTTON_PIN,INPUT_PULLUP);

  digitalWrite(LED_WIFI_PIN,LOW);
  digitalWrite(LED_LISTEN_PIN,LOW);

  Serial.begin(115200);
  delay(10);
  Serial.write("Setup!");
  wifi_fpm_set_sleep_type(LIGHT_SLEEP_T);

  Udp.begin(localUdpPort);
  startWifi();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if(WiFi.isConnected()){
    digitalWrite(LED_WIFI_PIN, HIGH);
  }
}


void loop() {
  if(!WiFi.isConnected()){
    Serial.println("Odpojeno od wifi");
    digitalWrite(LED_WIFI_PIN, LOW);
  }

  if(digitalRead(BUTTON_PIN) != HIGH && clicked == false){ //nastupna hrana tlacitka
    digitalWrite(LED_LISTEN_PIN, HIGH); //rozsvit diodu
    Serial.println("zacinam nahravat");
    sendPacket('s', 4545);
    clicked = true;
  } else if (digitalRead(BUTTON_PIN) == HIGH && clicked == true) { //sestupna hrana tlacitka
    digitalWrite(LED_LISTEN_PIN, LOW); //zhasni diodu
    clicked = false;
    Serial.println("stop nahravani");
    sendPacket('d', 4545);
    delay(25);
  } else if (digitalRead(BUTTON_PIN) == HIGH){
    //TODO sleep mode
  }

  delay(100);
}

void sleep()
{
    gpio_pin_wakeup_enable(GPIO_ID_PIN(2), GPIO_PIN_INTR_LOLEVEL); // or LO/ANYLEVEL, no change
    wifi_fpm_open();
    wifi_fpm_do_sleep(9990000);
}


void startWifi(){
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }
}

void sendPacket(char str,int port){
  Udp.beginPacketMulticast(IPAddress(192,168,1,255), 4545, WiFi.localIP());
  Udp.write(str);
  Udp.endPacket();
}
