#ifndef PIN_SET_H
#define PIN_SET_H
#include <Arduino.h>

/**
 * Read digital value of pin and return its value
 * @param  pin number of pin
 * @param  mode 0 = digital, 1 = analog
 * @return     true == HIGH | false == LOW
 */
int readPin(int pin, int mode = 0);

/**
 * @brief set pins on board according to parameter(including pullup,digital/analog write...)
 * @param pin number of pins
 * @param mode 1=output 0=input 2=input_pullup
 * @param d_or_a digital=0 analog=1
 * @param write if digital then 0/1; if analog 0-255
 **/
void setPin(int pin, int mode, int d_or_a, int write);
//|P|13|1|0|1|0 // set led pin on
//|P|0|0|1|0|0 // read A0 pin
/**
  * @brief parse message about pin setting, \0 is end string
  * STRUKUTURA ZPRÁVY: |P|pin|r/w|d_or_a|pinMode|write\0
                       |P|xx|x|x|x|xxx\0
                          0123456789012345
  * pinNumber = cislo pinNumber
  * r/w = 1 == W | 0 == R
  * d_or_a = digital=0 analog=1
  * pinMode = mode 1=output 0=input 2=input_pullup
  * write if digital then 0/1; if analog 0-255
  *
  * STRUKTURA ZPRÁVY PRO LISTENING:
  *                     |L|pin|value|a/d|timeout|analogCheckType\0
  *                     |L|xx|xxx|x|xxxxx|x
  *                     |L|8|1|0|1000|0
  * value value to be listen for
  * read digitaly=0 or analog=1
  * timeout ignore the signal for this time from the last
  * analogCheckType represents the type of listening true=listening_for_above| false=listening_for_belov
  */
void parseMsg(char *msg);

/**
  * @brief vynuluje pole
  * @param arr array to clear
  * @param n array size
  */
void nullArr(char *arr,int n);

/**
  * @brief get size of string
  * @param arr array to count
  */
int arrSize(char *arr);

/**
 * Send message to server with readed value in structured form
 * @param  pin   number of readed pin
 * @param  value the readed value
 * STRUKUTURA ZPRÁVY:
 *                  |R|pin|value\0
 *                  |R|xx|xxxx\0
 */
void sendMeasuredValue(int pin, int value);

#endif
