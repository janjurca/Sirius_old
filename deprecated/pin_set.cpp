#include "pinnotifregister.h"
#include "pin_set.h"
#include <Arduino.h>
#include "defines.h"

extern PinNotifRegister pinReg;

int readPin(int pin, int mode){
  if (mode == 0) {
    return digitalRead(pin);
  } else if (mode == 1) {
    return analogRead(pin + MAX_PIN_NUMBER);
  }
  return 0;
}

void setPin(int pin, int mode, int d_or_a, int write){
  #ifdef DEBUG
    Serial.print(__FUNCTION__);
    Serial.print(" pin: ");
    Serial.print(pin);
    Serial.print(" mode: ");
    Serial.print((mode)?((mode == 2)? "input_pullup":"output" ):"input");
    Serial.print(" d/a: ");
    Serial.print((d_or_a)?"analog":"digital");
    Serial.print(" write: ");
    Serial.println(write);
  #endif
  if (mode == 1) { // normal output mode
    pinMode(pin,OUTPUT);
  } else if(mode == 0){ // input mode
    pinMode(pin, INPUT);
  } else if (mode == 2) { // pullup input mode
    pinMode(pin, INPUT_PULLUP);
  }

  if(d_or_a == 0 ){ // digitalWrite
    if (write == 0) {
      digitalWrite(pin,LOW);
    } else {
      digitalWrite(pin, HIGH);
    }
  } else { // analog write part
    analogWrite(pin, write);
  }
}

void parseMsg(char *msg){
  #ifdef DEBUG
    Serial.print(__FUNCTION__);
    Serial.print(": ");
    Serial.println(msg);
  #endif
  if (arrSize(msg) >= 11) {
    /* code */ //TODO
  }
  char flag;
  int pin;
  int mode;
  int d_or_a;
  int write;
  int rw;

  sscanf(msg, "|%c|%d|%d|%d|%d|%d",&flag,&pin,&rw,&d_or_a,&mode,&write);
  if (flag =='P') {
    if (rw == 1) {
      setPin(pin, mode, d_or_a, write);
    } else if (rw == 0) {
      int value = readPin(pin, d_or_a);
      sendMeasuredValue(pin,value);
    }
  } else if (flag == 'L') {
    pinReg.registerPin(pin, rw , d_or_a, mode,write);
  }

}

void nullArr(char *arr, int n){
  for(int i = 0; i < n; i++){
    arr[i] = 0;
  }
}

int arrSize(char *arr){
  int i = 0;
  while (arr[i] != '\0') {
    i++;
  }
  return i;
}
void sendMeasuredValue(int pin, int value){
  char msg[20] = {0};
  sprintf(msg, "|R|%2d|%4d",pin,value);
  Serial.print(msg);
}
