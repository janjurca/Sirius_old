#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#define SENSOR_ID 1

RF24 radio(7, 8);

const byte rxAddr[6] = "00001";


void sendMessage(const char *msg);

void setup()
{
  radio.begin();
  radio.setRetries(15, 15);
  radio.openWritingPipe(rxAddr);

  radio.stopListening();
}

void loop()
{

  delay(1000);

}

void sleep(){

}

void sendMessage(const char *msg){
  radio.write(&msg, sizeof(msg));
}
