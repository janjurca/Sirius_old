#include <SPI.h>
#include <Wire.h>
#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <DHT.h>
#include "Timer.h"
#include <stdio.h>

//#define DEBUG
#define SENSOR_ID 0
#define DHTPIN D2    // what digital pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321

DHT dht(DHTPIN, DHTTYPE);
float humidity;
float temperature;
float heat_index;
const byte rxAddr[6] = "00001";
RF24 radio(4,15); // !!!! podle https://www.youtube.com/watch?v=iVmIs2i2O1Y
Timer timer;
void selfMeasure();
void UDPsend(const char *message);
void dhtRead();
WiFiUDP Udp;

#ifndef INGOING_ARDUINO_PORT
#define INGOING_ARDUINO_PORT  4548
#endif
#ifndef OUTGOING_ARDUINO_PORT
#define OUTGOING_ARDUINO_PORT  4549
#endif

char incomingPacket[255];  // buffer for incoming packets
const char* ssid = "jarda";
const char* password = "12345678900987654321abcdef";
void startWifi();
void sendPacket(const char *str,int port);
void OTA_set();
bool listening = false;

void setup() {

  Serial.begin(9600);

  Udp.begin(INGOING_ARDUINO_PORT);
  startWifi();
  #ifdef DEBUG
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  #endif
  //spusteni Oty a zmena jmena
  OTA_set();
  //-------------start nRF24L01-------------
/*  radio.begin();
  radio.openReadingPipe(0, rxAddr);
  radio.startListening();*/
  //-------------dht start-----------------
  //dht.begin();
  //-------------timer----------------------
  //timer.every(3600000, selfMeasure);
  //selfMeasure();

  pinMode(D8, INPUT);
}

void loop() {
  ArduinoOTA.handle();
  if(!WiFi.isConnected()){
    ESP.restart();
  }
  //timer.update();
/*  if (radio.available())
  {
    char text[32] = {0};
    radio.read(&text, sizeof(text));
    Serial.println(text);
    UDPsend(text);
  }*/

  if (Serial.available()){
    char buff[20]= {0};
    Serial.readBytes(buff, 19);
    sendPacket(buff, OUTGOING_ARDUINO_PORT);
    Serial.read();
  }
  int pktSize = Udp.parsePacket();
  if (pktSize) {
    char pktBuf[pktSize+1];
    for (size_t i = 0; i < pktSize+1; i++) {
      pktBuf[i] = 0;
    }
    /*Serial.print(Udp.remoteIP());
    Serial.print(":");
    Serial.println(Udp.remotePort());*/
    Udp.read(pktBuf, pktSize);
    Udp.stop();
    Udp.begin(INGOING_ARDUINO_PORT);
    Serial.print(pktBuf);
  }

}

void startWifi(){
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    #ifdef DEBUG;
    Serial.println("Connection Failed! Rebooting...");
    #endif
    delay(5000);
    ESP.restart();
  }
}


void sendPacket(const char *str,int port){
  #ifdef DEBUG
  Serial.print("UDPsend:");
  Serial.println(str);
  #endif
  Udp.beginPacketMulticast(IPAddress(192,168,1,255), port, WiFi.localIP());
  Udp.write(str);
  Udp.endPacket();
}

void UDPsend(const char *message){
  // send a reply, to the IP address and port that sent us the packet we received
  delay(1000);
  sendPacket(message, OUTGOING_ARDUINO_PORT);
  delay(1000);

}

void selfMeasure(){
  // Wait a few seconds between measurements.
  delay(2000);
  dhtRead();
  char message[20];
  sprintf(message, "|S|teplota|%d.%d|%d|",(int)temperature,(int)((temperature - ((int)temperature))*10),SENSOR_ID);
  UDPsend(message);
  delay(100);
  sprintf(message, "|S|vlhkost|%d.%d|%d|",(int)humidity,(int)((humidity - ((int)humidity))*10),SENSOR_ID);
  UDPsend(message);
  delay(100);
  sprintf(message, "|S|heat_index|%d.%d|%d|",(int)heat_index,(int)((heat_index - ((int)heat_index))*10),SENSOR_ID);
  UDPsend(message);
  delay(100);
}

void dhtRead(){
  humidity = NAN;
  temperature = NAN;
  while (isnan(humidity) || isnan(temperature)) {
    humidity = dht.readHumidity();
    temperature= dht.readTemperature();
    #ifdef DEBUG
    Serial.print(humidity);
    Serial.print(" ");
    Serial.print(temperature);
    Serial.println();
    #endif
  }
  heat_index = dht.computeHeatIndex(temperature, humidity, false);
}

void OTA_set(){
  // Port defaults to 8266
 // ArduinoOTA.setPort(8266);

 // Hostname defaults to esp8266-[ChipID]
 ArduinoOTA.setHostname("Sirius_connector");

 // No authentication by default
 // ArduinoOTA.setPassword((const char *)"123");
 ArduinoOTA.begin();
}
