from alpine:latest
LABEL maintainer="jjurcaj@gmail.com"

run apk add qt5-qtbase-dev
run apk add qt5-qtmultimedia-dev
run apk add qt5-qtserialport-dev
run apk add qt5-qtdeclarative-dev
run apk add make
run apk add g++
run apk add git
run git clone https://gitlab.com/kohoutovice/Sirius
run cd /Sirius/SiriusServer/ && git pull && qmake-qt5 && make
run echo "cd /Sirius/SiriusServer/ && git pull && qmake-qt5 && make && /Sirius/SiriusServer/SiriusServer" > /bin/SiriusStart
run chmod +x /bin/SiriusStart

EXPOSE 4547
#EXPOSE 3306
ENTRYPOINT ["sh" , "/bin/SiriusStart"]
