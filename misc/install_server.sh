#!/bin/bash
set -e
################# QT instalation #####################
sudo apt update

sudo apt install -y qt5-default
sudo apt install -y libqt5multimedia5
sudo apt install -y libqt5*
sudo apt install -y qtmultimedia5-*
sudo apt install -y libqt5serialport5-dev
sudo apt install python-pip
sudo apt-get install python-setuptools
pip install serial  --user
sudo pip install pyserial --user

################# Mariadb install ####################
sudo apt install mariadb-server


################# Pulseaudio server ##################
sudo apt install pulseaudio
sudo apt install pulseaudio-module-zeroconf
sudo apt install avahi
sudo apt install iptables

systemctl start avahi-daemon
systemctl enable avahi-daemon

iptables -I INPUT -p tcp --dport 4317 -j ACCEPT
iptables -I OUTPUT -p tcp --dport 4317 -j ACCEPT

sudo apt install iptables-persistent
iptables-save > /etc/ipatbles/rules.v4

echo "
load-module module-native-protocol-tcp auth-ip-acl=127.0.0.1;192.168.254.0/24 auth-anonymous=1
load-module module-zeroconf-publish" >> /etc/pulse/default.pa
