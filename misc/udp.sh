#!/bin/bash

HOST=${1}
PORT=${2}
while read i;
do
echo $i | nc -4u -w1 $HOST $PORT
done
