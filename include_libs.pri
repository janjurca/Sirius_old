QT += quick core multimedia network sql widgets serialport printsupport


INCLUDEPATH  += $$PWD/libs/ \
                $$PWD/libs/Connections \
                $$PWD/Sirius


SOURCES      += $$PWD/libs/Connections/databasehandling.cpp \
                $$PWD/libs/Connections/networkhandler.cpp \
                $$PWD/libs/Connections/cloudactions.cpp \
                $$PWD/libs/Connections/speechrecognition.cpp \
                $$PWD/libs/global.cpp \
    $$PWD/libs/SmtpClient/src/emailaddress.cpp \
    $$PWD/libs/SmtpClient/src/mimeattachment.cpp \
    $$PWD/libs/SmtpClient/src/mimecontentformatter.cpp \
    $$PWD/libs/SmtpClient/src/mimefile.cpp \
    $$PWD/libs/SmtpClient/src/mimehtml.cpp \
    $$PWD/libs/SmtpClient/src/mimeinlinefile.cpp \
    $$PWD/libs/SmtpClient/src/mimemessage.cpp \
    $$PWD/libs/SmtpClient/src/mimemultipart.cpp \
    $$PWD/libs/SmtpClient/src/mimepart.cpp \
    $$PWD/libs/SmtpClient/src/mimetext.cpp \
    $$PWD/libs/SmtpClient/src/quotedprintable.cpp \
    $$PWD/libs/SmtpClient/src/smtpclient.cpp \
    $$PWD/libs/textparser.cpp \
    $$PWD/libs/Cyphering/aes/qaesencryption.cpp \
    $$PWD/libs/configuration.cpp \
    $$PWD/libs/language.cpp \
    $$PWD/libs/system/systemcontroll.cpp \
    $$PWD/libs/media/mediaplayer.cpp \
    $$PWD/libs/media/musicmixer.cpp \
    $$PWD/libs/youtubeapi.cpp \
    $$PWD/libs/filebasics.cpp \
    $$PWD/libs/printing.cpp \
    $$PWD/libs/Connections/downloadmanager.cpp \
    $$PWD/libs/world_controll/device.cpp \
    $$PWD/libs/world_controll/devices.cpp \
    $$PWD/libs/world_controll/controllerboard.cpp \
    $$PWD/libs/abstractdbuser.cpp



HEADERS      += $$PWD/libs/Connections/databasehandling.h \
                $$PWD/libs/Connections/cloudactions.h \
                $$PWD/libs/Connections/speechrecognition.h \
                $$PWD/libs/Connections/networkhandler.h \
                $$PWD/libs/defines.h \
    $$PWD/libs/global.h \
    $$PWD/libs/SmtpClient/src/emailaddress.h \
    $$PWD/libs/SmtpClient/src/mimeattachment.h \
    $$PWD/libs/SmtpClient/src/mimecontentformatter.h \
    $$PWD/libs/SmtpClient/src/mimefile.h \
    $$PWD/libs/SmtpClient/src/mimehtml.h \
    $$PWD/libs/SmtpClient/src/mimeinlinefile.h \
    $$PWD/libs/SmtpClient/src/mimemessage.h \
    $$PWD/libs/SmtpClient/src/mimemultipart.h \
    $$PWD/libs/SmtpClient/src/mimepart.h \
    $$PWD/libs/SmtpClient/src/mimetext.h \
    $$PWD/libs/SmtpClient/src/quotedprintable.h \
    $$PWD/libs/SmtpClient/src/smtpclient.h \
    $$PWD/libs/SmtpClient/src/smtpexports.h \
    $$PWD/libs/SmtpClient/src/SmtpMime \
    $$PWD/libs/textparser.h \
    $$PWD/libs/Cyphering/aes/qaesencryption.h \
    $$PWD/libs/configuration.h \
    $$PWD/libs/language.h \
    $$PWD/libs/media/mediaplayer.h \
    $$PWD/libs/media/musicmixer.h \
    $$PWD/libs/system/systemcontroll.h \
    $$PWD/libs/youtubeapi.h \
    $$PWD/libs/filebasics.h \
    $$PWD/libs/Connections/downloadmanager.h \
    $$PWD/libs/printing.h \
    $$PWD/libs/world_controll/device.h \
    $$PWD/libs/world_controll/devices.h \
    $$PWD/libs/world_controll/controllerboard.h \
    $$PWD/libs/abstractdbuser.h



#include($$PWD/libs/AudioStreaming/AudioStreamingLib.pri)
