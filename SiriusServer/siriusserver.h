#ifndef SIRIUSSERVER_H
#define SIRIUSSERVER_H

#include <QObject>
#include "networkhandler.h"
#include "databasehandling.h"

class SiriusServer : public QObject
{
    Q_OBJECT
public:
    explicit SiriusServer(QObject *parent = 0);

private:
    QTimer *integrityCheck;
signals:

public slots:

private slots:
    void parseMessage(QString message, int port);
    void integrityStarter();
    void sayMe(QString);
};

#endif // SIRIUSSERVER_H
