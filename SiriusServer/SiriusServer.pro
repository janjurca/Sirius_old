QT += core multimedia network sql serialport
QT -= gui quick widgets
CONFIG += c++11

TARGET = SiriusServer
#CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    siriusserver.cpp

HEADERS += \
    siriusserver.h


include(../include_libs.pri)
#include(../libs/AudioStreaming/AudioStreamingLib.pri)


#VARIABLES
isEmpty(PREFIX) {
PREFIX = /usr
}
BINDIR = $$PREFIX/bin
DATADIR =$$PREFIX/share

#MAKE INSTALL
target.path = $$BINDIR

service.path = $$DATADIR/systemd/services
service.files += $${TARGET}.service

INSTALLS += target service
