#include "../defines.h"
#include <QCoreApplication>
#include <QCommandLineParser>
#include "global.h"

#include "siriusserver.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationName("Sirius server");
    QCoreApplication::setApplicationVersion("1.0");

    QCommandLineParser parser;
    parser.setApplicationDescription("Server part of sirius");
    parser.addHelpOption();
    // An option with a value
    QCommandLineOption configFile(QStringList() << "c" << "config",
          QCoreApplication::translate("main", "Set config file"),
          QCoreApplication::translate("main", "file"));
    parser.addOption(configFile);
    // Process the actual command line arguments given by the user
    parser.process(app);
    QString configFilename = parser.value(configFile);
    if(configFilename.isEmpty()){
        configFilename = WORK_DIR + "sirius.json";
    }
    config = new Configuration(configFilename); //TODO make the configuration cyphered
    try {
        config->load();
    } catch (const std::runtime_error& e) {
        printing.print(QString(e.what()));
    }

    SiriusServer siriusServer;

    return app.exec();
}
