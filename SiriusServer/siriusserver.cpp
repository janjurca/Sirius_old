#include "siriusserver.h"
#include <QFile>
#include <QtSql>
#include <QDateTime>
#include <QTimer>
#include "global.h"
#include <QJsonDocument>
#include <QJsonObject>
#include "printing.h"
#include <thread>
#include "world_controll/devices.h"
#include "world_controll/device.h"
#include "world_controll/controllerboard.h"

SiriusServer::SiriusServer(QObject *parent) : QObject(parent)
{
    network = new NetworkHandler(this);
    network->listenOnPort(INGOING_SERVER_PORT);
    network->startServer();

    db = new DatabaseHandling(config->getDBAdress(),config->getDBUserName(),config->getDBUserName(),config->getDBPassword(),this);
    connect(network,SIGNAL(receivedInput(QString,int)),this,SLOT(parseMessage(QString,int)));
    //connect(network,SIGNAL(receivedTCP(QString)),this,SLOT(parseMessage(QString)));
    integrityCheck = new QTimer;
    connect(integrityCheck,SIGNAL(timeout()),this,SLOT(integrityStarter()));
    integrityCheck->start(7200000);

    connect(&printing,SIGNAL(userPrint(QString)),this,SLOT(sayMe(QString)));
    printing.readyToPrint();
}

void SiriusServer::sayMe(QString m){
    QTextStream(stdout) << m << endl;
}
/**
 * @brief SiriusServer::parseMessage
 * @param message
 * zpracovava zpravy prijate ze site, tj. jsony
 */
void SiriusServer::parseMessage(QString message, int port){
    printing.print( QString("Got message: ") + message);
    if(message.at(0) == '{'){
        QJsonObject json = QJsonDocument::fromJson(message.toUtf8()).object();
        if(json["type"] == "telemetry"){
            QDateTime dt = QDateTime::currentDateTime();
            try{
                if(json.find("date_time") != json.end()){
                    dt = QDateTime::fromMSecsSinceEpoch(json["date_time"].toString().toULongLong());
                }
                QString did = Devices::sensorId(json["uuid"].toString(),json["value_type"].toString());
                if(!did.isEmpty()){
                    Device d(did);
                    if(!d.setActual_value(json["value"].toString().toDouble())){
                            qWarning("Cannot insert to db are we connected?");
                    }
                } else {
                    qWarning("No sensor found for this parameters, then i trying to add it");
                    if(!ControllerBoard::exists(json["uuid"].toString())){
                        ControllerBoard::create(json["uuid"].toString(),ControllerBoard::possibleTypes()["PASSIVE"]);
                    }
                    QString newDevice = Devices::createDevice(
                                json["uuid"].toString(),
                                Device::possibleTypes()[json["value_type"].toString()],
                                Device::possibleRoles()["TELEMETRY"],
                                Device::possibleModes()["SCALAR"],
                                json["value_type"].toString()
                            );
                    if(!newDevice.isEmpty()){
                        Device d(newDevice);
                        d.setActual_value(json["value"].toString().toDouble());
                    }

                }
            } catch (...){
                qWarning("Error while inserting telemetry data to db, probably index errror of json, or bad controller type");
            }

            //NEW device system

        } else if (json["type"] == "connectedController") {
            if(!ControllerBoard::exists(json["controller"].toString())){
                if (db->insertRow("controllers",QStringList() << "controller", QStringList() << json["controller"].toString())) {
                    qWarning() << (QString("New controller added:") + json["controller"].toString());
                } else {
                    qWarning() << (QString("Cannot add controller:") + json["controller"].toString());
                }
             }
        }
    }
}

/**
 * Starts testing database integrity
 * @brief SiriusServer::integrityStarter
 */
void SiriusServer::integrityStarter(){
    static std::thread integrityThread;     // spawn new thread
    static bool running = false;
    if (running) {
        if(integrityThread.joinable()){
            integrityThread.join();
            running = false;
        }
    } else {
        integrityThread = std::thread([](){

        });
        running = true;
    }
}
