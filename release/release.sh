#!/bin/bash
#THERE must be environment variable with api key inside, called GITLAB_API_KEY
set -e
mkdir tmp_workdir

cd tmp_workdir || exit 3

for TYPE in rpm deb
do
	echo "===================== Type ${TYPE} ============================================"
	for ARCHITECTURE in armv7 x86_64
	do
		echo "===================== Architecture ${ARCHITECTURE} ============================"
		rm -rf *
		URL=$(wget "https://gitlab.com/kohoutovice/Sirius/-/jobs/artifacts/master/download?job=release-${TYPE}-${ARCHITECTURE}&private_token=${GITLAB_API_KEY}" 2>&1 | grep artifacts/download | tail -n1 | awk '{n=split($0,a,"  ");print a[n]}')
		REQUEST="$URL?job=release-${TYPE}-${ARCHITECTURE}&private_token=${GITLAB_API_KEY}"
		echo $REQUEST
		curl -L  "$REQUEST" --output ${TYPE}-${ARCHITECTURE}_artifacts.zip
		unzip ${TYPE}-${ARCHITECTURE}_artifacts.zip

		INSTALLER_FILE=$(find . -type f | egrep "${TYPE}$")
		for file in ${INSTALLER_FILE}
		do
			echo "===================== File ${file} ============================================"
			if [[ "$TYPE" = "rpm" ]]; then
				true
				cloudsmith push rpm jan-jurca/sirius/fedora/29 ${file} || true
				cloudsmith push rpm jan-jurca/sirius/fedora/28 ${file} || true
				cloudsmith push rpm jan-jurca/sirius/el/7 ${file} || true
			elif [[ "$TYPE" = "deb" ]]; then
				true
				cloudsmith push deb jan-jurca/sirius/debian/jessie ${file} || true
				cloudsmith push deb jan-jurca/sirius/debian/stretch ${file} || true
				cloudsmith push deb jan-jurca/sirius/ubuntu/xenial ${file} || true
				cloudsmith push deb jan-jurca/sirius/ubuntu/yakkety ${file} || true
				cloudsmith push deb jan-jurca/sirius/ubuntu/zesty ${file} || true
			fi
		done
	done
done

cd ..
rm tmp_workdir -rf
