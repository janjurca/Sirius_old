<?php
    /**
     * Sensor representation
     */
    class sensor
    {
        public $type = "telemetry";
        public $uuid;
        public $value;
        public $value_type;
        function __construct($uuid,$value,$value_type)
        {
            $this->uuid = $uuid;
            $this->value = $value;
            $this->value_type = $value_type;
        }
    }


 ?>
