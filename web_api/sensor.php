<?php

require_once('inc/class.sensor.php');

if(!isset($_GET["device"])) echo "query is not complete\n";
if(!isset($_GET["time"])) echo "query is not complete\n";
if(!isset($_GET["duplicate"])) echo "query is not complete\n";
if(!isset($_GET["snr"])) echo "query is not complete\n";
if(!isset($_GET["station"])) echo "query is not complete\n";
if(!isset($_GET["data"])) echo "query is not complete\n";
if(!isset($_GET["avgSnr"])) echo "query is not complete\n";
if(!isset($_GET["lat"])) echo "query is not complete\n";
if(!isset($_GET["lng"])) echo "query is not complete\n";
if(!isset($_GET["rssi"])) echo "query is not complete\n";
if(!isset($_GET["seqNumber"])) echo "query is not complete\n";
if(!isset($_GET["uuid"])) echo "query is not complete\n";
#http://www.rowes.cz/sirius/sensor.php?device={device}&time={time}&duplicate={duplicate}&snr={snr}&station={station}&data={data}&avgSnr={avgSnr}&lat={lat}&lng={lng}&rssi={rssi}&seqNumber={seqNumber}&temperature={customData#temperature}.{customData#temperature_fp}&humidity={customData#humidity}.{customData#humidity_fp}.3&heatindex={customData#heatindex}.{customData#heatindex_fp}.3&uuid=0cd71471-8126-4c8a-bbbc-9002ffbe66d2&
#http://www.rowes.cz/sirius/sensor.php?device=help&temperature=24.4&uuid=0cd71471-8126-4c8a-bbbc-9002ffbe66d2&
$port = 4547;
$sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
socket_set_option($sock, SOL_SOCKET, SO_BROADCAST, 1);

$data_type = "temperature";
if(isset($_GET["$data_type"])){
    $$data_type = new Sensor($_GET["uuid"],$_GET["$data_type"],"$data_type");
    $JSON = json_encode($$data_type);
    echo $JSON;
    socket_sendto($sock, $JSON, strlen($JSON), 0, '255.255.255.255', $port);
}
$data_type = "humidity";
if(isset($_GET["$data_type"])){
    $$data_type = new Sensor($_GET["uuid"],$_GET["$data_type"],"$data_type");
    $JSON = json_encode($$data_type);
    echo $JSON;
    socket_sendto($sock, $JSON, strlen($JSON), 0, '255.255.255.255', $port);
}
$data_type = "heat_index";
if(isset($_GET["$data_type"])){
    $$data_type = new Sensor($_GET["uuid"],$_GET["$data_type"],"$data_type");
    $JSON = json_encode($$data_type);
    echo $JSON;
    socket_sendto($sock, $JSON, strlen($JSON), 0, '255.255.255.255', $port);
}
$data_type = "electricity";
if(isset($_GET["$data_type"])){
    $$data_type = new Sensor($_GET["uuid"],$_GET["$data_type"],"$data_type");
    $JSON = json_encode($$data_type);
    echo $JSON;
    socket_sendto($sock, $JSON, strlen($JSON), 0, '255.255.255.255', $port);
}

?>
