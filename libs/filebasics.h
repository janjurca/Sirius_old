#ifndef FILEBASICS_H
#define FILEBASICS_H

#include <QObject>

class FileBasics : public QObject
{
    Q_OBJECT
public:
    explicit FileBasics(QObject *parent = nullptr);
    Q_INVOKABLE QString readFile(QString filename);
    Q_INVOKABLE bool saveFile(QString filename, QString data);

signals:

public slots:
};

#endif // FILEBASICS_H
