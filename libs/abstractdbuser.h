#ifndef ABSTRACTDBUSER_H
#define ABSTRACTDBUSER_H


class AbstractDBUser
{
public:
    AbstractDBUser();
    virtual void dbIntegrityCheck(){};
};

#endif // ABSTRACTDBUSER_H
