#include "youtubeapi.h"
#include <QRegularExpression>
#include "networkhandler.h"

YoutubeAPI::YoutubeAPI(QObject *parent) : QObject(parent)
{

}



QString YoutubeAPI::getVideoLink(QString link){
    link = "https://www.youtube.com/watch?v=" + link;
    QEventLoop loop;
    QString program = "youtube-dl";
    QStringList arguments;
    arguments << "-f140" << "-g" << link;
    QProcess *youtube_dl = new QProcess();
    youtube_dl->start(program, arguments);
    connect(youtube_dl,SIGNAL(finished(int)),&loop,SLOT(quit()));
    loop.exec();
    return youtube_dl->readAllStandardOutput().split('\n').first();
}

QStringList YoutubeAPI::getPlaylistLinks(QString playlist_link){
    QRegularExpression re("<a href=\"/watch?(?<link>.*);",QRegularExpression::CaseInsensitiveOption);
    QString content = NetworkHandler::getWebContent(playlist_link);
    QStringList entities = getPlaylistEntities(content);
    for(int i = 0; i < entities.size(); i++){
        qWarning() << parseVideoID(entities.at(i));
    }
    return QStringList();
}

QString YoutubeAPI::parseVideoID(QString url){
    int index = 0;
    if ((index = url.indexOf("v=")) != -1) {
        return url.mid(index+2,11);
    }
    return "";
}

QStringList YoutubeAPI::getPlaylistEntities(QString web_content){
    int index = 0;
    QStringList ret;
    while ((index = web_content.indexOf("<li class=\"yt-uix-scroller-scroll-unit")) != -1) {
        int start = index;
        int state = 0;
        bool end = false;
        while (!end) {
            switch (state) {
            case 0:
                if(web_content.at(index) == '<')
                    state = 1;
                break;
            case 1:
                if(web_content.at(index) == '/')
                    state = 2;
                else
                    state = 0;
                break;
            case 2:
                if(web_content.at(index) == 'l')
                    state = 3;
                else
                    state = 0;
                break;
            case 3:
                if(web_content.at(index) == 'i')
                    state = 4;
                else
                    state = 0;
                break;
            case 4:
                if(web_content.at(index) == '>')
                    end = true;
                else
                    state = 0;
                break;

            default:
                break;
            }
            index++;
        }
        ret << web_content.mid(start,index-start );
        if(web_content.size() <= index)
            break;
        web_content = web_content.mid(index);

    }
    return ret;
}
