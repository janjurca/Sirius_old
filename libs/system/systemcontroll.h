#ifndef SYSTEMCONTROLL_H
#define SYSTEMCONTROLL_H

#include <QObject>

class systemControll : public QObject
{
    Q_OBJECT
public:
    explicit systemControll(QObject *parent = 0);
    void sleepMode();
    void shutDown();
    void reboot();


signals:

public slots:
};

#endif // SYSTEMCONTROLL_H
