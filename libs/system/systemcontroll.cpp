#include "systemcontroll.h"

systemControll::systemControll(QObject *parent) : QObject(parent)
{

}

void systemControll::sleepMode(){
    system("systemctl suspend");
}

void systemControll::shutDown(){
    system("shutdown -s -t now");
}

void systemControll::reboot(){
    system("reboot");
}
