#include "language.h"
#include "global.h"

Language::Language(QObject *parent) : QObject(parent)
{

}


int Language::findNextState(QString word, QString current_state, QString className){
    QPair<QString, QString>c_state(QString("state"), current_state);
    QPair<QString, QString>class_name(QString("class"), className);
    QList<QStringList> rows = db->readValue("fsm",QStringList() << "next_state" << "name" << "commands",QList<QPair<QString, QString>>() << c_state<<class_name);

    for(int i = 0; i < rows.size();i++){
        if(rows.at(i).at(2) == "&#&"){
            if(getWordFromVocabulary(rows.at(i).at(1)).indexOf(word) != -1){
                int next_state = QString(rows.at(i).at(0)).toInt();
                return next_state;
            }
        } else {
            if(QString(rows.at(i).at(2)).split(",").indexOf(word) != -1){
                int next_state = QString(rows.at(i).at(0)).toInt();
                return next_state;
            }
        }
    }
    return current_state.toInt();
}

int Language::findNextState(QString word, int actual_state){
    return findNextState(word,QString::number(actual_state));
}

int Language::findCommandstate(QString word,QString classIdentifier,int current_state){ //TODO make it dynamicaly for every class can make
    return findNextState(word,QString::number(current_state),classIdentifier);
}


QStringList Language::getWordFromVocabulary(QString word){
    QStringList ret;
    QList<QStringList> rows = db->readValue("slovnik",QStringList() << "variation","base",word );
    for(int i = 0; i < rows.size();i++){
        ret = QString(rows.at(i).first()).split(",");
    }
    return ret;
}
