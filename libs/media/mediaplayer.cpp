#include "mediaplayer.h"

mediaPlayer::mediaPlayer(QObject *parent) : QObject(parent)
{
    player = new QMediaPlayer;
    playlist = new QMediaPlaylist(player);
    player->setPlaylist(playlist);
    player->setVolume(100);
    connect(player,SIGNAL(currentMediaChanged(QMediaContent)),this,SLOT(mediaChanged()));

}

void mediaPlayer::play(){
        player->play();
}

/**
 * @brief mediaPlayer::addToPlaylist
 * @param urls List of files paths that have to be added to playlist
 * @param pocet
 */
void mediaPlayer::addToPlaylist(QList< QUrl > urls){
    for(int i = 0; i < urls.size();i++){
        playlist->addMedia(urls[i]);
    }
}

/**
 * @brief mediaPlayer::addToPlaylist
 * @param urls List of files paths that have to be added to playlist
 * @param pocet
 */
void mediaPlayer::addToPlaylist(QStringList urls){
    for(int i = 0; i < urls.size();i++){
        playlist->addMedia(QUrl::fromLocalFile(urls[i]));
    }
}

/**
 * On change of current playing song. Send name of song and if the song is last on playlist, send signal lastSong()
 * @brief mediaPlayer::mediaChanged
 */
void mediaPlayer::mediaChanged(){
    if(playlist->currentIndex() == (playlist->mediaCount() - 1))
        emit lastSong();
    emit currentSong(playlist->currentMedia().canonicalUrl().fileName());

}

void mediaPlayer::pause(){
    player->pause();
}

void mediaPlayer::next(){
    playlist->setCurrentIndex(playlist->currentIndex() + 1);

}
void mediaPlayer::previous(){
    playlist->setCurrentIndex(playlist->currentIndex() - 1);
}
void mediaPlayer::stop(){
    player->stop();
}
