#ifndef MEDIAPLAYER_H
#define MEDIAPLAYER_H

#include <QObject>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QDebug>
#include "global.h"

class mediaPlayer :public QObject
{
    Q_OBJECT
public:
    mediaPlayer(QObject *parent = 0);
    void pause();
    void stop();
    void next();
    void previous();
    void play();
    void addToPlaylist(QStringList urls);
    void addToPlaylist(QList< QUrl > urls);
public slots:
    void mediaChanged();

signals:
    void lastSong();
    void currentSong(QString);

private:
    QMediaPlayer *player;
    QMediaPlaylist *playlist;
};

#endif // MEDIAPLAYER_H
