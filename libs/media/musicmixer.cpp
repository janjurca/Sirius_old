#include "musicmixer.h"
#include "global.h"
#include "youtubeapi.h"

musicMixer::musicMixer(QObject *parent, QString musicDir){
    dir = musicDir;
    qsrand(QTime::currentTime().msec());
    std::thread first ();
}

void musicMixer::run()
{
    if(!dir.isEmpty()){
        printing.print(tr("Searching for music"));
        allFiles = getAllFiles(dir);
        printing.print(tr("Music search complete"));
        isMusic = true;
    } else {
        printing.print(tr("Music folder is NULL, so no music will be available"));
    }
    //QThread::run();
}

int musicMixer::getRandomNumber(const int Min, const int Max)
{
    return ((qrand() % ((Max + 1) - Min)) + Min);
}


void musicMixer::mixMusic(int count){
    if (!isMusic) { //FIXED when the music searching haven't ended and is called this funtion segfault is here
        return;
    }
    active=true;
    QStringList playlist_list;
    for(int i = 0; i < count; i++){
        if(allFiles.size() > 0){
            int cislo = getRandomNumber(0,allFiles.size() - 1);
            playlist_list << allFiles[cislo];
            allFiles.removeAt(cislo);
        }
    }

    player->addToPlaylist(playlist_list);
    printing.print(tr("Added to playlist:"));
    for(int i = 0; i < playlist_list.size();i++){
        printing.print(QString(playlist_list[i]).split("/").last());
    }
    play();
}


/**
 * specific word commands for this class
 * @brief musicMixer::command
 * @param command
 */
void musicMixer::command(QString command){
    int command_stav = lang->findCommandstate(command,"mediaPlayer");
    if(command_stav == 0 ){
        play();
        printing.print(tr("Playing"));
    }
    if(command_stav == 1){
        stop();
        printing.print(tr("Stop"));
        songChanged("");
    }
    if(command_stav == 2){
        next();
        printing.print(tr("Next song"));
    }
    if(command_stav == 3 ){
        previous();
        printing.print(tr("Previous song"));
    }
    if(command_stav == 4){
        pause();
        printing.print(tr("Paused"));
    }

}

/**
 * Finds recursively all files in directory
 * @brief musicMixer::getAllFiles
 * @param rootDir Dir to find files recursively
 * @return List of paths to all files in dir
 */
QStringList musicMixer::getAllFiles(QString rootDir){
    // pomocí qdiriteratiru vyhledá všechny soubory v dane slozce a vratí je v podobe qstringlistu
    QStringList files;
    QDirIterator it(rootDir, QStringList() << "*", QDir::Files, QDirIterator::Subdirectories);
    while(it.hasNext()) {
        files << it.next();
    }
    return files;
}

bool musicMixer::isActive(){
    return this->active;
}

