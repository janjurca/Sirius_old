#ifndef MUSICMIXER_H
#define MUSICMIXER_H

#include <QObject>
#include <QDir>
#include "mediaplayer.h"
#include "databasehandling.h"
#include "networkhandler.h"
#include <QDir>
#include <QDirIterator>
#include <QDebug>
#include <QString>
#include <QDateTime>
#include <QLabel>
#include <QLabel>
#include <thread>

class musicMixer : public mediaPlayer
{
    Q_OBJECT

public:
    musicMixer(QObject *parent = 0, QString musicDir = NULL);

public slots:
    void mixMusic(int count = 2);
    bool isActive();
    void command(QString command);

signals:
    void songChanged(QString msg);
    void searchCompleted();
private:
    bool isMusic = false;
    bool active = false;
    QString dir; ///Music directory
    QStringList allFiles; ///All files in music directory
    mediaPlayer *player;
    std::thread *searching_thread;
protected:
    void run();

private slots:
    int getRandomNumber(const int Min, const int Max);
    QStringList getAllFiles(QString rootDir);


};

#endif // MUSICMIXER_H
