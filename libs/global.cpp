#include "global.h"
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>
#include "configuration.h"
#include "language.h"

DatabaseHandling *db;
NetworkHandler *network;
SmtpClient *smtp;
Configuration *config;
Language *lang;
FileBasics filebasics;
Printing printing;
Devices *devices;

