/****************************************************************************
** Meta object code from reading C++ file 'levelmeterthread.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../AudioStreamingLibCore/levelmeterthread.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'levelmeterthread.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_LevelMeterThread_t {
    QByteArrayData data[10];
    char stringdata0[96];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LevelMeterThread_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LevelMeterThread_t qt_meta_stringdata_LevelMeterThread = {
    {
QT_MOC_LITERAL(0, 0, 16), // "LevelMeterThread"
QT_MOC_LITERAL(1, 17, 12), // "currentlevel"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 5), // "start"
QT_MOC_LITERAL(4, 37, 12), // "QAudioFormat"
QT_MOC_LITERAL(5, 50, 6), // "format"
QT_MOC_LITERAL(6, 57, 5), // "write"
QT_MOC_LITERAL(7, 63, 4), // "data"
QT_MOC_LITERAL(8, 68, 19), // "currentlevelPrivate"
QT_MOC_LITERAL(9, 88, 7) // "process"

    },
    "LevelMeterThread\0currentlevel\0\0start\0"
    "QAudioFormat\0format\0write\0data\0"
    "currentlevelPrivate\0process"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LevelMeterThread[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    1,   42,    2, 0x0a /* Public */,
       6,    1,   45,    2, 0x0a /* Public */,
       8,    0,   48,    2, 0x08 /* Private */,
       9,    0,   49,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QReal,    2,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, QMetaType::QByteArray,    7,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void LevelMeterThread::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        LevelMeterThread *_t = static_cast<LevelMeterThread *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->currentlevel((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 1: _t->start((*reinterpret_cast< const QAudioFormat(*)>(_a[1]))); break;
        case 2: _t->write((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 3: _t->currentlevelPrivate(); break;
        case 4: _t->process(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAudioFormat >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (LevelMeterThread::*_t)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LevelMeterThread::currentlevel)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject LevelMeterThread::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_LevelMeterThread.data,
      qt_meta_data_LevelMeterThread,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *LevelMeterThread::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LevelMeterThread::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LevelMeterThread.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int LevelMeterThread::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void LevelMeterThread::currentlevel(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
