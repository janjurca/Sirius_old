/****************************************************************************
** Meta object code from reading C++ file 'sslclient.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../AudioStreamingLibCore/sslclient.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'sslclient.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SslClient_t {
    QByteArrayData data[21];
    char stringdata0[223];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SslClient_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SslClient_t qt_meta_stringdata_SslClient = {
    {
QT_MOC_LITERAL(0, 0, 9), // "SslClient"
QT_MOC_LITERAL(1, 10, 5), // "abort"
QT_MOC_LITERAL(2, 16, 0), // ""
QT_MOC_LITERAL(3, 17, 13), // "connectToHost"
QT_MOC_LITERAL(4, 31, 4), // "host"
QT_MOC_LITERAL(5, 36, 4), // "port"
QT_MOC_LITERAL(6, 41, 18), // "negotiation_string"
QT_MOC_LITERAL(7, 60, 2), // "id"
QT_MOC_LITERAL(8, 63, 8), // "password"
QT_MOC_LITERAL(9, 72, 4), // "stop"
QT_MOC_LITERAL(10, 77, 5), // "write"
QT_MOC_LITERAL(11, 83, 4), // "data"
QT_MOC_LITERAL(12, 88, 7), // "testSsl"
QT_MOC_LITERAL(13, 96, 7), // "timeout"
QT_MOC_LITERAL(14, 104, 20), // "readyBeginEncryption"
QT_MOC_LITERAL(15, 125, 16), // "connectedPrivate"
QT_MOC_LITERAL(16, 142, 19), // "disconnectedPrivate"
QT_MOC_LITERAL(17, 162, 12), // "errorPrivate"
QT_MOC_LITERAL(18, 175, 28), // "QAbstractSocket::SocketError"
QT_MOC_LITERAL(19, 204, 1), // "e"
QT_MOC_LITERAL(20, 206, 16) // "readyReadPrivate"

    },
    "SslClient\0abort\0\0connectToHost\0host\0"
    "port\0negotiation_string\0id\0password\0"
    "stop\0write\0data\0testSsl\0timeout\0"
    "readyBeginEncryption\0connectedPrivate\0"
    "disconnectedPrivate\0errorPrivate\0"
    "QAbstractSocket::SocketError\0e\0"
    "readyReadPrivate"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SslClient[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x0a /* Public */,
       3,    5,   70,    2, 0x0a /* Public */,
       9,    0,   81,    2, 0x0a /* Public */,
      10,    1,   82,    2, 0x0a /* Public */,
      12,    0,   85,    2, 0x08 /* Private */,
      13,    0,   86,    2, 0x08 /* Private */,
      14,    0,   87,    2, 0x08 /* Private */,
      15,    0,   88,    2, 0x08 /* Private */,
      16,    0,   89,    2, 0x08 /* Private */,
      17,    1,   90,    2, 0x08 /* Private */,
      20,    0,   93,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::UShort, QMetaType::QByteArray, QMetaType::QString, QMetaType::QByteArray,    4,    5,    6,    7,    8,
    QMetaType::Void,
    QMetaType::Int, QMetaType::QByteArray,   11,
    QMetaType::Bool,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 18,   19,
    QMetaType::Void,

       0        // eod
};

void SslClient::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SslClient *_t = static_cast<SslClient *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->abort(); break;
        case 1: _t->connectToHost((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2])),(*reinterpret_cast< const QByteArray(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4])),(*reinterpret_cast< const QByteArray(*)>(_a[5]))); break;
        case 2: _t->stop(); break;
        case 3: { int _r = _t->write((*reinterpret_cast< const QByteArray(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 4: { bool _r = _t->testSsl();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 5: _t->timeout(); break;
        case 6: _t->readyBeginEncryption(); break;
        case 7: _t->connectedPrivate(); break;
        case 8: _t->disconnectedPrivate(); break;
        case 9: _t->errorPrivate((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        case 10: _t->readyReadPrivate(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 9:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractSocket::SocketError >(); break;
            }
            break;
        }
    }
}

const QMetaObject SslClient::staticMetaObject = {
    { &AbstractClient::staticMetaObject, qt_meta_stringdata_SslClient.data,
      qt_meta_data_SslClient,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SslClient::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SslClient::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SslClient.stringdata0))
        return static_cast<void*>(this);
    return AbstractClient::qt_metacast(_clname);
}

int SslClient::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = AbstractClient::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
