/****************************************************************************
** Meta object code from reading C++ file 'worker.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../AudioStreamingLibCore/worker.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'worker.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Worker_t {
    QByteArrayData data[72];
    char stringdata0[947];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Worker_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Worker_t qt_meta_stringdata_Worker = {
    {
QT_MOC_LITERAL(0, 0, 6), // "Worker"
QT_MOC_LITERAL(1, 7, 9), // "connected"
QT_MOC_LITERAL(2, 17, 0), // ""
QT_MOC_LITERAL(3, 18, 12), // "QHostAddress"
QT_MOC_LITERAL(4, 31, 12), // "disconnected"
QT_MOC_LITERAL(5, 44, 7), // "pending"
QT_MOC_LITERAL(6, 52, 9), // "inputData"
QT_MOC_LITERAL(7, 62, 13), // "veryInputData"
QT_MOC_LITERAL(8, 76, 10), // "outputData"
QT_MOC_LITERAL(9, 87, 14), // "veryOutputData"
QT_MOC_LITERAL(10, 102, 9), // "extraData"
QT_MOC_LITERAL(11, 112, 10), // "inputLevel"
QT_MOC_LITERAL(12, 123, 11), // "outputLevel"
QT_MOC_LITERAL(13, 135, 14), // "adjustSettings"
QT_MOC_LITERAL(14, 150, 16), // "extraDataWritten"
QT_MOC_LITERAL(15, 167, 5), // "error"
QT_MOC_LITERAL(16, 173, 5), // "start"
QT_MOC_LITERAL(17, 179, 13), // "StreamingInfo"
QT_MOC_LITERAL(18, 193, 14), // "streaming_info"
QT_MOC_LITERAL(19, 208, 7), // "setKeys"
QT_MOC_LITERAL(20, 216, 11), // "private_key"
QT_MOC_LITERAL(21, 228, 10), // "public_key"
QT_MOC_LITERAL(22, 239, 6), // "listen"
QT_MOC_LITERAL(23, 246, 4), // "port"
QT_MOC_LITERAL(24, 251, 11), // "auto_accept"
QT_MOC_LITERAL(25, 263, 8), // "password"
QT_MOC_LITERAL(26, 272, 15), // "max_connections"
QT_MOC_LITERAL(27, 288, 13), // "connectToHost"
QT_MOC_LITERAL(28, 302, 4), // "host"
QT_MOC_LITERAL(29, 307, 16), // "acceptConnection"
QT_MOC_LITERAL(30, 324, 16), // "rejectConnection"
QT_MOC_LITERAL(31, 341, 14), // "writeExtraData"
QT_MOC_LITERAL(32, 356, 4), // "data"
QT_MOC_LITERAL(33, 361, 20), // "writeExtraDataResult"
QT_MOC_LITERAL(34, 382, 13), // "inputDataBack"
QT_MOC_LITERAL(35, 396, 14), // "outputDataBack"
QT_MOC_LITERAL(36, 411, 12), // "isInputMuted"
QT_MOC_LITERAL(37, 424, 13), // "setInputMuted"
QT_MOC_LITERAL(38, 438, 4), // "mute"
QT_MOC_LITERAL(39, 443, 6), // "volume"
QT_MOC_LITERAL(40, 450, 9), // "setVolume"
QT_MOC_LITERAL(41, 460, 13), // "streamingInfo"
QT_MOC_LITERAL(42, 474, 15), // "connectionsList"
QT_MOC_LITERAL(43, 490, 19), // "QList<QHostAddress>"
QT_MOC_LITERAL(44, 510, 23), // "isReadyToWriteExtraData"
QT_MOC_LITERAL(45, 534, 12), // "errorPrivate"
QT_MOC_LITERAL(46, 547, 17), // "error_description"
QT_MOC_LITERAL(47, 565, 16), // "startOpusEncoder"
QT_MOC_LITERAL(48, 582, 16), // "startOpusDecoder"
QT_MOC_LITERAL(49, 599, 21), // "adjustSettingsPrivate"
QT_MOC_LITERAL(50, 621, 18), // "start_opus_encoder"
QT_MOC_LITERAL(51, 640, 18), // "start_opus_decoder"
QT_MOC_LITERAL(52, 659, 11), // "client_mode"
QT_MOC_LITERAL(53, 671, 21), // "serverClientConencted"
QT_MOC_LITERAL(54, 693, 8), // "PeerData"
QT_MOC_LITERAL(55, 702, 2), // "pd"
QT_MOC_LITERAL(56, 705, 2), // "id"
QT_MOC_LITERAL(57, 708, 24), // "serverClientDisconencted"
QT_MOC_LITERAL(58, 733, 15), // "clientConencted"
QT_MOC_LITERAL(59, 749, 18), // "clientDisconencted"
QT_MOC_LITERAL(60, 768, 17), // "posProcessedInput"
QT_MOC_LITERAL(61, 786, 16), // "preProcessOutput"
QT_MOC_LITERAL(62, 803, 18), // "posProcessedOutput"
QT_MOC_LITERAL(63, 822, 11), // "flowControl"
QT_MOC_LITERAL(64, 834, 5), // "bytes"
QT_MOC_LITERAL(65, 840, 18), // "processServerInput"
QT_MOC_LITERAL(66, 859, 18), // "processClientInput"
QT_MOC_LITERAL(67, 878, 12), // "createHeader"
QT_MOC_LITERAL(68, 891, 6), // "header"
QT_MOC_LITERAL(69, 898, 13), // "QAudioFormat*"
QT_MOC_LITERAL(70, 912, 19), // "refInputAudioFormat"
QT_MOC_LITERAL(71, 932, 14) // "refAudioFormat"

    },
    "Worker\0connected\0\0QHostAddress\0"
    "disconnected\0pending\0inputData\0"
    "veryInputData\0outputData\0veryOutputData\0"
    "extraData\0inputLevel\0outputLevel\0"
    "adjustSettings\0extraDataWritten\0error\0"
    "start\0StreamingInfo\0streaming_info\0"
    "setKeys\0private_key\0public_key\0listen\0"
    "port\0auto_accept\0password\0max_connections\0"
    "connectToHost\0host\0acceptConnection\0"
    "rejectConnection\0writeExtraData\0data\0"
    "writeExtraDataResult\0inputDataBack\0"
    "outputDataBack\0isInputMuted\0setInputMuted\0"
    "mute\0volume\0setVolume\0streamingInfo\0"
    "connectionsList\0QList<QHostAddress>\0"
    "isReadyToWriteExtraData\0errorPrivate\0"
    "error_description\0startOpusEncoder\0"
    "startOpusDecoder\0adjustSettingsPrivate\0"
    "start_opus_encoder\0start_opus_decoder\0"
    "client_mode\0serverClientConencted\0"
    "PeerData\0pd\0id\0serverClientDisconencted\0"
    "clientConencted\0clientDisconencted\0"
    "posProcessedInput\0preProcessOutput\0"
    "posProcessedOutput\0flowControl\0bytes\0"
    "processServerInput\0processClientInput\0"
    "createHeader\0header\0QAudioFormat*\0"
    "refInputAudioFormat\0refAudioFormat"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Worker[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      46,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      13,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,  244,    2, 0x06 /* Public */,
       4,    1,  249,    2, 0x06 /* Public */,
       5,    2,  252,    2, 0x06 /* Public */,
       6,    1,  257,    2, 0x06 /* Public */,
       7,    1,  260,    2, 0x06 /* Public */,
       8,    1,  263,    2, 0x06 /* Public */,
       9,    1,  266,    2, 0x06 /* Public */,
      10,    1,  269,    2, 0x06 /* Public */,
      11,    1,  272,    2, 0x06 /* Public */,
      12,    1,  275,    2, 0x06 /* Public */,
      13,    0,  278,    2, 0x06 /* Public */,
      14,    0,  279,    2, 0x06 /* Public */,
      15,    1,  280,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      16,    1,  283,    2, 0x0a /* Public */,
      19,    2,  286,    2, 0x0a /* Public */,
      22,    4,  291,    2, 0x0a /* Public */,
      27,    3,  300,    2, 0x0a /* Public */,
      29,    0,  307,    2, 0x0a /* Public */,
      30,    0,  308,    2, 0x0a /* Public */,
      31,    1,  309,    2, 0x0a /* Public */,
      33,    0,  312,    2, 0x0a /* Public */,
      34,    1,  313,    2, 0x0a /* Public */,
      35,    1,  316,    2, 0x0a /* Public */,
      36,    0,  319,    2, 0x0a /* Public */,
      37,    1,  320,    2, 0x0a /* Public */,
      39,    0,  323,    2, 0x0a /* Public */,
      40,    1,  324,    2, 0x0a /* Public */,
      41,    0,  327,    2, 0x0a /* Public */,
      42,    0,  328,    2, 0x0a /* Public */,
      44,    0,  329,    2, 0x0a /* Public */,
      45,    1,  330,    2, 0x08 /* Private */,
      47,    0,  333,    2, 0x08 /* Private */,
      48,    0,  334,    2, 0x08 /* Private */,
      49,    3,  335,    2, 0x08 /* Private */,
      53,    2,  342,    2, 0x08 /* Private */,
      57,    1,  347,    2, 0x08 /* Private */,
      58,    2,  350,    2, 0x08 /* Private */,
      59,    1,  355,    2, 0x08 /* Private */,
      60,    1,  358,    2, 0x08 /* Private */,
      61,    1,  361,    2, 0x08 /* Private */,
      62,    1,  364,    2, 0x08 /* Private */,
      63,    1,  367,    2, 0x08 /* Private */,
      65,    1,  370,    2, 0x08 /* Private */,
      66,    1,  373,    2, 0x08 /* Private */,
      67,    0,  376,    2, 0x08 /* Private */,
      68,    3,  377,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, QMetaType::QString,    2,    2,
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 3, QMetaType::QString,    2,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 17,   18,
    QMetaType::Void, QMetaType::QByteArray, QMetaType::QByteArray,   20,   21,
    QMetaType::Void, QMetaType::UShort, QMetaType::Bool, QMetaType::QByteArray, QMetaType::Int,   23,   24,   25,   26,
    QMetaType::Void, QMetaType::QString, QMetaType::UShort, QMetaType::QByteArray,   28,   23,   25,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,   32,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,   32,
    QMetaType::Void, QMetaType::QByteArray,   32,
    QMetaType::Bool,
    QMetaType::Void, QMetaType::Bool,   38,
    QMetaType::Int,
    QMetaType::Void, QMetaType::Int,   39,
    0x80000000 | 17,
    0x80000000 | 43,
    QMetaType::Bool,
    QMetaType::Void, QMetaType::QString,   46,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool, QMetaType::Bool, QMetaType::Bool,   50,   51,   52,
    QMetaType::Void, 0x80000000 | 54, QMetaType::QString,   55,   56,
    QMetaType::Void, 0x80000000 | 54,   55,
    QMetaType::Void, 0x80000000 | 54, QMetaType::QString,   55,   56,
    QMetaType::Void, 0x80000000 | 54,   55,
    QMetaType::Void, QMetaType::QByteArray,   32,
    QMetaType::Void, QMetaType::QByteArray,   32,
    QMetaType::Void, QMetaType::QByteArray,   32,
    QMetaType::Void, QMetaType::Int,   64,
    QMetaType::Void, 0x80000000 | 54,   55,
    QMetaType::Void, 0x80000000 | 54,   55,
    QMetaType::QByteArray,
    QMetaType::Void, QMetaType::QByteArray, 0x80000000 | 69, 0x80000000 | 69,   32,   70,   71,

       0        // eod
};

void Worker::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Worker *_t = static_cast<Worker *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->connected((*reinterpret_cast< QHostAddress(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 1: _t->disconnected((*reinterpret_cast< QHostAddress(*)>(_a[1]))); break;
        case 2: _t->pending((*reinterpret_cast< QHostAddress(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 3: _t->inputData((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 4: _t->veryInputData((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 5: _t->outputData((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 6: _t->veryOutputData((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 7: _t->extraData((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 8: _t->inputLevel((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 9: _t->outputLevel((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 10: _t->adjustSettings(); break;
        case 11: _t->extraDataWritten(); break;
        case 12: _t->error((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 13: _t->start((*reinterpret_cast< const StreamingInfo(*)>(_a[1]))); break;
        case 14: _t->setKeys((*reinterpret_cast< const QByteArray(*)>(_a[1])),(*reinterpret_cast< const QByteArray(*)>(_a[2]))); break;
        case 15: _t->listen((*reinterpret_cast< quint16(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< const QByteArray(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 16: _t->connectToHost((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2])),(*reinterpret_cast< const QByteArray(*)>(_a[3]))); break;
        case 17: _t->acceptConnection(); break;
        case 18: _t->rejectConnection(); break;
        case 19: _t->writeExtraData((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 20: _t->writeExtraDataResult(); break;
        case 21: _t->inputDataBack((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 22: _t->outputDataBack((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 23: { bool _r = _t->isInputMuted();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 24: _t->setInputMuted((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 25: { int _r = _t->volume();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 26: _t->setVolume((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 27: { StreamingInfo _r = _t->streamingInfo();
            if (_a[0]) *reinterpret_cast< StreamingInfo*>(_a[0]) = std::move(_r); }  break;
        case 28: { QList<QHostAddress> _r = _t->connectionsList();
            if (_a[0]) *reinterpret_cast< QList<QHostAddress>*>(_a[0]) = std::move(_r); }  break;
        case 29: { bool _r = _t->isReadyToWriteExtraData();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 30: _t->errorPrivate((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 31: _t->startOpusEncoder(); break;
        case 32: _t->startOpusDecoder(); break;
        case 33: _t->adjustSettingsPrivate((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        case 34: _t->serverClientConencted((*reinterpret_cast< const PeerData(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 35: _t->serverClientDisconencted((*reinterpret_cast< const PeerData(*)>(_a[1]))); break;
        case 36: _t->clientConencted((*reinterpret_cast< const PeerData(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 37: _t->clientDisconencted((*reinterpret_cast< const PeerData(*)>(_a[1]))); break;
        case 38: _t->posProcessedInput((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 39: _t->preProcessOutput((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 40: _t->posProcessedOutput((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 41: _t->flowControl((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 42: _t->processServerInput((*reinterpret_cast< const PeerData(*)>(_a[1]))); break;
        case 43: _t->processClientInput((*reinterpret_cast< const PeerData(*)>(_a[1]))); break;
        case 44: { QByteArray _r = _t->createHeader();
            if (_a[0]) *reinterpret_cast< QByteArray*>(_a[0]) = std::move(_r); }  break;
        case 45: _t->header((*reinterpret_cast< QByteArray(*)>(_a[1])),(*reinterpret_cast< QAudioFormat*(*)>(_a[2])),(*reinterpret_cast< QAudioFormat*(*)>(_a[3]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (Worker::*_t)(QHostAddress , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Worker::connected)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (Worker::*_t)(QHostAddress );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Worker::disconnected)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (Worker::*_t)(QHostAddress , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Worker::pending)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (Worker::*_t)(QByteArray );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Worker::inputData)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (Worker::*_t)(QByteArray );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Worker::veryInputData)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (Worker::*_t)(QByteArray );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Worker::outputData)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (Worker::*_t)(QByteArray );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Worker::veryOutputData)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (Worker::*_t)(QByteArray );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Worker::extraData)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (Worker::*_t)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Worker::inputLevel)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (Worker::*_t)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Worker::outputLevel)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (Worker::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Worker::adjustSettings)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (Worker::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Worker::extraDataWritten)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (Worker::*_t)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Worker::error)) {
                *result = 12;
                return;
            }
        }
    }
}

const QMetaObject Worker::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Worker.data,
      qt_meta_data_Worker,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Worker::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Worker::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Worker.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Worker::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 46)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 46;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 46)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 46;
    }
    return _id;
}

// SIGNAL 0
void Worker::connected(QHostAddress _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Worker::disconnected(QHostAddress _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Worker::pending(QHostAddress _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Worker::inputData(QByteArray _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Worker::veryInputData(QByteArray _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Worker::outputData(QByteArray _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Worker::veryOutputData(QByteArray _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Worker::extraData(QByteArray _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void Worker::inputLevel(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void Worker::outputLevel(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void Worker::adjustSettings()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void Worker::extraDataWritten()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}

// SIGNAL 12
void Worker::error(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
