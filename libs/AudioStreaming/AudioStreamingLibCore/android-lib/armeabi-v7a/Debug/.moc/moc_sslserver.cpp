/****************************************************************************
** Meta object code from reading C++ file 'sslserver.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../AudioStreamingLibCore/sslserver.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'sslserver.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SslServer_t {
    QByteArrayData data[31];
    char stringdata0[347];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SslServer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SslServer_t qt_meta_stringdata_SslServer = {
    {
QT_MOC_LITERAL(0, 0, 9), // "SslServer"
QT_MOC_LITERAL(1, 10, 5), // "abort"
QT_MOC_LITERAL(2, 16, 0), // ""
QT_MOC_LITERAL(3, 17, 7), // "qintptr"
QT_MOC_LITERAL(4, 25, 10), // "descriptor"
QT_MOC_LITERAL(5, 36, 7), // "setKeys"
QT_MOC_LITERAL(6, 44, 11), // "private_key"
QT_MOC_LITERAL(7, 56, 10), // "public_key"
QT_MOC_LITERAL(8, 67, 6), // "listen"
QT_MOC_LITERAL(9, 74, 4), // "port"
QT_MOC_LITERAL(10, 79, 11), // "auto_accept"
QT_MOC_LITERAL(11, 91, 15), // "max_connections"
QT_MOC_LITERAL(12, 107, 18), // "negotiation_string"
QT_MOC_LITERAL(13, 126, 2), // "id"
QT_MOC_LITERAL(14, 129, 8), // "password"
QT_MOC_LITERAL(15, 138, 4), // "stop"
QT_MOC_LITERAL(16, 143, 19), // "rejectNewConnection"
QT_MOC_LITERAL(17, 163, 19), // "acceptNewConnection"
QT_MOC_LITERAL(18, 183, 11), // "writeToHost"
QT_MOC_LITERAL(19, 195, 4), // "data"
QT_MOC_LITERAL(20, 200, 10), // "writeToAll"
QT_MOC_LITERAL(21, 211, 7), // "testSsl"
QT_MOC_LITERAL(22, 219, 20), // "newConnectionPrivate"
QT_MOC_LITERAL(23, 240, 20), // "readyBeginEncryption"
QT_MOC_LITERAL(24, 261, 8), // "verifier"
QT_MOC_LITERAL(25, 270, 7), // "timeout"
QT_MOC_LITERAL(26, 278, 16), // "readyReadPrivate"
QT_MOC_LITERAL(27, 295, 19), // "disconnectedPrivate"
QT_MOC_LITERAL(28, 315, 12), // "removeSocket"
QT_MOC_LITERAL(29, 328, 11), // "QTcpSocket*"
QT_MOC_LITERAL(30, 340, 6) // "socket"

    },
    "SslServer\0abort\0\0qintptr\0descriptor\0"
    "setKeys\0private_key\0public_key\0listen\0"
    "port\0auto_accept\0max_connections\0"
    "negotiation_string\0id\0password\0stop\0"
    "rejectNewConnection\0acceptNewConnection\0"
    "writeToHost\0data\0writeToAll\0testSsl\0"
    "newConnectionPrivate\0readyBeginEncryption\0"
    "verifier\0timeout\0readyReadPrivate\0"
    "disconnectedPrivate\0removeSocket\0"
    "QTcpSocket*\0socket"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SslServer[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   94,    2, 0x0a /* Public */,
       5,    2,   97,    2, 0x0a /* Public */,
       8,    6,  102,    2, 0x0a /* Public */,
      15,    0,  115,    2, 0x0a /* Public */,
      16,    0,  116,    2, 0x0a /* Public */,
      17,    0,  117,    2, 0x0a /* Public */,
      18,    2,  118,    2, 0x0a /* Public */,
      20,    1,  123,    2, 0x0a /* Public */,
      21,    0,  126,    2, 0x08 /* Private */,
      22,    1,  127,    2, 0x08 /* Private */,
      23,    0,  130,    2, 0x08 /* Private */,
      24,    0,  131,    2, 0x08 /* Private */,
      25,    0,  132,    2, 0x08 /* Private */,
      26,    0,  133,    2, 0x08 /* Private */,
      27,    0,  134,    2, 0x08 /* Private */,
      28,    1,  135,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::QByteArray, QMetaType::QByteArray,    6,    7,
    QMetaType::Void, QMetaType::UShort, QMetaType::Bool, QMetaType::Int, QMetaType::QByteArray, QMetaType::QString, QMetaType::QByteArray,    9,   10,   11,   12,   13,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray, 0x80000000 | 3,   19,    4,
    QMetaType::Int, QMetaType::QByteArray,   19,
    QMetaType::Bool,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 29,   30,

       0        // eod
};

void SslServer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SslServer *_t = static_cast<SslServer *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->abort((*reinterpret_cast< qintptr(*)>(_a[1]))); break;
        case 1: _t->setKeys((*reinterpret_cast< const QByteArray(*)>(_a[1])),(*reinterpret_cast< const QByteArray(*)>(_a[2]))); break;
        case 2: _t->listen((*reinterpret_cast< quint16(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< const QByteArray(*)>(_a[4])),(*reinterpret_cast< const QString(*)>(_a[5])),(*reinterpret_cast< const QByteArray(*)>(_a[6]))); break;
        case 3: _t->stop(); break;
        case 4: _t->rejectNewConnection(); break;
        case 5: _t->acceptNewConnection(); break;
        case 6: _t->writeToHost((*reinterpret_cast< const QByteArray(*)>(_a[1])),(*reinterpret_cast< qintptr(*)>(_a[2]))); break;
        case 7: { int _r = _t->writeToAll((*reinterpret_cast< const QByteArray(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 8: { bool _r = _t->testSsl();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 9: _t->newConnectionPrivate((*reinterpret_cast< qintptr(*)>(_a[1]))); break;
        case 10: _t->readyBeginEncryption(); break;
        case 11: _t->verifier(); break;
        case 12: _t->timeout(); break;
        case 13: _t->readyReadPrivate(); break;
        case 14: _t->disconnectedPrivate(); break;
        case 15: _t->removeSocket((*reinterpret_cast< QTcpSocket*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 15:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QTcpSocket* >(); break;
            }
            break;
        }
    }
}

const QMetaObject SslServer::staticMetaObject = {
    { &AbstractServer::staticMetaObject, qt_meta_stringdata_SslServer.data,
      qt_meta_data_SslServer,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SslServer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SslServer::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SslServer.stringdata0))
        return static_cast<void*>(this);
    return AbstractServer::qt_metacast(_clname);
}

int SslServer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = AbstractServer::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
