/****************************************************************************
** Meta object code from reading C++ file 'server.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../AudioStreamingLibCore/server.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'server.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Server_t {
    QByteArrayData data[29];
    char stringdata0[315];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Server_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Server_t qt_meta_stringdata_Server = {
    {
QT_MOC_LITERAL(0, 0, 6), // "Server"
QT_MOC_LITERAL(1, 7, 5), // "abort"
QT_MOC_LITERAL(2, 13, 0), // ""
QT_MOC_LITERAL(3, 14, 7), // "qintptr"
QT_MOC_LITERAL(4, 22, 10), // "descriptor"
QT_MOC_LITERAL(5, 33, 7), // "setKeys"
QT_MOC_LITERAL(6, 41, 11), // "private_key"
QT_MOC_LITERAL(7, 53, 10), // "public_key"
QT_MOC_LITERAL(8, 64, 6), // "listen"
QT_MOC_LITERAL(9, 71, 4), // "port"
QT_MOC_LITERAL(10, 76, 11), // "auto_accept"
QT_MOC_LITERAL(11, 88, 15), // "max_connections"
QT_MOC_LITERAL(12, 104, 18), // "negotiation_string"
QT_MOC_LITERAL(13, 123, 2), // "id"
QT_MOC_LITERAL(14, 126, 8), // "password"
QT_MOC_LITERAL(15, 135, 4), // "stop"
QT_MOC_LITERAL(16, 140, 19), // "rejectNewConnection"
QT_MOC_LITERAL(17, 160, 19), // "acceptNewConnection"
QT_MOC_LITERAL(18, 180, 11), // "writeToHost"
QT_MOC_LITERAL(19, 192, 4), // "data"
QT_MOC_LITERAL(20, 197, 10), // "writeToAll"
QT_MOC_LITERAL(21, 208, 20), // "newConnectionPrivate"
QT_MOC_LITERAL(22, 229, 8), // "verifier"
QT_MOC_LITERAL(23, 238, 7), // "timeout"
QT_MOC_LITERAL(24, 246, 16), // "readyReadPrivate"
QT_MOC_LITERAL(25, 263, 19), // "disconnectedPrivate"
QT_MOC_LITERAL(26, 283, 12), // "removeSocket"
QT_MOC_LITERAL(27, 296, 11), // "QTcpSocket*"
QT_MOC_LITERAL(28, 308, 6) // "socket"

    },
    "Server\0abort\0\0qintptr\0descriptor\0"
    "setKeys\0private_key\0public_key\0listen\0"
    "port\0auto_accept\0max_connections\0"
    "negotiation_string\0id\0password\0stop\0"
    "rejectNewConnection\0acceptNewConnection\0"
    "writeToHost\0data\0writeToAll\0"
    "newConnectionPrivate\0verifier\0timeout\0"
    "readyReadPrivate\0disconnectedPrivate\0"
    "removeSocket\0QTcpSocket*\0socket"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Server[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   84,    2, 0x0a /* Public */,
       5,    2,   87,    2, 0x0a /* Public */,
       8,    6,   92,    2, 0x0a /* Public */,
      15,    0,  105,    2, 0x0a /* Public */,
      16,    0,  106,    2, 0x0a /* Public */,
      17,    0,  107,    2, 0x0a /* Public */,
      18,    2,  108,    2, 0x0a /* Public */,
      20,    1,  113,    2, 0x0a /* Public */,
      21,    1,  116,    2, 0x08 /* Private */,
      22,    0,  119,    2, 0x08 /* Private */,
      23,    0,  120,    2, 0x08 /* Private */,
      24,    0,  121,    2, 0x08 /* Private */,
      25,    0,  122,    2, 0x08 /* Private */,
      26,    1,  123,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::QByteArray, QMetaType::QByteArray,    6,    7,
    QMetaType::Void, QMetaType::UShort, QMetaType::Bool, QMetaType::Int, QMetaType::QByteArray, QMetaType::QString, QMetaType::QByteArray,    9,   10,   11,   12,   13,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray, 0x80000000 | 3,   19,    4,
    QMetaType::Int, QMetaType::QByteArray,   19,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 27,   28,

       0        // eod
};

void Server::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Server *_t = static_cast<Server *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->abort((*reinterpret_cast< qintptr(*)>(_a[1]))); break;
        case 1: _t->setKeys((*reinterpret_cast< const QByteArray(*)>(_a[1])),(*reinterpret_cast< const QByteArray(*)>(_a[2]))); break;
        case 2: _t->listen((*reinterpret_cast< quint16(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< const QByteArray(*)>(_a[4])),(*reinterpret_cast< const QString(*)>(_a[5])),(*reinterpret_cast< const QByteArray(*)>(_a[6]))); break;
        case 3: _t->stop(); break;
        case 4: _t->rejectNewConnection(); break;
        case 5: _t->acceptNewConnection(); break;
        case 6: _t->writeToHost((*reinterpret_cast< const QByteArray(*)>(_a[1])),(*reinterpret_cast< qintptr(*)>(_a[2]))); break;
        case 7: { int _r = _t->writeToAll((*reinterpret_cast< const QByteArray(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 8: _t->newConnectionPrivate((*reinterpret_cast< qintptr(*)>(_a[1]))); break;
        case 9: _t->verifier(); break;
        case 10: _t->timeout(); break;
        case 11: _t->readyReadPrivate(); break;
        case 12: _t->disconnectedPrivate(); break;
        case 13: _t->removeSocket((*reinterpret_cast< QTcpSocket*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 13:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QTcpSocket* >(); break;
            }
            break;
        }
    }
}

const QMetaObject Server::staticMetaObject = {
    { &AbstractServer::staticMetaObject, qt_meta_stringdata_Server.data,
      qt_meta_data_Server,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Server::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Server::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Server.stringdata0))
        return static_cast<void*>(this);
    return AbstractServer::qt_metacast(_clname);
}

int Server::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = AbstractServer::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
