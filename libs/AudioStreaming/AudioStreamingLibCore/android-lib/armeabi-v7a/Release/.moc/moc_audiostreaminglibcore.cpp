/****************************************************************************
** Meta object code from reading C++ file 'audiostreaminglibcore.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../AudioStreamingLibCore/audiostreaminglibcore.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'audiostreaminglibcore.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_AudioStreamingLibCore_t {
    QByteArrayData data[54];
    char stringdata0[664];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_AudioStreamingLibCore_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_AudioStreamingLibCore_t qt_meta_stringdata_AudioStreamingLibCore = {
    {
QT_MOC_LITERAL(0, 0, 21), // "AudioStreamingLibCore"
QT_MOC_LITERAL(1, 22, 9), // "connected"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 12), // "QHostAddress"
QT_MOC_LITERAL(4, 46, 12), // "disconnected"
QT_MOC_LITERAL(5, 59, 7), // "pending"
QT_MOC_LITERAL(6, 67, 9), // "inputData"
QT_MOC_LITERAL(7, 77, 13), // "veryInputData"
QT_MOC_LITERAL(8, 91, 10), // "outputData"
QT_MOC_LITERAL(9, 102, 14), // "veryOutputData"
QT_MOC_LITERAL(10, 117, 9), // "extraData"
QT_MOC_LITERAL(11, 127, 10), // "inputLevel"
QT_MOC_LITERAL(12, 138, 11), // "outputLevel"
QT_MOC_LITERAL(13, 150, 14), // "adjustSettings"
QT_MOC_LITERAL(14, 165, 16), // "extraDataWritten"
QT_MOC_LITERAL(15, 182, 8), // "finished"
QT_MOC_LITERAL(16, 191, 5), // "error"
QT_MOC_LITERAL(17, 197, 5), // "start"
QT_MOC_LITERAL(18, 203, 13), // "StreamingInfo"
QT_MOC_LITERAL(19, 217, 14), // "streaming_info"
QT_MOC_LITERAL(20, 232, 4), // "stop"
QT_MOC_LITERAL(21, 237, 9), // "isRunning"
QT_MOC_LITERAL(22, 247, 16), // "discoverInstance"
QT_MOC_LITERAL(23, 264, 15), // "DiscoverClient*"
QT_MOC_LITERAL(24, 280, 7), // "setKeys"
QT_MOC_LITERAL(25, 288, 11), // "private_key"
QT_MOC_LITERAL(26, 300, 10), // "public_key"
QT_MOC_LITERAL(27, 311, 6), // "listen"
QT_MOC_LITERAL(28, 318, 4), // "port"
QT_MOC_LITERAL(29, 323, 11), // "auto_accept"
QT_MOC_LITERAL(30, 335, 8), // "password"
QT_MOC_LITERAL(31, 344, 15), // "max_connections"
QT_MOC_LITERAL(32, 360, 13), // "connectToHost"
QT_MOC_LITERAL(33, 374, 4), // "host"
QT_MOC_LITERAL(34, 379, 16), // "acceptConnection"
QT_MOC_LITERAL(35, 396, 16), // "rejectConnection"
QT_MOC_LITERAL(36, 413, 14), // "writeExtraData"
QT_MOC_LITERAL(37, 428, 4), // "data"
QT_MOC_LITERAL(38, 433, 20), // "writeExtraDataResult"
QT_MOC_LITERAL(39, 454, 13), // "inputDataBack"
QT_MOC_LITERAL(40, 468, 14), // "outputDataBack"
QT_MOC_LITERAL(41, 483, 12), // "isInputMuted"
QT_MOC_LITERAL(42, 496, 13), // "setInputMuted"
QT_MOC_LITERAL(43, 510, 4), // "mute"
QT_MOC_LITERAL(44, 515, 6), // "volume"
QT_MOC_LITERAL(45, 522, 9), // "setVolume"
QT_MOC_LITERAL(46, 532, 13), // "streamingInfo"
QT_MOC_LITERAL(47, 546, 15), // "connectionsList"
QT_MOC_LITERAL(48, 562, 19), // "QList<QHostAddress>"
QT_MOC_LITERAL(49, 582, 23), // "isReadyToWriteExtraData"
QT_MOC_LITERAL(50, 606, 11), // "audioFormat"
QT_MOC_LITERAL(51, 618, 12), // "QAudioFormat"
QT_MOC_LITERAL(52, 631, 16), // "inputAudioFormat"
QT_MOC_LITERAL(53, 648, 15) // "finishedPrivate"

    },
    "AudioStreamingLibCore\0connected\0\0"
    "QHostAddress\0disconnected\0pending\0"
    "inputData\0veryInputData\0outputData\0"
    "veryOutputData\0extraData\0inputLevel\0"
    "outputLevel\0adjustSettings\0extraDataWritten\0"
    "finished\0error\0start\0StreamingInfo\0"
    "streaming_info\0stop\0isRunning\0"
    "discoverInstance\0DiscoverClient*\0"
    "setKeys\0private_key\0public_key\0listen\0"
    "port\0auto_accept\0password\0max_connections\0"
    "connectToHost\0host\0acceptConnection\0"
    "rejectConnection\0writeExtraData\0data\0"
    "writeExtraDataResult\0inputDataBack\0"
    "outputDataBack\0isInputMuted\0setInputMuted\0"
    "mute\0volume\0setVolume\0streamingInfo\0"
    "connectionsList\0QList<QHostAddress>\0"
    "isReadyToWriteExtraData\0audioFormat\0"
    "QAudioFormat\0inputAudioFormat\0"
    "finishedPrivate"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_AudioStreamingLibCore[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      41,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      14,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,  219,    2, 0x06 /* Public */,
       4,    1,  224,    2, 0x06 /* Public */,
       5,    2,  227,    2, 0x06 /* Public */,
       6,    1,  232,    2, 0x06 /* Public */,
       7,    1,  235,    2, 0x06 /* Public */,
       8,    1,  238,    2, 0x06 /* Public */,
       9,    1,  241,    2, 0x06 /* Public */,
      10,    1,  244,    2, 0x06 /* Public */,
      11,    1,  247,    2, 0x06 /* Public */,
      12,    1,  250,    2, 0x06 /* Public */,
      13,    0,  253,    2, 0x06 /* Public */,
      14,    0,  254,    2, 0x06 /* Public */,
      15,    0,  255,    2, 0x06 /* Public */,
      16,    1,  256,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      17,    1,  259,    2, 0x0a /* Public */,
      20,    0,  262,    2, 0x0a /* Public */,
      21,    0,  263,    2, 0x0a /* Public */,
      22,    0,  264,    2, 0x0a /* Public */,
      24,    2,  265,    2, 0x0a /* Public */,
      27,    4,  270,    2, 0x0a /* Public */,
      27,    3,  279,    2, 0x2a /* Public | MethodCloned */,
      27,    2,  286,    2, 0x2a /* Public | MethodCloned */,
      27,    1,  291,    2, 0x2a /* Public | MethodCloned */,
      32,    3,  294,    2, 0x0a /* Public */,
      32,    2,  301,    2, 0x2a /* Public | MethodCloned */,
      34,    0,  306,    2, 0x0a /* Public */,
      35,    0,  307,    2, 0x0a /* Public */,
      36,    1,  308,    2, 0x0a /* Public */,
      38,    0,  311,    2, 0x0a /* Public */,
      39,    1,  312,    2, 0x0a /* Public */,
      40,    1,  315,    2, 0x0a /* Public */,
      41,    0,  318,    2, 0x0a /* Public */,
      42,    1,  319,    2, 0x0a /* Public */,
      44,    0,  322,    2, 0x0a /* Public */,
      45,    1,  323,    2, 0x0a /* Public */,
      46,    0,  326,    2, 0x0a /* Public */,
      47,    0,  327,    2, 0x0a /* Public */,
      49,    0,  328,    2, 0x0a /* Public */,
      50,    0,  329,    2, 0x0a /* Public */,
      52,    0,  330,    2, 0x0a /* Public */,
      53,    0,  331,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, QMetaType::QString,    2,    2,
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 3, QMetaType::QString,    2,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,

 // slots: parameters
    QMetaType::Bool, 0x80000000 | 18,   19,
    QMetaType::Void,
    QMetaType::Bool,
    0x80000000 | 23,
    QMetaType::Void, QMetaType::QByteArray, QMetaType::QByteArray,   25,   26,
    QMetaType::Void, QMetaType::UShort, QMetaType::Bool, QMetaType::QByteArray, QMetaType::Int,   28,   29,   30,   31,
    QMetaType::Void, QMetaType::UShort, QMetaType::Bool, QMetaType::QByteArray,   28,   29,   30,
    QMetaType::Void, QMetaType::UShort, QMetaType::Bool,   28,   29,
    QMetaType::Void, QMetaType::UShort,   28,
    QMetaType::Void, QMetaType::QString, QMetaType::UShort, QMetaType::QByteArray,   33,   28,   30,
    QMetaType::Void, QMetaType::QString, QMetaType::UShort,   33,   28,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,   37,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,   37,
    QMetaType::Void, QMetaType::QByteArray,   37,
    QMetaType::Bool,
    QMetaType::Void, QMetaType::Bool,   43,
    QMetaType::Int,
    QMetaType::Void, QMetaType::Int,   44,
    0x80000000 | 18,
    0x80000000 | 48,
    QMetaType::Bool,
    0x80000000 | 51,
    0x80000000 | 51,
    QMetaType::Void,

       0        // eod
};

void AudioStreamingLibCore::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        AudioStreamingLibCore *_t = static_cast<AudioStreamingLibCore *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->connected((*reinterpret_cast< QHostAddress(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 1: _t->disconnected((*reinterpret_cast< QHostAddress(*)>(_a[1]))); break;
        case 2: _t->pending((*reinterpret_cast< QHostAddress(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 3: _t->inputData((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 4: _t->veryInputData((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 5: _t->outputData((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 6: _t->veryOutputData((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 7: _t->extraData((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 8: _t->inputLevel((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 9: _t->outputLevel((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 10: _t->adjustSettings(); break;
        case 11: _t->extraDataWritten(); break;
        case 12: _t->finished(); break;
        case 13: _t->error((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 14: { bool _r = _t->start((*reinterpret_cast< const StreamingInfo(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 15: _t->stop(); break;
        case 16: { bool _r = _t->isRunning();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 17: { DiscoverClient* _r = _t->discoverInstance();
            if (_a[0]) *reinterpret_cast< DiscoverClient**>(_a[0]) = std::move(_r); }  break;
        case 18: _t->setKeys((*reinterpret_cast< const QByteArray(*)>(_a[1])),(*reinterpret_cast< const QByteArray(*)>(_a[2]))); break;
        case 19: _t->listen((*reinterpret_cast< quint16(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< const QByteArray(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 20: _t->listen((*reinterpret_cast< quint16(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< const QByteArray(*)>(_a[3]))); break;
        case 21: _t->listen((*reinterpret_cast< quint16(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 22: _t->listen((*reinterpret_cast< quint16(*)>(_a[1]))); break;
        case 23: _t->connectToHost((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2])),(*reinterpret_cast< const QByteArray(*)>(_a[3]))); break;
        case 24: _t->connectToHost((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2]))); break;
        case 25: _t->acceptConnection(); break;
        case 26: _t->rejectConnection(); break;
        case 27: _t->writeExtraData((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 28: _t->writeExtraDataResult(); break;
        case 29: _t->inputDataBack((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 30: _t->outputDataBack((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 31: { bool _r = _t->isInputMuted();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 32: _t->setInputMuted((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 33: { int _r = _t->volume();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 34: _t->setVolume((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 35: { StreamingInfo _r = _t->streamingInfo();
            if (_a[0]) *reinterpret_cast< StreamingInfo*>(_a[0]) = std::move(_r); }  break;
        case 36: { QList<QHostAddress> _r = _t->connectionsList();
            if (_a[0]) *reinterpret_cast< QList<QHostAddress>*>(_a[0]) = std::move(_r); }  break;
        case 37: { bool _r = _t->isReadyToWriteExtraData();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 38: { QAudioFormat _r = _t->audioFormat();
            if (_a[0]) *reinterpret_cast< QAudioFormat*>(_a[0]) = std::move(_r); }  break;
        case 39: { QAudioFormat _r = _t->inputAudioFormat();
            if (_a[0]) *reinterpret_cast< QAudioFormat*>(_a[0]) = std::move(_r); }  break;
        case 40: _t->finishedPrivate(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (AudioStreamingLibCore::*_t)(QHostAddress , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioStreamingLibCore::connected)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (AudioStreamingLibCore::*_t)(QHostAddress );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioStreamingLibCore::disconnected)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (AudioStreamingLibCore::*_t)(QHostAddress , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioStreamingLibCore::pending)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (AudioStreamingLibCore::*_t)(QByteArray );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioStreamingLibCore::inputData)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (AudioStreamingLibCore::*_t)(QByteArray );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioStreamingLibCore::veryInputData)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (AudioStreamingLibCore::*_t)(QByteArray );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioStreamingLibCore::outputData)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (AudioStreamingLibCore::*_t)(QByteArray );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioStreamingLibCore::veryOutputData)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (AudioStreamingLibCore::*_t)(QByteArray );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioStreamingLibCore::extraData)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (AudioStreamingLibCore::*_t)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioStreamingLibCore::inputLevel)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (AudioStreamingLibCore::*_t)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioStreamingLibCore::outputLevel)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (AudioStreamingLibCore::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioStreamingLibCore::adjustSettings)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (AudioStreamingLibCore::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioStreamingLibCore::extraDataWritten)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (AudioStreamingLibCore::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioStreamingLibCore::finished)) {
                *result = 12;
                return;
            }
        }
        {
            typedef void (AudioStreamingLibCore::*_t)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioStreamingLibCore::error)) {
                *result = 13;
                return;
            }
        }
    }
}

const QMetaObject AudioStreamingLibCore::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_AudioStreamingLibCore.data,
      qt_meta_data_AudioStreamingLibCore,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *AudioStreamingLibCore::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *AudioStreamingLibCore::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_AudioStreamingLibCore.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int AudioStreamingLibCore::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 41)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 41;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 41)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 41;
    }
    return _id;
}

// SIGNAL 0
void AudioStreamingLibCore::connected(QHostAddress _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void AudioStreamingLibCore::disconnected(QHostAddress _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void AudioStreamingLibCore::pending(QHostAddress _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void AudioStreamingLibCore::inputData(QByteArray _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void AudioStreamingLibCore::veryInputData(QByteArray _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void AudioStreamingLibCore::outputData(QByteArray _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void AudioStreamingLibCore::veryOutputData(QByteArray _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void AudioStreamingLibCore::extraData(QByteArray _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void AudioStreamingLibCore::inputLevel(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void AudioStreamingLibCore::outputLevel(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void AudioStreamingLibCore::adjustSettings()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void AudioStreamingLibCore::extraDataWritten()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}

// SIGNAL 12
void AudioStreamingLibCore::finished()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}

// SIGNAL 13
void AudioStreamingLibCore::error(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
