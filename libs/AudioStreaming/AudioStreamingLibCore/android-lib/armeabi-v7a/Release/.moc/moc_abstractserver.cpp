/****************************************************************************
** Meta object code from reading C++ file 'abstractserver.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../AudioStreamingLibCore/abstractserver.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'abstractserver.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_AbstractServer_t {
    QByteArrayData data[28];
    char stringdata0[285];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_AbstractServer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_AbstractServer_t qt_meta_stringdata_AbstractServer = {
    {
QT_MOC_LITERAL(0, 0, 14), // "AbstractServer"
QT_MOC_LITERAL(1, 15, 9), // "connected"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 8), // "PeerData"
QT_MOC_LITERAL(4, 35, 12), // "disconnected"
QT_MOC_LITERAL(5, 48, 9), // "readyRead"
QT_MOC_LITERAL(6, 58, 7), // "pending"
QT_MOC_LITERAL(7, 66, 12), // "QHostAddress"
QT_MOC_LITERAL(8, 79, 5), // "error"
QT_MOC_LITERAL(9, 85, 5), // "abort"
QT_MOC_LITERAL(10, 91, 7), // "qintptr"
QT_MOC_LITERAL(11, 99, 10), // "descriptor"
QT_MOC_LITERAL(12, 110, 7), // "setKeys"
QT_MOC_LITERAL(13, 118, 11), // "private_key"
QT_MOC_LITERAL(14, 130, 10), // "public_key"
QT_MOC_LITERAL(15, 141, 6), // "listen"
QT_MOC_LITERAL(16, 148, 4), // "port"
QT_MOC_LITERAL(17, 153, 11), // "auto_accept"
QT_MOC_LITERAL(18, 165, 15), // "max_connections"
QT_MOC_LITERAL(19, 181, 18), // "negotiation_string"
QT_MOC_LITERAL(20, 200, 2), // "id"
QT_MOC_LITERAL(21, 203, 8), // "password"
QT_MOC_LITERAL(22, 212, 4), // "stop"
QT_MOC_LITERAL(23, 217, 19), // "rejectNewConnection"
QT_MOC_LITERAL(24, 237, 19), // "acceptNewConnection"
QT_MOC_LITERAL(25, 257, 11), // "writeToHost"
QT_MOC_LITERAL(26, 269, 4), // "data"
QT_MOC_LITERAL(27, 274, 10) // "writeToAll"

    },
    "AbstractServer\0connected\0\0PeerData\0"
    "disconnected\0readyRead\0pending\0"
    "QHostAddress\0error\0abort\0qintptr\0"
    "descriptor\0setKeys\0private_key\0"
    "public_key\0listen\0port\0auto_accept\0"
    "max_connections\0negotiation_string\0"
    "id\0password\0stop\0rejectNewConnection\0"
    "acceptNewConnection\0writeToHost\0data\0"
    "writeToAll"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_AbstractServer[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   79,    2, 0x06 /* Public */,
       4,    1,   84,    2, 0x06 /* Public */,
       5,    1,   87,    2, 0x06 /* Public */,
       6,    2,   90,    2, 0x06 /* Public */,
       8,    1,   95,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    1,   98,    2, 0x0a /* Public */,
      12,    2,  101,    2, 0x0a /* Public */,
      15,    6,  106,    2, 0x0a /* Public */,
      22,    0,  119,    2, 0x0a /* Public */,
      23,    0,  120,    2, 0x0a /* Public */,
      24,    0,  121,    2, 0x0a /* Public */,
      25,    2,  122,    2, 0x0a /* Public */,
      27,    1,  127,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, QMetaType::QString,    2,    2,
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 7, QMetaType::QString,    2,    2,
    QMetaType::Void, QMetaType::QString,    2,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 10,   11,
    QMetaType::Void, QMetaType::QByteArray, QMetaType::QByteArray,   13,   14,
    QMetaType::Void, QMetaType::UShort, QMetaType::Bool, QMetaType::Int, QMetaType::QByteArray, QMetaType::QString, QMetaType::QByteArray,   16,   17,   18,   19,   20,   21,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray, 0x80000000 | 10,   26,   11,
    QMetaType::Int, QMetaType::QByteArray,   26,

       0        // eod
};

void AbstractServer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        AbstractServer *_t = static_cast<AbstractServer *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->connected((*reinterpret_cast< PeerData(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 1: _t->disconnected((*reinterpret_cast< PeerData(*)>(_a[1]))); break;
        case 2: _t->readyRead((*reinterpret_cast< PeerData(*)>(_a[1]))); break;
        case 3: _t->pending((*reinterpret_cast< QHostAddress(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 4: _t->error((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: _t->abort((*reinterpret_cast< qintptr(*)>(_a[1]))); break;
        case 6: _t->setKeys((*reinterpret_cast< const QByteArray(*)>(_a[1])),(*reinterpret_cast< const QByteArray(*)>(_a[2]))); break;
        case 7: _t->listen((*reinterpret_cast< quint16(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< const QByteArray(*)>(_a[4])),(*reinterpret_cast< const QString(*)>(_a[5])),(*reinterpret_cast< const QByteArray(*)>(_a[6]))); break;
        case 8: _t->stop(); break;
        case 9: _t->rejectNewConnection(); break;
        case 10: _t->acceptNewConnection(); break;
        case 11: _t->writeToHost((*reinterpret_cast< const QByteArray(*)>(_a[1])),(*reinterpret_cast< qintptr(*)>(_a[2]))); break;
        case 12: { int _r = _t->writeToAll((*reinterpret_cast< const QByteArray(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (AbstractServer::*_t)(PeerData , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AbstractServer::connected)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (AbstractServer::*_t)(PeerData );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AbstractServer::disconnected)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (AbstractServer::*_t)(PeerData );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AbstractServer::readyRead)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (AbstractServer::*_t)(QHostAddress , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AbstractServer::pending)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (AbstractServer::*_t)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AbstractServer::error)) {
                *result = 4;
                return;
            }
        }
    }
}

const QMetaObject AbstractServer::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_AbstractServer.data,
      qt_meta_data_AbstractServer,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *AbstractServer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *AbstractServer::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_AbstractServer.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int AbstractServer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void AbstractServer::connected(PeerData _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void AbstractServer::disconnected(PeerData _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void AbstractServer::readyRead(PeerData _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void AbstractServer::pending(QHostAddress _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void AbstractServer::error(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
