#ifndef GLOBAL_H
#define GLOBAL_H
class Devices;
#include "databasehandling.h"
#include "networkhandler.h"
#include "SmtpClient/src/SmtpMime"
#include "configuration.h"
#include <iostream>
#include "language.h"
#include "filebasics.h"
#include "printing.h"
#include "world_controll/devices.h"

//I know it should be in some class as static but I like C style more
extern DatabaseHandling *db;
extern NetworkHandler *network;
extern SmtpClient *smtp;
extern Configuration *config;
extern Language *lang;
extern FileBasics filebasics;
extern Printing printing;
extern Devices *devices;

#endif // GLOBAL_H
