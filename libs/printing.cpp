#include "printing.h"
Printing::Printing(QObject *parent) : QObject(parent)
{
    msgs = new QStringList;
}

Printing::~Printing(){
    delete msgs;
}

bool Printing::print(QString msg){
    if(ready_to_print){
        emit userPrint(msg);
        return true;
    } else {
        msgs->append(msg);
        return false;
    }
}


void Printing::readyToPrint(bool ready){
    ready_to_print = ready;
    if(ready_to_print){
        for(int i = 0; i < msgs->size(); i++){
            emit userPrint(msgs->at(i));
        }
    }
}
