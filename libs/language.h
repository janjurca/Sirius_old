#ifndef LANGUAGE_H
#define LANGUAGE_H

#include <QObject>


class Language : public QObject
{
    Q_OBJECT
public:
    explicit Language(QObject *parent = nullptr);

    int findNextState(QString word, int actual_state);
    int findNextState(QString word, QString current_state, QString className = "base");

    int findCommandstate(QString word, QString classIdentifier, int current_state=0);
    QStringList getWordFromVocabulary(QString word);

public slots:

};

#endif // LANGUAGE_H
