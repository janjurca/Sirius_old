#ifndef DEVICES_H
#define DEVICES_H

class Device;
class ControllerBoard;
#include <QObject>
#include "device.h"
#include "controllerboard.h"
#include <QMap>
#include <QVariantMap>
#include <abstractdbuser.h>
class Devices : public QObject, public AbstractDBUser
{
    Q_OBJECT
public:
    Devices(QObject *parent = nullptr);

    Q_INVOKABLE void loadDevices();

    Q_INVOKABLE QVariantMap controllers();
    Q_INVOKABLE static QVariantMap roles();
    Q_INVOKABLE static QVariantMap modes();
    Q_INVOKABLE static QVariantMap types();
    Q_INVOKABLE static QVariantMap controllerTypes();

    Q_INVOKABLE static QString mapTypeName(QString id);

    Q_INVOKABLE QString deviceRole(QString id);

    static QString sensorId(QString controller_uuid,QString type_name);


    /**
      * creates new simple (default) device in db and return its id
      */
    Q_INVOKABLE static QString createDevice(QString controller_uuid, QString type, QString role, QString mode,QString name = "New");

    void dbIntegrityCheck() override;

signals:
    void deviceAdded(QString id);
    void controllerAdded(QString uuid);
    void clear();

private:
    QMap<QString, Device*> m_devices;
    QMap<QString, ControllerBoard*> m_controllers;


public slots:
};

#endif // DEVICES_H
