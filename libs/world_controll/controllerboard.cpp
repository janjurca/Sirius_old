#include "controllerboard.h"
#include "global.h"

ControllerBoard::ControllerBoard(QString uuid,QObject *parent) :  QObject(parent),m_uuid(uuid)
{

}

void ControllerBoard::refresh(){
    if(m_uuid.isEmpty())
        return;
    QList<QStringList> r = db->readValue(QStringList() << "controllers" << "controller_type",
                                         QStringList()  << "controller_name" << "controller_type" << "controller_type_name",
                                         "controller",
                                         m_uuid);
    if(!r.isEmpty()){
        if(!r.at(0).isEmpty()){
            m_name = r.at(0).at(0);
            m_type.id = QString(r.at(0).at(1));
            m_type.name = r.at(0).at(2);
        } else {
            qWarning("Cannot refresh controller");
        }
    } else {
        qWarning("Cannot refresh controller");

    }
}

QString ControllerBoard::name(){
    return m_name;
}

QString ControllerBoard::uuid(){
    return m_uuid;
}

bool ControllerBoard::setName(QString val){
    bool ret = db->updateRow("controllers","controller_name",val,"controller",m_uuid);
    if(ret)
        m_name = val;
    return ret;
}

bool ControllerBoard::setUuid(QString val){
    m_uuid = val;
    refresh();
    emit uuidChanged();
    return true;
}

ControllerBoard::controllerType_t ControllerBoard::type(){
    return m_type;
}

bool ControllerBoard::exists(QString uuid){
    QList<QStringList> r = db->readValue("controllers",
                                         QStringList()  << "controller",
                                         "controller",
                                         uuid);
    if(!r.isEmpty()){
        if(r.at(0).isEmpty()){
            return false;
        }
        return false;
    } else {
        return false;
    }
    return true;
}

bool ControllerBoard::create(QString uuid, QString type){
    return db->insertRow("controllers",
                         QStringList() << "controller" << "controller_type" << "controller_name",
                         QStringList() << uuid << type << "New");
}

QMap<QString,QString> ControllerBoard::possibleTypes(){
    QList<QStringList> r = db->readValue("controller_type",
                                         QStringList()  << "controller_type" << "controller_type_name"
                                         );
    QMap<QString,QString> ret;
    for(auto &x: r){
        ret[x.at(1)] = x.at(0);
    }
    return ret;
}

QString ControllerBoard::typeName(){
    return m_type.name;
}

QString ControllerBoard::typeId(){
    return m_type.id;
}


bool ControllerBoard::setType(QString id){
    bool ret = db->updateRow("controllers","controller_type",id,"controller",m_uuid);
    if(ret)
        m_name = id;
    return ret;
}

bool ControllerBoard::remove(){
    //TODO solve situation of not deleting devices from db soome warning or something else
}
