#include "devices.h"

Devices::Devices(QObject *parent) : QObject(parent)
{

}

void Devices::loadDevices(){
    QList<QStringList> d = db->readValue("devices",QStringList("id"));
    emit clear();
    for(auto &x: d){
        Device *d =  new Device(x.first());
        d->refresh();
        m_devices.insert(x.first(),d);
        emit deviceAdded(QString(x.first()));
    }

    d = db->readValue("controllers",QStringList("controller"));
    for(auto &x: d){
        ControllerBoard *c =  new ControllerBoard(x.first());
        c->refresh();
        m_controllers.insert(x.first(),c);
        emit controllerAdded(QString(x.first()));
    }
}


QString Devices::createDevice(QString controller_uuid, QString type, QString role, QString mode,QString name){
    if(db->insertRow("devices",QStringList() << "name" << "role" << "mode" << "controller" << "type",QStringList()<< name <<role<< mode << controller_uuid << type)){
        QList<QStringList> r =  db->readValue("devices",QStringList("id"),"name",name);
        if(!r.isEmpty()){
            if(!r.last().isEmpty()){
                return r.last().first();
            }
        }
    }
    printing.print("Cannot create new device, probably error in database");
    return QString();
}

QVariantMap Devices::controllers(){
    QVariantMap ret;
    for(auto &x: m_controllers.keys()){
        ret.insert(x,m_controllers[x]->name());
    }
    return ret;
}


QVariantMap Devices::roles(){
    QVariantMap r;
    QMap<QString,QString> m = Device::possibleRoles();
    for(auto &x: m.keys()){
        r.insert(x,m[x]);
    }
    return QVariantMap(r);
}

QVariantMap Devices::modes(){
    QVariantMap r;
    QMap<QString,QString> m = Device::possibleModes();
    for(auto &x: m.keys()){
        r.insert(x,m[x]);
    }
    return QVariantMap(r);
}
QVariantMap Devices::types(){
    QVariantMap r;
    QMap<QString,QString> m = Device::possibleTypes();
    for(auto &x: m.keys()){
        r.insert(x,m[x]);
    }
    return QVariantMap(r);
}

QString Devices::deviceRole(QString id){
    if(m_devices.find(id) != m_devices.end()){
        return m_devices[id]->role();
    }
    return "";
}
QString Devices::mapTypeName(QString id){
    QList <QStringList> r = db->readValue("device_types",QStringList() << "type_name","type",id);
    if(!r.isEmpty()){
        if(!r.first().isEmpty()){
            return r.first().first();
        }
    }
    return "";
}

QString Devices::sensorId(QString controller_uuid, QString type_name){
    QList<QStringList> r = db->readValue(QStringList() << "devices"<< "device_types",
                                         QStringList("id"),
                                         QList<QPair<QString,QString>>() << QPair<QString,QString>("controller",controller_uuid) << QPair<QString,QString>("type_name",type_name));
    for(auto &x: r){
        for(auto &y: x){
            return y;
        }
    }
    return "";
}

void Devices::dbIntegrityCheck(){

}

QVariantMap Devices::controllerTypes(){
    QVariantMap r;
    QMap<QString,QString> m = ControllerBoard::possibleTypes();
    for(auto &x: m.keys()){
        r.insert(x,m[x]);
    }
    return QVariantMap(r);
}
