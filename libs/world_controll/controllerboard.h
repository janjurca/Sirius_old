#ifndef CONTROLLERBOARD_H
#define CONTROLLERBOARD_H

#include <QObject>
#include "global.h"
#include <QMap>

class ControllerBoard : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QString uuid READ uuid WRITE setUuid NOTIFY uuidChanged)

public:
    typedef struct controllerType
    {
        QString id;
        QString name;
    }controllerType_t;
    ControllerBoard(){}
    explicit ControllerBoard(QString uuid,QObject *parent = nullptr);

    void refresh();
    QString name();
    QString uuid();
    Q_INVOKABLE controllerType_t type();
    Q_INVOKABLE QString typeName();
    Q_INVOKABLE QString typeId();

    bool setName(QString val);
    bool setUuid(QString val);
    Q_INVOKABLE bool setType(QString id);

    static bool exists(QString uuid);
    static bool create(QString uuid,QString type);
    Q_INVOKABLE bool remove();
    Q_INVOKABLE static QMap<QString,QString> possibleTypes();

signals:
    void uuidChanged();
public slots:

private:
    QString m_uuid;
    QString m_name;
    controllerType_t m_type;

};

#endif // CONTROLLERBOARD_H
