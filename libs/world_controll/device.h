#ifndef DEVICE_H
#define DEVICE_H

#include <QObject>
#include "global.h"
#include <QList>
#include <QJsonObject>
#include <QDateTime>
#include <QMap>

class Device : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString uid READ uid WRITE setId NOTIFY uidChanged)
    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QString type READ type WRITE setType)
    Q_PROPERTY(QString note READ note WRITE setNote)
    Q_PROPERTY(int pin READ pin WRITE setPin)
    Q_PROPERTY(QString role READ role WRITE setRole)
    Q_PROPERTY(QString mode READ mode WRITE setMode)
    Q_PROPERTY(int active_value READ active_value WRITE setActive_value)
    Q_PROPERTY(int inactive_value READ inactive_value WRITE setInactive_value)
    Q_PROPERTY(double actual_value READ actual_value WRITE setActual_value)
    Q_PROPERTY(QString controller READ controller WRITE setController)
    Q_PROPERTY(QDateTime value_age READ value_age)

public:
    Device(){}
    Device(QString id, QObject *parent = nullptr);
    Q_INVOKABLE void refresh();

    bool setId(QString val);
    bool setName(QString val);
    bool setType(QString val);
    bool setNote(QString val);
    bool setPin(int val);
    bool setRole(QString val);
    bool setMode(QString val);
    bool setActive_value(int val);
    bool setInactive_value(int val);
    bool setActual_value(double val);
    bool setController(QString val);

    void fetchActual_valueFromController();

    QString uid();
    QString name();
    QString type();
    QString note();
    int pin();
    QString role();
    QString mode();
    int active_value();
    int inactive_value();
    double actual_value();
    QString controller();
    QDateTime value_age();

    static QMap<QString,QString> possibleRoles();
    static QMap<QString,QString> possibleTypes();
    static QMap<QString,QString> possibleModes();

    static QMap<QString,QString> possibleRolesID();
    static QMap<QString,QString> possibleTypesID();
    static QMap<QString,QString> possibleModesID();

    bool activeController(){return m_activeController;}
    Q_INVOKABLE void turnOn();
    Q_INVOKABLE void turnOff();

    Q_INVOKABLE bool remove();
private:
    bool m_activeController = false;
    QString m_id;
    QString m_name;
    QString m_type;
    QString s_type; //translated value from table
    QString m_note;
    int m_pin;
    QString m_role;
    QString s_role; //translated value from table
    QString m_mode;
    QString s_mode; //translated value from table
    QString m_controller;///uuid of controller

    int m_active_value;
    int m_inactive_value;
    double m_actual_value;

    QString m_value_age;



private slots:
    QString readProperty(QString property);
signals:
    void uidChanged();
public slots:
};

#endif // DEVICE_H
