#include "device.h"
#include <QJsonObject>
#include <QJsonDocument>
#include <QDebug>
#include <stdexcept>
#include "devices.h"

Device::Device(QString m_id, QObject *parent) :QObject(parent),m_id(m_id){

}

void Device::refresh(){
    if(m_id.isEmpty())
        return;
    try{
        m_name = readProperty("name");
        m_pin = readProperty("pin").toInt();
        m_type = readProperty("type");
        m_note = readProperty("note");
        m_role = readProperty("role");
        m_mode = readProperty("mode");
        m_active_value = readProperty("active_value").toInt();
        m_inactive_value = readProperty("inactive_value").toInt();
        m_actual_value = readProperty("actual_value").toDouble();
        m_controller = readProperty("controller");
        m_value_age = readProperty("value_age");
        s_mode = possibleModesID()[m_mode];
        s_role = possibleRolesID()[m_role];
    } catch(const std::runtime_error& e){
        printing.print(QString(e.what()));
    }
}

QString Device::readProperty(QString property){
    QList <QStringList> r = db->readValue("devices",QStringList() << property ,"id",(m_id));
    if(!r.isEmpty())
        if(!r.first().isEmpty())
            return r.first().first();
    throw std::runtime_error("Cannot read device " + property.toStdString() + ", probably is not in the database or is corrupted");
}

bool Device::setId(QString val){
    m_id = val;
    refresh();
    emit uidChanged();

    return true;
}

bool Device::setPin(int val){
    bool ret = db->updateRow("devices","pin",QString::number(val),"id",m_id);
    if(ret)
        m_pin = val;
    return ret;
}

bool Device::setName(QString val){
    bool ret = db->updateRow("devices","name",val,"id",m_id);
    if(ret)
        m_name = val;
    return ret;
}
bool Device::setType(QString val){
    bool ret = db->updateRow("devices","type",val,"id",m_id);
    if(ret)
        m_type = val;
    return ret;
}
bool Device::setNote(QString val){
    bool ret = db->updateRow("devices","note",val,"id",m_id);
    if(ret)
        m_note = val;
    return ret;
}

bool Device::setRole(QString val){
    bool ret = db->updateRow("devices","role",val,"id",m_id);
    if(ret)
        m_role = val;
    return ret;
}
bool Device::setMode(QString val){
    bool ret = db->updateRow("devices","mode",val,"id",m_id);
    if(ret)
        m_mode = val;
    return ret;
}
bool Device::setActive_value(int val){
    bool ret = db->updateRow("devices","active_value",QString::number(val),"id",m_id);
    if(ret)
        m_active_value = val;
    return ret;
}
bool Device::setInactive_value(int val){
    bool ret = db->updateRow("devices","inactive_value",QString::number(val),"id",m_id);
    if(ret)
        m_inactive_value = val;
    return ret;
}
bool Device::setActual_value(double val){
    bool ret = db->updateRow("devices","actual_value",QString::number(val),"id",m_id);
    if(ret)
        m_actual_value = val;
    return ret;
}

bool Device::setController(QString val){
    bool ret = db->updateRow("devices","controller",(val),"id",m_id);
    if(ret)
        m_controller = val;
    return ret;
}

QString Device::uid(){
    return m_id;
}

int Device::pin(){
    return m_pin;
}

QString Device::name(){
    return m_name;
}
QString Device::type(){
    return m_type;
}
QString Device::note(){
    return m_note;
}

QString Device::role(){
    return m_role;
}
QString Device::mode(){
    return m_mode;
}
int Device::active_value(){
    return m_active_value;
}
int Device::inactive_value(){
    return m_inactive_value;
}
double Device::actual_value(){
    return readProperty("actual_value").toDouble();
}

QString Device::controller(){
    return m_controller;
}

QDateTime Device::value_age(){
    QString format = (m_value_age.indexOf("T") != -1) ? "yyyy-MM-ddTHH:mm:ss":"yyyy-MM-dd HH:mm:ss";
    return QDateTime::fromString(m_value_age,format);

}


void Device::turnOn(){
    QJsonObject msg
    {
        {"controller", m_controller},
        {"type", "deviceSet"},
        {"pin", m_pin},
        {"mode", s_mode},
        {"role", s_role},
        {"value", m_active_value}
    };
    network->send(QString(QJsonDocument(msg).toJson(QJsonDocument::Compact)));

}

void Device::turnOff(){
    QJsonObject msg
    {
        {"controller", m_controller},
        {"type", "deviceSet"},
        {"pin", m_pin},
        {"mode", s_mode},
        {"role", s_role},
        {"value", m_inactive_value}
    };
    network->send(QString(QJsonDocument(msg).toJson(QJsonDocument::Compact)));
}

QMap<QString,QString> Device::possibleRoles(){
    QList<QStringList> r = db->readValue("device_roles",
                                         QStringList()  << "role_name" << "role"
                                         );
    QMap<QString,QString> ret;
    for(auto &x: r){
        ret[x.at(0)] = x.at(1);
    }
    return ret;
}

QMap<QString,QString> Device::possibleTypes(){
    QList<QStringList> r = db->readValue("device_types",
                                         QStringList()  << "type_name" << "type"
                                         );
    QMap<QString,QString> ret;
    for(auto &x: r){
        ret[x.at(0)] = x.at(1);
    }
    return ret;
}

QMap<QString,QString> Device::possibleModes(){
    QList<QStringList> r = db->readValue("device_modes",
                                         QStringList()  << "mode_name" << "mode"
                                         );
    QMap<QString,QString> ret;
    for(auto &x: r){
        ret[x.at(0)] = x.at(1);
    }
    return ret;
}

QMap<QString,QString> Device::possibleRolesID(){
    QList<QStringList> r = db->readValue("device_roles",
                                         QStringList()  << "role_name" << "role"
                                         );
    QMap<QString,QString> ret;
    for(auto &x: r){
        ret[x.at(1)] = x.at(0);
    }
    return ret;
}

QMap<QString,QString> Device::possibleTypesID(){
    QList<QStringList> r = db->readValue("device_types",
                                         QStringList()  << "type_name" << "type"
                                         );
    QMap<QString,QString> ret;
    for(auto &x: r){
        ret[x.at(1)] = x.at(0);
    }
    return ret;
}

QMap<QString,QString> Device::possibleModesID(){
    QList<QStringList> r = db->readValue("device_modes",
                                         QStringList()  << "mode_name" << "mode"
                                         );
    QMap<QString,QString> ret;
    for(auto &x: r){
        ret[x.at(1)] = x.at(0);
    }
    return ret;
}

bool Device::remove(){
    return db->removeRow("devices","id",m_id);
}

