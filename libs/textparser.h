#ifndef TEXTPARSER_H
#define TEXTPARSER_H

#include <QObject>
#include <QDateTime>

class TextParser : public QObject
{
    Q_OBJECT
public:
    explicit TextParser(QObject *parent = nullptr);

signals:

public slots:
    static QDate getDate(QString str);
    static QTime getTime(QString str);
    static QDateTime getDateTime(QString str);
};

#endif // TEXTPARSER_H
