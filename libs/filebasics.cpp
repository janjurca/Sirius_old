#include "filebasics.h"
#include "global.h"


FileBasics::FileBasics(QObject *parent) : QObject(parent)
{

}

QString FileBasics::readFile(QString filename){
    filename.remove("file://");
    QFile file(filename);
    QString ret = "";
    if(file.open(QIODevice::ReadOnly | QIODevice::Text)){
        ret = file.readAll();
    }
    return  ret;
}

bool FileBasics::saveFile(QString filename, QString data){
    QFile file(filename);
    if(file.open(QIODevice::WriteOnly | QIODevice::Text)){
        file.write(data.toUtf8());
    } else {
        return  false;
    }
    return true;
}
