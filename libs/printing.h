#ifndef PRINTING_H
#define PRINTING_H

#include <QObject>
#include <QStringList>
class Printing : public QObject
{
    Q_OBJECT

private:
    QStringList *msgs;
    bool ready_to_print = false;
public:
    explicit Printing(QObject *parent = nullptr);
    ~Printing();
    void readyToPrint(bool ready = true);
    bool print(QString msg);
signals:
    void userPrint(QString);

public slots:
};

#endif // PRINTING_H
