#include "configuration.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QDebug>
#include <stdexcept>
#include "global.h"
#include <QCryptographicHash>


Configuration::Configuration(QString filename,QString password, QObject *parent) : QObject(parent) , filename(filename), password(password)
{
    encryption = new QAESEncryption(QAESEncryption::AES_256, QAESEncryption::ECB);
}

bool Configuration::load(){
    QFile f_json(filename);
    if(f_json.open(QIODevice::ReadOnly | QIODevice::Text)){
        QByteArray json_text = QString(decode(f_json.readAll())).toUtf8();
        QJsonObject json = QJsonDocument::fromJson(json_text).object();
        for(auto &x: json.toVariantMap().keys()){
            values.insert(x,json[x].toString());
        }
    } else {
            throw std::runtime_error("Config file "+ filename.toStdString() + " cannot be opened -> emergency mode");
    }
    return true;
}

Configuration::~Configuration(){
    delete encryption;
}

bool Configuration::saveConfiguration(){
    QJsonObject json;
    for(auto &x: values.keys()){
        json[x] = values.value(x);
        qWarning() <<json[x];
    }
    QJsonDocument saveDoc(json);
    QByteArray fin =saveDoc.toJson();

    //-----------------crypto

    QFile configFile(filename);
    if(configFile.open(QIODevice::WriteOnly | QIODevice::Text)){
        qWarning("writing");
        configFile.write(encode(fin));
        configFile.close();
    } else {
     return false;
    }
    return true;
}

QByteArray Configuration::encode(QByteArray input){
    if(password.isEmpty()) //No password
        return input;
    QString key(this->password);
    QByteArray hashKey = QCryptographicHash::hash(key.toLocal8Bit(), QCryptographicHash::Sha256);

    QString iv(hashKey);
    QByteArray hashIV = QCryptographicHash::hash(iv.toLocal8Bit(), QCryptographicHash::Sha256);

    return encryption->encode(input, hashKey, hashIV);

}

QByteArray Configuration::decode(QByteArray input){
    if(password.isEmpty()) //No password
        return input;
    QString key(this->password);
    QByteArray hashKey = QCryptographicHash::hash(key.toLocal8Bit(), QCryptographicHash::Sha256);

    QString iv(hashKey);
    QByteArray hashIV = QCryptographicHash::hash(iv.toLocal8Bit(), QCryptographicHash::Sha256);

    return encryption->decode(input, hashKey, hashIV);
}



void Configuration::setFilename(QString filename){
    this->filename = filename;
}

QString Configuration::getConsoleDir(){
    return values["ConsoleDir"];
}

QString Configuration::getDBAdress(){
    return values["DBAdress"];
}

QString Configuration::getDBPassword(){
    return values["DBPassword"];
}

QString Configuration::getDBUserName(){
    return values["DBUsername"];
}

QString Configuration::getMailPassword(){
    return values["MailPassword"];
}

QString Configuration::getMailUsername(){
    return values["MailUsername"];
}

QString Configuration::getMusicDir(){
    return values["MusicDir"];
}

QString Configuration::getNotesFile(){
    return values["NotesFile"];
}

QString Configuration::getProjectDir(){
    return values["ProjectDir"];
}

QString Configuration::getSmtpServer(){
    return values["SmtpServer"];
}

QString Configuration::getServerIp(){
    return values["ServerIp"];
}

int Configuration::getSmtpPort(){
    return values["SmtpPort"].toInt();
}


void Configuration::setConsoleDir(QString value){
    values["ConsoleDir"] = value;
}

void Configuration::setDBAdress(QString value){
    values["DBAdress"] = value;
}

void Configuration::setDBPassword(QString value){
    values["DBPassword"] = value;
}

void Configuration::setDBUserName(QString value){
    values["DBUsername"] = value;
}

void Configuration::setMailPassword(QString value){
    values["MailPassword"] = value;
}

void Configuration::setMailUsername(QString value){
    values["MailUsername"] = value;
}

void Configuration::setMusicDir(QString value){
    values["MusicDir"] = value;
}

void Configuration::setNotesFile(QString value){
    values["NotesFile"] = value;
}

void Configuration::setProjectDir(QString value){
    values["ProjectDir"] = value;
}

void Configuration::setSmtpServer(QString value){
    values["SmtpServer"] = value;
}

void Configuration::setServerIp(QString value){
    values["ServerIp"] = value;
}

void Configuration::setSmtpPort(int value){
    values["SmtpPort"] = QString::number(value);
}

QStringList Configuration::getValuesKeys(){
    return values.keys();
}

QString Configuration::getValue(QString key){
    return values[key];
}
