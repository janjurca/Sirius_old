#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <QObject>
#include "Cyphering/aes/qaesencryption.h"
#include <QMap>

class Configuration : public QObject
{
    Q_OBJECT
public:
    explicit Configuration(QString filename, QString password="", QObject *parent = nullptr);
    ~Configuration();
    QString getConsoleDir();
    QString getDBAdress();
    QString getDBPassword();
    QString getDBUserName();
    QString getMailPassword();
    QString getMailUsername();
    QString getMusicDir();
    QString getNotesFile();
    QString getProjectDir();
    QString getSmtpServer();
    QString getServerIp();
    int getSmtpPort();

    void setConsoleDir(QString value);
    void setDBAdress(QString value);
    void setDBPassword(QString value);
    void setDBUserName(QString value);
    void setMailPassword(QString value);
    void setMailUsername(QString value);
    void setMusicDir(QString value);
    void setNotesFile(QString value);
    void setProjectDir(QString value);
    void setSmtpServer(QString value);
    void setSmtpPort(int value);
    void setServerIp(QString value);
    void setFilename(QString filename);

    Q_INVOKABLE bool saveConfiguration();
    Q_INVOKABLE QStringList getValuesKeys();
    Q_INVOKABLE QString getValue(QString key);
    Q_INVOKABLE void setValue(QString key, QString value){values[key] = value;}

    bool load();

signals:

public slots:

private:
    QMap<QString,QString> values;

    QString filename;
    QAESEncryption *encryption;
    QString password;

private slots:
    QByteArray decode(QByteArray input);
    QByteArray encode(QByteArray input);

};

#endif // CONFIGURATION_H
