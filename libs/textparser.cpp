#include "textparser.h"
#include <QRegularExpression>
#include <QDebug>

TextParser::TextParser(QObject *parent) : QObject(parent)
{

}

QDate TextParser::getDate(QString str){
    QDate date;
    QRegularExpression re("(?<day>\\d+)[ ]*\.[ ]*(?<month>\\d+)[ ]*\.[ ]*(?<year>\\d\\d\\d\\d)");

    QRegularExpressionMatch match = re.match(str);
    if (match.hasMatch()) {
        int day = match.captured("day").toInt();
        int month = match.captured("month").toInt();
        int year = match.captured("year").toInt();
        date = QDate::fromString(QString::number(day)+"." +
                                 QString::number(month)+"." +
                                 QString::number(year),
                                 "d.M.yyyy");
    }
    return date;
}

QTime TextParser::getTime(QString str){
    QTime time;
    QRegularExpression re("(?<hour>\\d+)[ ]*:[ ]*(?<minute>\\d+)");

    QRegularExpressionMatch match = re.match(str);
    if (match.hasMatch()) {
        int hour = match.captured("hour").toInt();
        int minute = match.captured("minute").toInt();

        time = QTime::fromString(QString::number(hour)+":" +
                                 QString::number(minute),
                                 "h:m");
    }
    return time;
}

QDateTime TextParser::getDateTime(QString str){
    QDateTime dt;
    dt.setDate(TextParser::getDate(str));
    dt.setTime(TextParser::getTime(str));
    return dt;
}
