#ifndef YOUTUBEAPI_H
#define YOUTUBEAPI_H

#include <QObject>
#include "networkhandler.h"

class YoutubeAPI : public QObject
{
    Q_OBJECT
public:
    explicit YoutubeAPI(QObject *parent = nullptr);

public slots:
    /**
     * @brief getPlaylist
     * @param link url of page with playlist
     * @return list of youtube links in playlist
     */
    QStringList getPlaylistLinks(QString playlist_link);

    /**
     * In start it will call youtube-dl but in future i want it integrated so //TODO
     * @brief getVideoLink
     * @param link get direct link to video
     * @return direct link to video that can be used in QMediaPlayer
     */
    QString getVideoLink(QString link);
signals:

public slots:
    /**
     * Gets from url the video ID ex.: https://www.youtube.com/watch?v=P8ymgFyzbDo
     * id = P8ymgFyzbDo
     * @brief parseVideoID
     * @param url url of video
     * @return id
     */
    QString parseVideoID(QString url);

    /**
     * Parse boxes of <div> with "ytd-playlist-panel-video-renderer"
     * @brief getPlaylistEntities
     * @param web_content idealy the youtube webpage in plain text
     * @return "boxes" of <div> strings
     */
    QStringList getPlaylistEntities(QString web_content);


};

#endif // YOUTUBEAPI_H
