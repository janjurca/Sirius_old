#ifndef DATABASEHANDLING_H
#define DATABASEHANDLING_H

#include <QObject>
#include <QtSql>
#include <thread>
#include <downloadmanager.h>

class DatabaseHandling : public QObject
{
    Q_OBJECT
public:
    explicit DatabaseHandling(QString server, QString username, QString db_name, QString password, QObject *parent = 0);
private:
    QSqlDatabase *db;
    QSqlDatabase db_remote;
    QSqlDatabase db_local;

    enum states { ONLINE,OFFLINE,};
    int state = OFFLINE;
    DownloadManager *dm = nullptr;
private slots:
    QPair<QSqlQuery,bool> placeQuery(QString qstr);
    bool checkDbOpened();
    void downloadDatabase();
    void setState(enum states state);

signals:
    void message(QString);
    void stateChanged();

public slots:
    bool isOpen();
    QSqlQuery findNull(QString table, QString collumn);
    bool rowExist(QString table, QString collumn, QString value);


    bool removeRow(QString table, QString collumn, QString key);
    bool insertRow(QString table, QStringList collumnsNames, QStringList values);

    bool updateRow(QString table, QString collumn, QString newValue, QString row_id);
    bool updateRow(QString table, QString collumn, QString newValue, QString where, QString key);


    QStringList readRow(QString table, QString key_collumn, QString key);

    /**
     * returns Multiple rows in list
     * @brief readValue
     * @param table Table where to search
     * @param collumns_to_read what collumns to read
     * @param key_collumn (OPTIONAL) if filled than specifiing the collumn where to find for "key"
     * @param key key for what is looked for if key_collumn is filled
     * @param extend this will be added at the end of cluasule
     * @return
     */
    QList<QStringList> readValue(QString table, QStringList collumns_to_read,QString key_collumn = "" ,QString key = "", QString extend = "");
    QList < QStringList > readValue(QString table, QStringList collumns_to_read, QList<QPair<QString,QString>> conditions, QString extend = "");

    /**
     * @brief readValue if is called this then all tables in list are joint by natural join
     */
    QList<QStringList> readValue(QStringList tables, QStringList collumns_to_read, QList<QPair<QString, QString> > conditions , QString extend = "");
    QList<QStringList> readValue(QStringList tables, QStringList collumns_to_read,QString key_collumn = "" ,QString key = "", QString extend = "");


    bool insertTODO(QString thing, QString detail);

    /**
     * @brief getTablesInfo
     * @return List of all tables names in active db
     */
    QStringList getTablesInfo();
};

#endif // DATABASEHANDLING_H
