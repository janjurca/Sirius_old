#ifndef CLOUDACTIONS_H
#define CLOUDACTIONS_H

#include <QObject>

class cloudActions : public QObject
{
    Q_OBJECT
public:
    explicit cloudActions(QObject *parent = 0);
    bool rhcPortForward();
    bool testNetworkConnect();
signals:

public slots:
};

#endif // CLOUDACTIONS_H
