#include "networkhandler.h"
#include <QDebug>
#include <QtGlobal>

NetworkHandler::NetworkHandler(QObject *parent) : QObject(parent)
{
    udpSocket = new QUdpSocket();
}

void NetworkHandler::processPendingDatagrams(){
    for(int i = 0; i < listeningPorts.size();i++){
        while (listeningPorts[i]->hasPendingDatagrams()) {
            QByteArray datagram;
            datagram.resize(listeningPorts[i]->pendingDatagramSize());
            QHostAddress host;
            quint16 port;
            listeningPorts[i]->readDatagram(datagram.data(), datagram.size(),&host,&port);
            QString input_qt(datagram);
            emit receivedInput(input_qt,port);
        }
    }
}

void NetworkHandler::send(QString input, int port){
    QByteArray datagram = input.toUtf8();
    QUdpSocket socket;
    socket.writeDatagram(datagram.data(), datagram.size(), QHostAddress::Broadcast, port);
}

void NetworkHandler::listenOnPort(int port){
    listeningPorts << new QUdpSocket(this);
    if(listeningPorts.last()->bind(port,QUdpSocket::ShareAddress)){
        connect(listeningPorts.last(),SIGNAL(readyRead()),this,SLOT(processPendingDatagrams()));
    } else {
        listeningPorts.removeLast();
        qWarning() << "Cannot bind udp port:" << port;
    }
}

void NetworkHandler::disconnectFromServer()
{
    tcpSocket->abort();
}
void NetworkHandler::readTCP()
{
#if QT_VERSION >= QT_VERSION_CHECK(5, 7, 1)
    in.startTransaction();
    QString msg;
    in >> msg;
    if (!in.commitTransaction())
        return;
    emit receivedTCP(msg); //TODO parse message
    qWarning() << "tcp message: " << msg ;
#endif
}

void NetworkHandler::displayError(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        qWarning() << "The host was not found. Please check the host name and port settings.";
        break;
    case QAbstractSocket::ConnectionRefusedError:
        qWarning() << "The connection was refused by the peer. "
                                    "Make sure the server is running, "
                                    "and check that the host name and port "
                                    "settings are correct.";
        break;
    default:
        qWarning() << tr("The following error occurred: %1.").arg(tcpSocket->errorString());
    }
}

void NetworkHandler::startServer(){
    QNetworkConfigurationManager manager;
    if (manager.capabilities() & QNetworkConfigurationManager::NetworkSessionRequired) {
        // If the saved network configuration is not currently discovered use the system default
        QNetworkConfiguration config = manager.defaultConfiguration();
        if ((config.state() & QNetworkConfiguration::Discovered) !=
            QNetworkConfiguration::Discovered) {
            config = manager.defaultConfiguration();
        }

        networkSession = new QNetworkSession(config, this);
        connect(networkSession, &QNetworkSession::opened, this, &NetworkHandler::sessionOpenedServer);

        qDebug() <<tr("Opening network session.");
        networkSession->open();
    } else {
        sessionOpenedServer();
    }
        connect(tcpServer, &QTcpServer::newConnection, this, &NetworkHandler::hostConnected);
}
void NetworkHandler::sessionOpenedServer()
{
    tcpServer = new QTcpServer(this);
    if (!tcpServer->listen(QHostAddress::Any,SERVER_TCP_PORT)) {
        qWarning() << tcpServer->errorString();
    }
}

void NetworkHandler::hostConnected(){
    clientsConnected << tcpServer->nextPendingConnection();
    sendTCP("OK",clientsConnected.first());
}


void NetworkHandler::startClient(QString serverIP,int port){
    tcpSocket = new QTcpSocket(this);
    in.setDevice(tcpSocket);
    in.setVersion(QDataStream::Qt_4_0);
    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readTCP()));
    connect(tcpSocket,SIGNAL(error(QAbstractSocket::SocketError)),this, SLOT(displayError(QAbstractSocket::SocketError)));
    connect(tcpSocket, &QTcpSocket::disconnected, this, &NetworkHandler::disconnected);
    connect(tcpSocket, &QTcpSocket::connected, this, &NetworkHandler::connected);
    connect(tcpSocket, &QTcpSocket::stateChanged, this, &NetworkHandler::stateChanged);
    QNetworkConfigurationManager manager;
    if (manager.capabilities() & QNetworkConfigurationManager::NetworkSessionRequired) {
        // If the saved network configuration is not currently discovered use the system default
        QNetworkConfiguration config = manager.defaultConfiguration();
        if ((config.state() & QNetworkConfiguration::Discovered) !=
            QNetworkConfiguration::Discovered) {
            config = manager.defaultConfiguration();
        }

        networkSession = new QNetworkSession(config, this);

        qWarning() <<"Opening network session.";
        networkSession->open();
    }

    tcpSocket->connectToHost(serverIP, port);
}

void NetworkHandler::sendTCP(QString str, QTcpSocket *socket)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    out << str;
    socket->write(block);
}

bool NetworkHandler::isConnected(){
    if(tcpSocket){
        if(tcpSocket->state() == QAbstractSocket::ConnectedState){
            return true;
        }
    }
    return false;
}

void NetworkHandler::waitForTcpConnection(int time){
    tcpSocket->waitForConnected(time);
}
