#include "cloudactions.h"
#include <QProcess>
#include <QDebug>
#include <QtNetwork>
#include <QFile>

cloudActions::cloudActions(QObject *parent) : QObject(parent)
{

}

bool cloudActions::rhcPortForward(){
    if(testNetworkConnect()){
        qWarning() << "Připojuji k rh cloudu";
        QString program  = "/var/sirius/./rhc_connect";
        qWarning() << program;
        QProcess *cloudPortForward = new QProcess(this);
        cloudPortForward->start(program);
        cloudPortForward->waitForFinished(7000);
        //QString output = cloudPortForward->readAllStandardOutput();
        return true;
    } else {
        qWarning() << "Nepřipojen k internetu";
        return false;
    }

}

bool cloudActions::testNetworkConnect(){
    QNetworkAccessManager nam;
    QNetworkRequest req(QUrl("http://www.google.com"));
    QNetworkReply *reply = nam.get(req);
    QEventLoop loop;
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();
    if(reply->bytesAvailable()){
        return true;
    }else {
        return false;
    }
}
