#include "databasehandling.h"
#include <QDateTime>
#include <QDebug>
#include "downloadmanager.h"
#include <QTimer>
#include "defines.h"
#include "global.h"

DatabaseHandling::DatabaseHandling(QString server,QString username,QString db_name,QString password,QObject *parent) : QObject(parent)
{
    db_remote = QSqlDatabase::addDatabase("QMYSQL","remote");
    db_remote.setHostName(server);
    db_remote.setDatabaseName(db_name);
    db_remote.setUserName(username);
    db_remote.setPassword(password);
    db_remote.setConnectOptions("MYSQL_OPT_CONNECT_TIMEOUT=2");//2 secs timeout
    if(!db_remote.open()){
        setState(OFFLINE);
        printing.print("Cannot connect to remote DB");
    }else{
        setState(ONLINE);
        printing.print("Remote DB connected");
    }
    dm = new DownloadManager();

    db_local = QSqlDatabase::addDatabase("QSQLITE","local");
    db_local.setDatabaseName(WORK_DIR + "sirius.db");
    if(!db_local.open()){
        printing.print("Cannot connect to local DB");
    }else{
        printing.print("Local DB connected");
    }
    if(state == OFFLINE){
        db = &db_local;
    } else if (state == ONLINE){
        db = &db_remote;
    }
    dm = new DownloadManager();
    downloadDatabase();
}

void DatabaseHandling::downloadDatabase(){
    db_local.close();
    QUrl url = QUrl::fromEncoded("http://www.rowes.cz/sirius.db");
    dm->doDownload(url);
    connect(
        dm, &DownloadManager::finished,
        [=](  ) { this->db_local.open(); }
    );
}


/**
 * @brief DatabaseHandling::insertRow
 * @param table
 * @param collumnsNames
 * @param values
 * @return
 * @pre size of collumnsNames must be the same as size of values
 * funkce vlozi do tabulky radek
 *
 */
bool DatabaseHandling::insertRow(QString table,QStringList collumnsNames, QStringList values){
    if(db->isOpen()){
        if(collumnsNames.size() == values.size()){
            QString query = "INSERT INTO `" + db->databaseName() + "` . `" + table + "` (";
            for(int i = 0; i < collumnsNames.size(); i++){ // postupne skladani sql query - nazvy sloupcu
                query += collumnsNames[i];
                if(i != (collumnsNames.size() - 1)) // aby to nedelalo carku za posledni hodnotou
                    query += ",";
            }
            query += ") VALUES (";
            for(int i = 0; i < values.size(); i++){ // vkladane hodnoty
                query += (values[i] == "NULL") ? "" : "'";
                query += values[i];
                query += (values[i] == "NULL") ? "" : "'";
                if(i != (values.size() - 1)) // aby to nedelalo carku za posledni hodnotou
                    query += ",";
            }
            query += ");";

            return placeQuery(query).second;
        }
        return false;
    }
    return false;
}


bool DatabaseHandling::insertTODO(QString thing, QString detail){
    QDateTime date = QDateTime::currentDateTime();
    QString qstr = "INSERT INTO TODO (thing,detail,time) VALUES ('" +
            thing +
            "','" +
            detail +
            "','" +
            date.toString("yyyy-MM-dd hh:mm:ss") +
            "');";

    return rowExist("TODO","detail",detail) ? true : placeQuery(qstr).second;
    //TODO kdyz stejne todo existuje tak ho nezarazuj;
}

/**
 * @brief DatabaseHandling::updateRow
 * @param table table to work with
 * @param collumn to what collumn the value will be updated
 * @param newValue
 * @param id identificate row by this id
 * @return
 */
bool DatabaseHandling::updateRow(QString table, QString collumn, QString newValue, QString row_id){
    QString qstr = "UPDATE " + table + " SET " + collumn + "='" + newValue + "' WHERE id=" + row_id +";";
    return placeQuery(qstr).second;
}
bool DatabaseHandling::updateRow(QString table, QString collumn, QString newValue, QString where, QString key){
    QString qstr = "UPDATE " + table + " SET " + collumn + "='" + newValue + "' WHERE "+ where +"='" + key +"';";
    return placeQuery(qstr).second;
}

bool DatabaseHandling::removeRow(QString table, QString collumn, QString key){
    QString qstr = "DELETE FROM " + table + " WHERE " + collumn + "='" + key + "';";
    return placeQuery(qstr).second;
}

/**
 * @brief DatabaseHandling::findNull
 * @param table nazev tabulky ve ktere se ma hledat
 * @param collumn nazev sloupce ve kterem se ma hledat null
 * @return
 */
QSqlQuery DatabaseHandling::findNull(QString table, QString collumn){
    QString qstr = "SELECT * FROM " + table + " WHERE " + collumn + " IS NULL;";
    return placeQuery(qstr).first;
}

bool DatabaseHandling::rowExist(QString table, QString collumn, QString value){
    QString qstr = "SELECT " + collumn + " FROM " + table + " WHERE " + collumn + "='" + value + "';";
    QSqlQuery query = placeQuery(qstr).first;
    if(query.next())
        return true;
    else
        return false;
}

QPair<QSqlQuery,bool> DatabaseHandling::placeQuery(QString qstr){
    QSqlQuery query(*db);
#ifdef QT_DEBUG
    qWarning() << qstr;
#endif
    QPair<QSqlQuery,bool> ret;
    if(checkDbOpened()){
        ret.second = query.exec(qstr);
    }
    ret.first = query;
    return ret;
}


bool DatabaseHandling::checkDbOpened(){
    if(state == ONLINE){
        if(db->isOpen()){
            setState(ONLINE);
            return true;
        } else {
            db->close();
            if(db->open()){
                setState(ONLINE);
                return true;
            } else {
                setState(OFFLINE);
                db = &db_local;
                printing.print("Disconected from remote db, trying to swithc to local one");
                checkDbOpened();
                return false;
            }
        }
    } else {
        if(db->isOpen()){
            return true;
        } else {
            db->close();
            if(db->open()){
                return true;
            } else {
                printing.print("ERROR: there is no db with FSM, many functions will be limited");
                return false;
            }
        }
    }
}
void DatabaseHandling::setState(enum states state){
    if(state != this->state){
        if(state == OFFLINE){
            db = &db_local;
        }
        if(state == ONLINE){
            db = &db_remote;
            //TODO process changes that have been made to loacal to remote too
        }
        this->state = state;
    }

}

QStringList DatabaseHandling::readRow(QString table,QString key_collumn, QString key){
    QStringList result;
    if(checkDbOpened()){
        QSqlQuery query = placeQuery("select * from `" + table + "` where "+key_collumn+"='"+key+"';").first;
            if (query.next()){
                for(int i = 0; query.value(i).isValid(); i++){
                    result << query.value(i).toString();
                }
            }
    }
    return result;
}

QList < QStringList > DatabaseHandling::readValue(QString table, QStringList collumns_to_read, QString key_collumn , QString key, QString extend){
    QList<QPair<QString,QString>> cond;
    if(!key_collumn.isEmpty())
        cond << QPair<QString,QString>(key_collumn,key);
    return readValue(table,collumns_to_read,cond,extend);
}

QList < QStringList > DatabaseHandling::readValue(QString table, QStringList collumns_to_read, QList<QPair<QString,QString>> conditions, QString extend){
    return readValue(QStringList(table),collumns_to_read,conditions,extend);
}

QList < QStringList > DatabaseHandling::readValue(QStringList tables, QStringList collumns_to_read, QString key_collumn, QString key, QString extend){
    return readValue(tables,collumns_to_read,QList<QPair<QString,QString>>() << QPair<QString,QString>(key_collumn,key),extend);
}

QList < QStringList > DatabaseHandling::readValue(QStringList tables, QStringList collumns_to_read, QList<QPair<QString,QString>> conditions, QString extend){
    QList < QStringList > ret = QList < QStringList >();
    QStringList result;
    if(checkDbOpened()){
        QString collumns;
        for(int i = 0; i < collumns_to_read.size();i++){
            collumns += collumns_to_read.at(i);
            if(collumns_to_read.size() != (i + 1))
                collumns += ",";
        }
        QString qr = "select "+collumns+" from ";
        for(int i = 0; i< tables.size(); i++){
            if(i != 0){
                qr += " natural join ";
            }
            qr += "`"+tables.at(i) + "`";
        }

        for(int i = 0; i < conditions.size(); i++){
            if(i == 0){
                 qr += " where "+conditions.at(i).first+"='"+conditions.at(i).second+ "' ";
            } else {
                 qr += " and "+conditions.at(i).first+"='"+conditions.at(i).second+ "' ";
            }
        }
        qr += extend +";";
        QSqlQuery query = placeQuery(qr).first;
            while (query.next()){
                result.clear();
                for(int i = 0; i < collumns_to_read.size(); i++){
                    result << query.value(i).toString();
                }
                ret << result;
            }
    }
    return ret;
}


bool DatabaseHandling::isOpen(){
    return db->isOpen();
}


QStringList DatabaseHandling::getTablesInfo(){
    QStringList result;
    QSqlQuery query = placeQuery("SHOW TABLE STATUS;").first;
    while(query.next()){
        result << query.value(0).toString();
    }
    return result;
}
