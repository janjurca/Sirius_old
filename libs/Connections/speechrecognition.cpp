#include "speechrecognition.h"
#include <QDir>
#include "../defines.h"
QList<QString> transkripce;


template <class T>
static QVector<qreal> getBufferLevels(const T *buffer, int frames, int channels);

speechRecognition::speechRecognition(QObject *parent) : QObject(parent)
{
    /*
    QDir speech_dir;
    QString speech_dir_qstr = "/var/sirius";
    if(!speech_dir.cd(speech_dir_qstr)){

        emit recognizedOutput("neni dostupna slozka pro ukladani nahravek" + speech_dir_qstr);
    }
    audioRecorder = new QAudioRecorder(this);

    connect(audioRecorder, SIGNAL(durationChanged(qint64)), this, SLOT(updateProgress(qint64)));
    connect(audioRecorder, SIGNAL(statusChanged(QMediaRecorder::Status)), this,SLOT(updateStatus(QMediaRecorder::Status)));
    //connect(audioRecorder, SIGNAL(stateChanged(QMediaRecorder::State)),this, SLOT(onStateChanged(QMediaRecorder::State)));
    connect(audioRecorder, SIGNAL(error(QMediaRecorder::Error)), this, SLOT(displayErrorMessage()));
    */
}

void speechRecognition::updateProgress(qint64 duration)
{
    qWarning() << "updateprogress" << duration;
    if(duration > 10000) //maximální cas nahravani prikazu
        rozpoznejStop();

    if (audioRecorder->error() != QMediaRecorder::NoError || duration < 2000)
        return;
}

void speechRecognition::updateStatus(QMediaRecorder::Status status){
    QString statusMessage;

    switch (status) {
    case QMediaRecorder::RecordingStatus:
        statusMessage = tr("Recording to %1").arg(audioRecorder->actualLocation().toString());
        break;
    case QMediaRecorder::PausedStatus:

        statusMessage = tr("Paused");
        break;
    case QMediaRecorder::UnloadedStatus:
    case QMediaRecorder::LoadedStatus:

        statusMessage = tr("Stopped");
    default:
        break;
    }
}

void speechRecognition::toggleRecord(){
       if (audioRecorder->state() == QMediaRecorder::StoppedState) {
           emit recording(true);
           qWarning() << "rozpoznavam";

           audioRecorder->setAudioInput(audioRecorder->defaultAudioInput());

           QAudioEncoderSettings settings;
           settings.setCodec("audio/FLAC");
           settings.setQuality(QMultimedia::HighQuality);
           settings.setEncodingMode(QMultimedia::ConstantQualityEncoding);
           audioRecorder->setEncodingSettings(settings);
           audioRecorder->setOutputLocation(QUrl::fromLocalFile("/var/sirius/recognize.flac"));
           audioRecorder->record();
       }
       else {
           emit recording(false);
           qWarning() << "konec rozpoznavani";
           audioRecorder->stop();
           google_speech_post("/var/sirius/recognize.flac");
       }
}

void speechRecognition::displayErrorMessage(){
    qWarning() << audioRecorder->errorString();
}

void speechRecognition::google_speech_post(QString soubor){
    QProcess* proc = new QProcess();
    transkripce = QStringList();

    QString cmd( "/bin/sh" );
    QStringList args;

    args << "-c" << "curl -X POST --data-binary @" + soubor + " --header 'Content-Type: audio/x-flac; rate=44100;' 'https://www.google.com/speech-api/v2/recognize?output=json&lang=cs&key=AIzaSyBWTN4Lt8Qqt1YMnuq67VtDcj0ef1_ItjA'";
    proc->start(cmd, args);
    //qWarning() << "arguments " << proc->arguments();
    proc->waitForReadyRead();
    QString output(proc->readAllStandardOutput());
    //qWarning() << "output" << output;
    QStringList json_list_oba = output.split("\n");
    proc->kill();
    delete proc;

    //qWarning() << "json_list_oba" << json_list_oba;
    output = json_list_oba[1];
    /*qWarning() << "output" << output;
    output.replace("\"","");
    qWarning() << "output" << output;*/
    QJsonDocument jsonResponse = QJsonDocument::fromJson(output.toUtf8()); // vytvoření json dokumentu ( jestli to dobře chápu tak je to něco jako inicializace jsonu aby ten program vytvořil určitou známou strukturu se kterou dále pracuje)

    QJsonObject jsonObj = jsonResponse.object(); // inicializace objektů 1. řádu v jsonu
    //qWarning() << "jsonresponse" << jsonResponse;
    //qWarning() << "jsonObj" <<jsonObj;
    //qWarning() << "result:" << jsonObj["result"].toArray();//  výpis objektu 1. řádu

    QJsonArray result = jsonObj["result"].toArray(); //inicializace objeků 2. řádu nacházejících se pod objektem 1.řádu "main"
    QJsonObject alternative = result[0].toObject(); // vypsání číselné hodnouty objektu druhého řádu
    QJsonArray alternative_array = alternative["alternative"].toArray();
    for(int i = 0; i < alternative_array.size(); i++){
        QJsonObject transcript = alternative_array[i].toObject();

        transkripce << transcript["transcript"].toString();
    }

    qWarning() << "transkripce-pole" << transkripce;
    for(int i = 0; i < transkripce.size(); i++){
        qWarning() << "transkripce" << transkripce[i];
    }
    if(!transkripce.isEmpty()){
        emit recognizedOutput(transkripce.first().toLower());
    }
}

void speechRecognition::rozpoznejStart(){
    if (audioRecorder->state() == QMediaRecorder::StoppedState) {
        emit recording(true);
        qWarning() << "rozpoznavam";

        audioRecorder->setAudioInput(audioRecorder->defaultAudioInput());

        QAudioEncoderSettings settings;
        settings.setCodec("audio/FLAC");
        settings.setQuality(QMultimedia::HighQuality);
        settings.setEncodingMode(QMultimedia::ConstantQualityEncoding);
        audioRecorder->setEncodingSettings(settings);
        audioRecorder->setOutputLocation(QUrl::fromLocalFile(WORK_DIR +  "recognize.flac"));
        audioRecorder->record();
    }
}
void speechRecognition::rozpoznejStop(){
    emit recording(false);
    qWarning() << "konec rozpoznavani";
    audioRecorder->stop();
    google_speech_post(WORK_DIR + "recognize.flac");
}

