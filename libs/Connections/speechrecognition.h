#ifndef SPEECHRECOGNITION_H
#define SPEECHRECOGNITION_H

#include <QObject>
#include <QMediaRecorder>
#include <QUrl>
#include <QNetworkReply>
#include <QProcess>
QT_BEGIN_NAMESPACE

class QAudioRecorder;
class QAudioProbe;
class QAudioBuffer;
QT_END_NAMESPACE
class QAudioLevel;

#include <QObject>
#include <QAudioProbe>
#include <QAudioRecorder>
#include <QDir>
#include <QMediaRecorder>
#include <QDebug>
#include <QUrlQuery>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QProcess>
#include <QJsonValue>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QJsonArray>
#include <QList>

class speechRecognition : public QObject
{
    Q_OBJECT
public:
    explicit speechRecognition(QObject *parent = 0);


private:
    void clearAudioLevels();
    QAudioRecorder *audioRecorder;
    QList<QAudioLevel*> audioLevels;
    bool outputLocationSet;



public slots:
    void rozpoznejStart();
    void rozpoznejStop();

private slots:
    void toggleRecord();
    void updateStatus(QMediaRecorder::Status);
    void updateProgress(qint64 pos);
    void displayErrorMessage();
    void google_speech_post(QString soubor);



signals:
    void recognizedOutput(QString vystup);
    void recording(bool);


};

#endif // SPEECHRECOGNITION_H
