#ifndef NETWORKHANDLER_H
#define NETWORKHANDLER_H
#include <QObject>
#include <QtNetwork>

#include "../defines.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QEventLoop>
#include <QTextCodec>

class NetworkHandler : public QObject
{
    Q_OBJECT
public:
    explicit NetworkHandler(QObject *parent = 0);
    static void send(QString input,int port=INGOING_SERVER_PORT);
    void listenOnPort(int port);
    void sendTCP(QString, QTcpSocket *);
    void startServer();
    Q_INVOKABLE void startClient(QString serverIP, int port=SERVER_TCP_PORT);
    bool isConnected();
    void disconnectFromServer();
    void waitForTcpConnection(int time = 30000);

    static QString getWebContent(QString url){
        QNetworkAccessManager manager;
        QNetworkReply *response = manager.get(QNetworkRequest(QUrl(url)));
        QEventLoop event;
        connect(response,SIGNAL(finished()),&event,SLOT(quit()));
        event.exec();
        return response->readAll();
    }

private:
    QTcpServer *tcpServer;
    QList < QTcpSocket* > clientsConnected;
    QTcpSocket *tcpSocket;
    QDataStream in;
    QNetworkSession *networkSession;

    QUdpSocket *udpSocket;
    QList < QUdpSocket* > listeningPorts;



signals:
    void receivedInput(QString data,int port);
    void connected();
    void disconnected();
    void stateChanged(QAbstractSocket::SocketState state);
    void receivedTCP(QString data);


private slots:
    void processPendingDatagrams();
    void readTCP();
    void displayError(QAbstractSocket::SocketError socketError);
    void sessionOpenedServer();
    void hostConnected();

};
#endif // NetworkHandler_H
