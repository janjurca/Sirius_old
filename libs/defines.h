#ifndef DEFINES_H
#define DEFINES_H

#define INGOING_CLIENT_PORT  4545
//#define OUTGOING_CLIENT_PORT  4546 ///pouziva se na komunikaci s klintem pomoci udp multicastu,odpoved od klienta uzivateli
#define INGOING_SERVER_PORT  4547
#define INGOING_ARDUINO_PORT  4547
#define SERVER_TCP_PORT 42424
#define DB_TODO "todo"
#define VOIP_PORT "1048"

#define LOCK_TIME 900000



#ifdef Q_OS_ANDROID
#define WORK_DIR QString("")
#else
#define WORK_DIR (QString(QDir::homePath())+"/.Sirius/")
#endif


#endif // DEFINES_H
