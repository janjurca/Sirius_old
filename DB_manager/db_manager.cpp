#include "db_manager.h"
#include "ui_db_manager.h"
#include "databasehandling.h"
#include "../defines.h"
#include <QDebug>
#include <iostream>
#include <QMessageBox>
#include <QInputDialog>
#include "global.h"
#include "defines.h"
using namespace std;

DB_manager::DB_manager(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DB_manager)
{
    ui->setupUi(this);
    config = new Configuration(WORK_DIR + "sirius.json"); //TODO make the configuration cyphered
    try {
        config->load();
    } catch (const std::runtime_error& e) {
        ui->statusBar->showMessage("Cannot open ~/.Sirius/sirius.json config file. Please create it by Sirius program.");
    }
    db = new DatabaseHandling(config->getDBAdress(),config->getDBUserName(),config->getDBUserName(),config->getDBPassword());
    ui->device_select->addItems(readDeviceList());

    ui->io->addItems(QStringList() << "INPUT" << "OUTPUT" << "INPUT_PULLUP");
    ui->ad->addItems(QStringList() << "ANALOG" << "DIGITAL");
    ui->tabWidget->setTabText(0,"Output devices");
    ui->tabWidget->setTabText(1,"Vocabulary and Sentences");
    ui->tabWidget->setTabText(2,"Sensors");

    refreshVocabulary();
    on_sensors_refresh_clicked();
    on_refresh_clicked();
    connect(ui->word, SIGNAL(returnPressed()),this,SLOT(addWordToSentence()));

    //TODO slova manager chci tam napsat větu a chci aby program tu větu rozparsoval
    //podle slov ( tím pádem tam musí být automatické doplnovaní abych byl schopen
    //používat už jednou zadané stavy )  a jediné co specifikuji tak bude koncový stav
    //a možné alternativy zadaných slov
    // mela by tam být nějaká vizualizace stavu po zápisu tzn co se zapíše do databáze stavy entity atd a které se jen použijí...
}

QString DB_manager::selectedWord(QString sentence, int cursorPosition){

    QStringList words = sentence.split(" ");
    for(int i = 0; i < words.size(); i++){
        cursorPosition = cursorPosition - QString(words.at(i)).length();
        if(cursorPosition <= 0){
            return words.at(i);
        }
        cursorPosition--;
    }
    return QString();
}

DB_manager::~DB_manager()
{
    delete ui;
    delete vocabularyCompleter;
}

void DB_manager::readDevice(QString id){
    if(id.isEmpty()){
        return;
    }
    QList < QStringList > note = db->readValue("devices",QStringList() << "note" ,"id",id);
    QList < QStringList > pin = db->readValue("devices",QStringList() << "pin" ,"id",id);
    QList < QStringList > active_value = db->readValue("devices",QStringList() << "active_value" ,"id",id);
    QList < QStringList > inactive_value = db->readValue("devices",QStringList() << "inactive_value" ,"id",id);
    QList < QStringList > io_list = db->readValue("devices",QStringList() << "role" ,"id",id);
    QList < QStringList > ad_list = db->readValue("devices",QStringList() << "mode" ,"id",id);

    QString ref = "Cannot read: ";
    QString err(ref);

    ui->id->setText(id);
    ui->type;
    if(!note.isEmpty())
        ui->note->setText(note.first().first());
    else
        err += "note, ";
    if(!pin.isEmpty())
        ui->pin->setValue(QString(pin.first().first()).toInt());
    else
        err += "pin, ";
    if(!io_list.isEmpty())
        ui->io->setCurrentText(io_list.first().first());
    else
        err += "role, ";
    if(!ad_list.isEmpty())
        ui->ad->setCurrentText(ad_list.first().first());
    else
        err += "mode, ";
    if(!active_value.isEmpty())
        ui->active_value->setValue(QString(active_value.first().first()).toInt());
    else
        err += "active value, ";
    if(!inactive_value.isEmpty())
        ui->inactive_value->setValue(QString(inactive_value.first().first()).toInt());
    else
        err += "inactive value ";

    if (err != ref)
        ui->statusBar->showMessage(err,5000);
}

QStringList DB_manager::readDeviceList(){
    QList < QStringList > res = db->readValue("devices",QStringList()<< "id" << "name");
    QStringList devices;
    for(int i = 0; i < res.size(); i++){
        devices << res.at(i).at(1) + "|" + res.at(i).at(0);
    }
    return devices;
}

void DB_manager::on_device_select_currentIndexChanged(const QString &arg1)
{
    QString id = arg1.split("|").last();
    readDevice(id);
}

void DB_manager::writeDevice(){
    QString err = "Cant update ";
    QString ref = err;
    if(!db->updateRow("devices","note",ui->note->text(),ui->id->text()))
        err = err +"note";
    if(!db->updateRow("devices","pin",QString::number(ui->pin->value()),ui->id->text()))
        err = err +",pin";
    if(!db->updateRow("devices","role",ui->io->currentText(),ui->id->text()))
        err = err +",io";
    if(!db->updateRow("devices","mode",ui->ad->currentText(),ui->id->text()))
        err = err +",ad";
    if(!db->updateRow("devices","active_value",QString::number(ui->active_value->value()),ui->id->text()))
        err = err +",active_value";
    if(!db->updateRow("devices","inactive_value",QString::number(ui->inactive_value->value()),ui->id->text()))
        err = err +",inactive_value";
    if (err != ref)
        ui->statusBar->showMessage(err,5000);
}

void DB_manager::on_pushButton_clicked()
{
    writeDevice();
}

void DB_manager::on_pushButton_2_clicked()
{
    QMessageBox msgBox;
    msgBox.setText("Opravdu smazat?");
    QPushButton *abortButton = msgBox.addButton(QMessageBox::Abort);
    QPushButton *connectButton = msgBox.addButton(tr("Smazat"), QMessageBox::ActionRole);
    msgBox.exec();

    if (msgBox.clickedButton() == connectButton) {
        if(db->removeRow("devices","id",ui->device_select->currentText().split("|").last())){
            ui->statusBar->showMessage("Uspešně smazáno",3000);
            on_refresh_clicked();
        }
        else
            ui->statusBar->showMessage("Nelze smazat",3000);
    }
}

void DB_manager::on_refresh_clicked()
{
    ui->device_select->clear();
    ui->device_select->addItems(readDeviceList());
}

void DB_manager::on_newItem_clicked()
{
    bool ok;
    QString text = QInputDialog::getText(this, tr("New item"),
                                        tr("Name:"), QLineEdit::Normal,
                                        "", &ok);
    if (ok && !text.isEmpty()){
        db->insertRow("devices",QStringList() << "name",QStringList() <<text);
        on_refresh_clicked();
    }
}


void DB_manager::on_availableStates_itemDoubleClicked(QListWidgetItem *item)
{
    //ui->sentence->setText(ui->sentence->text() + " " + item->text());
}

void DB_manager::refreshVocabulary(){
    if(vocabularyCompleter != NULL){
        delete vocabularyCompleter;
    }
    vocabulary = getVocabulary();
    vocabularyCompleter = new QCompleter(vocabulary,this);
    vocabularyCompleter->setCaseSensitivity(Qt::CaseInsensitive);
    vocabularyCompleter->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
    vocabularyCompleter->setWrapAround(false);
    vocabularyCompleter->setCompletionMode(QCompleter::InlineCompletion);
    ui->word->setCompleter(vocabularyCompleter);
    ui->vocabulary->clear();
    ui->vocabulary->addItems(vocabulary);
}

QStringList DB_manager::getVocabulary(){
    QList < QStringList > result = db->readValue("slovnik",QStringList() << "base");
    QStringList vocab;
    for(int i = 0; i < result.size(); i++){
        for(int j = 0; j < result.at(j).size(); j++){
            vocab.append(result.at(i).at(j));
        }
    }
    return vocab;
}

void DB_manager::addWordToSentence(){
    ui->currentSentence->addItem(ui->word->text());
    ui->word->setText("");
    ui->word->selectAll();
}


void DB_manager::on_vocabulary_itemDoubleClicked(QListWidgetItem *item)
{
    ui->currentSentence->addItem(item->text());
}

void DB_manager::on_word_cursorPositionChanged(int arg1, int arg2)
{
    if(arg2 != 0){
        ui->vocabulary->clear();
        QString prefix;
        QStringList word = ui->word->text().split("");
        for(int i = 0; i < word.size() && i < arg2 ; i++){
            prefix.append(word[i]);
        }
        for(int i = 0; i < vocabulary.size() ;i++){
            if(QString(vocabulary.at(i)).contains(prefix)){
                ui->vocabulary->addItem(vocabulary.at(i));
            }
        }
    } else {
        ui->vocabulary->clear();
        ui->vocabulary->addItems(vocabulary);
    }
}

void DB_manager::on_currentSentence_itemClicked(QListWidgetItem *item)
{
    ui->newWord->setText(item->text());
    QList < QStringList > list = db->readValue("slovnik",QStringList() <<"variation","base",item->text());
    QString variation = list.first().first();
    ui->wordVariation->setText(variation);
}

void DB_manager::on_saveWord_clicked()
{
    if(!db->readRow("slovnik","base",ui->newWord->text().trimmed()).isEmpty()){
        if(db->updateRow("slovnik","variation",ui->wordVariation->text().trimmed(),"base",ui->newWord->text().trimmed()))
            ui->statusBar->showMessage("Úspěšne akualizovano ve slovníku",4000);
        else
            ui->statusBar->showMessage("Nepovedlo se uložit");
    } else {
        if(db->insertRow("slovnik",QStringList() << "base" << "variation",QStringList() << ui->newWord->text().trimmed() << ui->wordVariation->text().trimmed()))
            ui->statusBar->showMessage("Uspěšně vloženo do slovníku");
        else
            ui->statusBar->showMessage("Nepovedlo se vložit do slovníku");
    }
    refreshVocabulary();
}

void DB_manager::on_vocabulary_itemClicked(QListWidgetItem *item)
{
    on_currentSentence_itemClicked(item);
}

void DB_manager::on_currentSentence_itemDoubleClicked(QListWidgetItem *item)
{
    delete item;
}

void DB_manager::makeStatesSequence(QStringList sentence){
    int state = 1;

    for(int i = 0; i < sentence.size(); i++){
        qWarning() << "Processing word: " << sentence.at(i);
        QString next_state = QString::number(getAvailableState());
        QString end = "0";
        if(i == sentence.size()-1){
            next_state = ui->endState->text();
            end = "1";
        }
        qWarning() << "------------Max new state:" << next_state;
        QPair<QString,QString> s;
        s.first = "state";
        s.second = QString::number(state);
        QPair<QString,QString> name;
        name.first = "name";
        name.second = sentence.at(i);

        QList<QPair<QString,QString>> cond;
        cond << s << name;
        QList<QStringList> commands = db->readValue("fsm",QStringList("next_state"),cond);

        if(commands.isEmpty()){
            if(db->insertRow("fsm",QStringList()<<"state"<<"name"<<"commands"<<"next_state"<<"end_state",QStringList()<< QString::number(state)<<sentence.at(i) <<"&#&" << next_state<<end)){
                ui->statusBar->showMessage("Problem with insering state");
                return;
            }
            state = next_state.toInt();
        } else {
            state = QString(commands.first().first()).toInt();
        }
    ui->statusBar->showMessage("OK");
    }
}

int DB_manager::getAvailableState(){
    QList<QStringList> states = db->readValue("fsm",QStringList("state"));
    int max = 0;
    for(int i = 0; i < states.size(); i++){
        if(max < QString(states.at(i).first()).toInt()){
            max = QString(states.at(i).first()).toInt();
        }
    }
    return ++max;
}

void DB_manager::on_actionRefrash_vocabulary_triggered()
{
    refreshVocabulary();
}

void DB_manager::on_pushButton_3_clicked()
{
    QStringList list;
    for (int i = 0; i < ui->currentSentence->count(); ++i) {
        list.append(ui->currentSentence->item(i)->text());
    }
    makeStatesSequence(list);
}

void DB_manager::on_sensors_refresh_clicked()
{
    ui->sensors->clear();
    ui->sensors->addItems(readSensorsList());;
}

QStringList DB_manager::readSensorsList(){
    QList < QStringList > res = db->readValue("sensors",QStringList()<< "uuid" << "name" << "type");
    QStringList devices;
    for(int i = 0; i < res.size(); i++){
        devices << res.at(i).at(1) + "|" + res.at(i).at(0) + "|" + res.at(i).at(2);
    }
    return devices;
}

void DB_manager::on_sensors_currentIndexChanged(const QString &arg1)
{
    if(arg1.split("|").size() == 3){
        ui->sensor_name->setText(arg1.split("|")[0]);
        ui->sensor_uuid->setText(arg1.split("|")[1]);
        ui->sensorType->setText(arg1.split("|")[2]);
    }
}

void DB_manager::on_sensor_submit_clicked()
{
    QString err = "Cant update ";
    QString ref = err;
    if(!db->updateRow("sensors","name",ui->sensor_name->text(),"uuid",ui->sensor_uuid->text()))
        err = err + ui->sensor_name->text();
    on_sensors_refresh_clicked();
}

//NEW Sensor
void DB_manager::on_pushButton_4_clicked()
{
    bool ok;
    QString text = QInputDialog::getText(this, tr("New sensor"),
                                        tr("uuid:"), QLineEdit::Normal,
                                        "", &ok);
    if (ok && !text.isEmpty()){
        db->insertRow("sensors",QStringList() << "uuid" << "name",QStringList() <<text << "");
    }
    on_sensors_refresh_clicked();
}

void DB_manager::on_sensor_remove_clicked()
{
    QString err = "Cant remove ";
    if(!db->removeRow("sensors","uuid",ui->sensor_uuid->text()))
        err = err +ui->sensor_uuid->text();
    on_sensors_refresh_clicked();
}
