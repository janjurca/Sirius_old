#ifndef DB_MANAGER_H
#define DB_MANAGER_H

#include <QMainWindow>
#include "databasehandling.h"
#include <QListWidgetItem>
#include <QCompleter>

namespace Ui {
class DB_manager;
}

class DB_manager : public QMainWindow
{
    Q_OBJECT

public:
    explicit DB_manager(QWidget *parent = 0);
    ~DB_manager();

private:
    Ui::DB_manager *ui;
    DatabaseHandling *db = NULL;
    QCompleter *vocabularyCompleter = NULL;
    QStringList vocabulary;
private slots:
    int getAvailableState();
    void makeStatesSequence(QStringList sentence);
    void addWordToSentence();
    void refreshVocabulary();
    QStringList getVocabulary();
    /**
     * Return word of sentence in that the cursor is.
     * @brief selectedWord
     * @param sentence
     * @param cursorPosition
     * @return
     */
    QString selectedWord(QString sentence, int cursorPosition);

    /**
     * Read the info about device from DB and set all labels... to proper value
     * @brief readDevice
     * @param id id of things
     */
    void readDevice(QString id);

    /**
     * Write current setting of comboboxes,lineEdits... to DB
     * @brief writeDevice
     */
    void writeDevice();

    /**
     * Read the name and id of all devices from DB and return their list
     * @brief readDeviceList
     * @return
     */
    QStringList readDeviceList();

    /**
     * Read the name and id of all sensors from DB and return their list
     * @brief readSensorsList
     * @return
     */
    QStringList readSensorsList();

    void on_device_select_currentIndexChanged(const QString &arg1);
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_refresh_clicked();
    void on_newItem_clicked();
    void on_availableStates_itemDoubleClicked(QListWidgetItem *item);
    void on_vocabulary_itemDoubleClicked(QListWidgetItem *item);
    void on_word_cursorPositionChanged(int arg1, int arg2);
    void on_currentSentence_itemClicked(QListWidgetItem *item);
    void on_saveWord_clicked();
    void on_vocabulary_itemClicked(QListWidgetItem *item);
    void on_currentSentence_itemDoubleClicked(QListWidgetItem *item);
    void on_actionRefrash_vocabulary_triggered();
    void on_pushButton_3_clicked();
    void on_sensors_refresh_clicked();
    void on_sensors_currentIndexChanged(const QString &arg1);
    void on_sensor_submit_clicked();
    void on_pushButton_4_clicked();
    void on_sensor_remove_clicked();
};

#endif // DB_MANAGER_H
