#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtWidgets>
#include <QtNetwork>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    udpSocket = new QUdpSocket(this);
    udpSocket->bind(ui->inputPortSet->value(), QUdpSocket::ShareAddress);
    connect(udpSocket, SIGNAL(readyRead()),this, SLOT(processPendingDatagrams()));
    connect(ui->lineEdit, SIGNAL(returnPressed()),ui->pushButton,SIGNAL(clicked()));
    setWindowTitle(tr("SiriusCom"));
    player = new QMediaPlayer();
    player->setMedia(QUrl("qrc:/sound.mp3"));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{

    QString vstup = ui->lineEdit->text();
    ui->lineEdit->setText("");
    ui->odeslano->setText(vstup + "\n" + ui->odeslano->text());
    QByteArray datagram = vstup.toUtf8();
    udpSocket->writeDatagram(datagram.data(), datagram.size(),QHostAddress::Broadcast, ui->outputPortSet->value());


}

void MainWindow::processPendingDatagrams(){
     while (udpSocket->hasPendingDatagrams()) {
        QByteArray datagram;
        datagram.resize(udpSocket->pendingDatagramSize());
        udpSocket->readDatagram(datagram.data(), datagram.size());
        QString input_qt(datagram);
        qWarning() << input_qt;
        ui->prijato->setText(input_qt  + "\n" + ui->prijato->text());
        if(ui->sound->isChecked()){
            player->play();
        }
    }
}

void MainWindow::on_inputPortSet_valueChanged(int arg1)
{
    udpSocket->close();
    udpSocket->bind(arg1, QUdpSocket::ShareAddress);
}

