#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QObject>
#include <QtNetwork>
#include <QMediaPlayer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();
    void processPendingDatagrams();

    void on_inputPortSet_valueChanged(int arg1);

private:
    Ui::MainWindow *ui;
    QUdpSocket *udpSocket;
    QMediaPlayer *player;
};

#endif // MAINWINDOW_H
