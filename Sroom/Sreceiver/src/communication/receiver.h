class Receiver {
public:
    virtual bool available();
    virtual char* readString(char* target, int len);
    virtual void sleep();
    virtual void awake();
};
