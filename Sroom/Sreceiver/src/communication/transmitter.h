class Transmitter {
public:
    virtual bool send(const char*);
    virtual void sleep();
    virtual void awake();
    virtual void begin();
};
