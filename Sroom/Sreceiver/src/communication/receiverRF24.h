#include "receiver.h"
#include "RF24.h"

class ReceiverRF24 : public Receiver {
private:
    RF24* rf24;
    uint64_t addr;
    uint8_t ch;
public:
    ReceiverRF24(RF24* rf24,uint64_t addr = 0x53726f6f6dLL, uint8_t ch = 96);
    bool available();
    char* readString(char* target,int len);
    void read(void * data, size_t len);
    void sleep();
    void awake();
    void begin();
};
