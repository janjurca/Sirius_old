#include "receiverRF24.h"
#include "RF24.h"

ReceiverRF24::ReceiverRF24(RF24* rf24,uint64_t addr, uint8_t ch): rf24(rf24),addr(addr),ch(ch){
}

void ReceiverRF24::begin(){
    if(!rf24->begin())
        Serial.println("ERROR while connecting rf24");
    else
        Serial.println("OK while connecting rf24");
    rf24->setPALevel(RF24_PA_MAX);
    rf24->openWritingPipe(addr);
    rf24->setDataRate(RF24_250KBPS);
    rf24->setChannel(ch);
    rf24->setAutoAck(1);                     // Ensure autoACK is enabled
    rf24->setRetries(2,15);                  // Optionally, increase the delay between retries & # of retries
    rf24->setCRCLength(RF24_CRC_8);          // Use 8-bit CRC for performance
    rf24->openReadingPipe(1,addr);
    rf24->startListening();
}

bool ReceiverRF24::available(){
    return this->rf24->available();
}

char* ReceiverRF24::readString(char* target, int len){
    this->rf24->read(target, len);
    return target;
}

void ReceiverRF24::read(void *data, size_t len){
  this->rf24->read(data, len);
}

void ReceiverRF24::sleep(){
    this->rf24->powerDown();
}

void ReceiverRF24::awake(){
    this->rf24->powerUp();
    delay(100);
}
