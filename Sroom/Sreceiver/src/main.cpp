#include "Arduino.h"
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include "communication/receiverRF24.h"
#include <SPI.h>
#include <Ethernet.h>

typedef struct payload{
  uint8_t uuid[16];
  float value;
  char value_type[12];
}payload_t;

byte mac[] = { 0xDE, 0xAE, 0xBE, 0xEF, 0xFE, 0xED };
char server[] = "192.168.1.11";
// Set the static IP address to use if the DHCP fails to assign
IPAddress ip(192, 168, 1, 45);
EthernetClient client;

RF24 radio(8,9);
ReceiverRF24 receiver(&radio);

char* uuidToChar(uint8_t source[16],char target[37]) {
  int index = 0;
  for (int i = 0; i < 16; i++) {
    if (i == 4 || i == 6 || i == 8 || i == 10){
        target[index++] = '-';
    }
    uint8_t topDigit = source[i] >> 4;
    uint8_t bottomDigit = source[i] & 0x0f;
    // Print high hex digit
    target[index++] = "0123456789ABCDEF"[topDigit];
    // Low hex digit
    target[index++] = "0123456789ABCDEF"[bottomDigit];

  }
  target[index] = '\0';
  return target;
}


void setup()
{
    Serial.begin(9600);
    receiver.begin();
    if (Ethernet.begin(mac) == 0) {
     Serial.println("Failed to configure Ethernet using DHCP, using fixed ip");
     Ethernet.begin(mac, ip);
  }
  // give the Ethernet shield a second to initialize:
  Serial.println("connecting...");
  delay(1000);



}

void loop()
{
    while (receiver.available()) {
        payload_t payload;
        receiver.read(&payload, sizeof(payload_t));
        //{"type":"telemetry","uuid":"0cd71471-8126-4c8a-bbbc-9002ffbe66d2","value":"24.4","value_type":"temperature"}
        char uuid_h[37] = {0};
        uuidToChar(payload.uuid, uuid_h);
        if (strcpy(uuid_h, "00000000-0000-0000-0000-000000000000") == 0) {
            break;
        }
        Serial.print("{\"type\":\"telemetry\",");
        Serial.print("\"uuid\":\"");
        Serial.print(uuid_h);
        Serial.print("\",\"value\":\"");
        Serial.print(payload.value);
        Serial.print("\",\"value_type\":\"");
        Serial.print(payload.value_type);
        Serial.println("\"}");
        Serial.flush();

        if (client.connect(server, 80)) {
            Serial.println("connected to http server and sending query:");
            char uuid_h[37] = {0};
            uuidToChar(payload.uuid, uuid_h);
            // Make a HTTP request:
            client.print("GET ");
            client.print("/sirius/sensor.php?uuid=");
            client.print(uuid_h);
            client.print("&");
            client.print(payload.value_type);
            client.print("=");
            client.print(payload.value);
            client.println(" HTTP/1.1");
            client.println("Host: www.rowes.com");
            client.println("Connection: close");
            client.println();
            client.flush();
            client.stop();
        } else {
            // if you didn't get a connection to the server:
            Serial.println("connection failed");
        }
    }
}
