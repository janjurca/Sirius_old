#include "DHT.h"
#include "../sensor.h"
class SDHT: public Sensor, public DHT {
private:


public:
    SDHT (uint8_t pin, uint8_t type, uint8_t count=6): DHT(pin, type, count){

    }
    void read() override;
};
