#include "dht.h"

void SDHT::read(){
    if (action == 0) {
        return;
    }
    float value =  readHumidity();
    if (!isnan(value)) {
        action("humidity",value);
    }
    value = readTemperature();
    if (!isnan(value)) {
        action("temperature",value);
    }

}
