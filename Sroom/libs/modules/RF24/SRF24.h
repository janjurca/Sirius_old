#ifndef SRF24_H
#define SRF24_H value

#include "../transceiver.h"
#include "RF24.h"

class SRF24: public Transceiver, public RF24  {
private:


public:
    SRF24 (uint16_t _cepin, uint16_t _cspin): RF24(_cepin,_cspin){

    }
    bool sendString(const char* msg);
    bool send(void * data, size_t n);

    bool begin();
    void listenMode(uint64_t addr = 0x53726f6f6dLL, uint8_t ch = 96);
    void sendMode(uint64_t addr = 0x53726f6f6dLL, uint8_t ch = 96);
    bool available();
    char* read(char* target, int len);
    void sleep();
    void awake();
};

#endif
