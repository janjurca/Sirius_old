class Transmitter {
public:
    virtual bool sendString(const char*){};
    virtual void sleep(){};
    virtual void awake(){};
    virtual bool begin(){};
};
