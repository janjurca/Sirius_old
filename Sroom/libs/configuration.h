#ifndef CONFIGURATION_H
#define CONFIGURATION_H value

#include "highprom.h"
#include "TrueRandom.h"

class Configuration {
private:
    char m_uuid[37] = {0};
    char m_bin_uuid[16] = {0};
    Highprom prom = Highprom(1000);

public:
    Configuration (){};
    void begin();
    void erase(){
        prom.init();
    }
    char* uuid(){return m_uuid; };
    char* bin_uuid(){return m_bin_uuid; };

private:
    void uuidSetup();
    char* uuidToChar(uint8_t source[16],char target[37]);
};

#endif
