#ifndef DEFINES
#define DEFINES 1

#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
#define MAX_PIN_NUMBER 13
#endif
#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
#define MAX_PIN_NUMBER 49
#endif
//#define DEBUG

#endif
