#include "Arduino.h"
#include "UniversalController.h"
#include "TrueRandom.h"
#include "highprom.h"

UniversalController u_controller;
char uuid[37];
Highprom prom(100);


char* uuidToChar(uint8_t source[16],char target[37]) {
  int index = 0;
  for (int i = 0; i < 16; i++) {
    if (i == 4 || i == 6 || i == 8 || i == 10){
        target[index++] = '-';
    }
    uint8_t topDigit = source[i] >> 4;
    uint8_t bottomDigit = source[i] & 0x0f;
    // Print high hex digit
    target[index++] = "0123456789ABCDEF"[topDigit];
    // Low hex digit
    target[index++] = "0123456789ABCDEF"[bottomDigit];

  }
  target[index] = '\0';
  return target;
}

void uuidSetup(){
    //UUID INITITALIZATION
    if (prom.indexOf("UUID") == -1) {
        unsigned char hex_id[16];
        TrueRandom.uuid(hex_id);
        uuidToChar(hex_id, uuid);
        prom.insertValue("UUID", uuid);
    } else {
        prom.getValue("UUID", uuid, 37);
    }
}

void print(const char *c){
    Serial.print(c);
}

void setup(void) {
    Serial.begin(9600);
    uuidSetup();
    u_controller.setPrint(print);
    //Notify about connection
    Serial.print("{\"type\":\"connectedController\",\"controller\": \"");
    Serial.print(uuid);
    Serial.print("\"}");

    Serial.setTimeout(10);
}

void loop(void) {
    if (Serial.available()) {
        char buff[150] = {0};
        Serial.readBytes( buff, 150);
        u_controller.parseJSON(buff);
    }

}
