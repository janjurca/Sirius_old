#include "pinnotifregister.h"

PinNotifRegister::PinNotifRegister(){
  for (size_t i = 0; i <= MAX_PIN_NUMBER; i++) {
    this->map[i].timeout = 0;
    this->map[i].value = 0;
    this->map[i].ad = 0;
    this->map[i].listen = false;
    this->map[i].timestamp = 0;
  }
}

void PinNotifRegister::registerPin(int pin, int value,int ad, int timeout,bool analogCheckType){
  this->map[pin].value = value;
  this->map[pin].ad = ad;
  this->map[pin].listen = true;
  this->map[pin].timeout = timeout;
}

void PinNotifRegister::update(){
  for (size_t i = 0; i < MAX_PIN_NUMBER; i++) {
    if (this->map[i].listen) {
      if ((int)(millis() - map[i].timestamp) > map[i].timeout) { //is over timeout
        if (map[i].ad == 0) { // go digitaly
          if (digitalRead(i) == map[i].value) {
            notify(i);
          }
        } else if (map[i].ad == 1) {
          int value = analogRead(i + MAX_PIN_NUMBER);
          if (map[i].analogCheckType && value > map[i].value) {
            notify(i);
          } else if (!(map[i].analogCheckType) && value < map[i].value) {
            notify(i);
          }
        }
      }
    }
  }
}

void PinNotifRegister::notify(int pin){
  map[pin].timestamp = millis();
  //char msg[6] = {0};
  //snprintf(msg,6, "|N|%d",pin);
  //Serial.print(msg);
  Serial.print("|N|");
  Serial.println(pin);
}

void PinNotifRegister::unregisterPin(int pin){
  map[pin].listen = false;
}
