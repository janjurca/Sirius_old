#ifndef PIN_NOTIF_REGISTER_H
#define PIN_NOTIF_REGISTER_H 1

#include "Arduino.h"
#include "defines.h"

class PinNotifRegister{
public:
  PinNotifRegister();
  /**
   * register pin to map and on every update check this pin if does not pass the selection
   * @param pin   number of pin
   * @param value value to be listen for
   * @param ad    read digitaly=0 or analog=1
   * @param timeout ignore the signal for this time from the last
   * @param analogCheckType represents the type of listening true=listening_for_above| false=listening_for_belov
   */
  void registerPin(int pin, int value,int ad = 0, int timeout = 1000,bool analogCheckType = false);

  /**
   * Unset listener for desired pin
   * @param pin pin number
   */
  void unregisterPin(int pin);

  /**
   * print message to serial port as notification
   * @param pin pin that is callbacked
   */
  void notify(int pin);
  void update();
private:
  typedef struct pin{
    bool listen;
    int value;
    int ad;
    int timeout;
    int timestamp;
    bool analogCheckType; // below or above value
  }pin;
  pin map[MAX_PIN_NUMBER];
};

#endif
