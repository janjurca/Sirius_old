#!/bin/bash
stty -F /dev/ttyS3 9600 raw -echo -echoe -echok -echoctl -echoke

socat - udp4-listen:4547,reuseaddr,fork | tee  /dev/ttyS3 &

cat /dev/ttyS3 | socat - UDP-DATAGRAM:255.255.255.255:4548,broadcast
