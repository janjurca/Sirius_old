from socket import *
import serial
import argparse
import threading
import time



def serial_to_udp():
    with serial.Serial(args.serial, args.baud) as ser:
        while True:
            line = ser.readline()   # read a '\n' terminated line
            print("Sending:",line)
            cs.sendto(line, ('255.255.255.255', args.port))

def udp_to_serial():
    sock.bind(("", args.port))
    while True:
       data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
       print "received message:", data



if __name__ == '__main__':
    example_usage = '''
        Connect the udp broadcast to serial port.
    '''
    parser = argparse.ArgumentParser(example_usage)
    parser.add_argument('-p', '--port', action='store', type=int, help='Port to broadcast on', default=4547)
    parser.add_argument('-b', '--baud', action='store', type=int, help='Baudrate', default=9600)
    parser.add_argument('-s', '--serial', action='store', type=str, help='serial port to listen on', required=True)
    parser.add_argument('-l', '--list', action='store_true', help='Just list available ports', default=False)
    args = parser.parse_args()

    if args.list:
        import serial.tools.list_ports
        comlist = serial.tools.list_ports.comports()
        connected = []
        for element in comlist:
            connected.append(element.device)
        print("Connected COM ports: " ,connected)
        exit(0)

    cs = socket(AF_INET, SOCK_DGRAM)
    cs.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    cs.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)

    threading.Thread(target=serial_to_udp).start()
    threading.Thread(target=udp_to_serial).start()
