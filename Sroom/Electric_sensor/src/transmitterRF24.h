#ifndef TRANSMITTERRF24_H
#define TRANSMITTERRF24_H value

#include "transmitter.h"
#include "RF24.h"

class TransmitterRF24 : public Transmitter {
private:
    RF24* rf24;
    uint64_t addr;
    uint8_t ch;
public:
    TransmitterRF24(RF24* rf24,uint64_t addr = 0x53726f6f6dLL, uint8_t ch = 96);
    bool send(const char* msg);
    bool sendData(const void* msg, size_t n);
    void sleep();
    void awake();
    bool begin();
};

#endif
