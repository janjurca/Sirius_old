#ifndef TRANSMITTER_H
#define TRANSMITTER_H value

class Transmitter {
public:
    virtual bool send(const char*);
    virtual void sleep();
    virtual void awake();
    virtual bool begin();
};

#endif
