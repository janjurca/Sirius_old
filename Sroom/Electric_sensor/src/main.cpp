#include "Arduino.h"
#include "SPI.h"
#include "TrueRandom.h"
#include "configuration.h"
#include "LowPower.h"
#include "RF24.h"
#include "transmitter.h"
#include "transmitterRF24.h"


typedef struct payload{
  uint8_t uuid[16];
  float value;
  char value_type[12];
}payload_t;

RF24 rf(7, 8);
TransmitterRF24 rf24 = TransmitterRF24(&rf);

Configuration config;
//Support for five sensors
int impulseCounter = 0;

void print(const char *c){
    Serial.print(c);
}
void sleep(){
    Serial.flush();
    rf24.sleep();
    LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
}


void impulseCatched(){
    digitalWrite(13, HIGH);
    impulseCounter++;
    Serial.println("INC");
    delay(10);
    digitalWrite(13, LOW);
    sleep();
}


void process(){
    digitalWrite(13, HIGH);
    Serial.println("impulseCounter: ");
    float kwh = impulseCounter/500.0;
    Serial.println(kwh);
    impulseCounter = 0;
    payload_t msg;
    msg.uuid[0] = 1;
    msg.uuid[2] = 2;

    msg.value = kwh;
    strncpy(msg.value_type, "electricity", 12);
    rf24.awake();
    rf.writeFast(&msg,32);
    Serial.println("AHOJ");
    void (*f)() = 0;
    Serial.flush();
    rf24.sleep();
    
    f();
}

void setup(void) {
    Serial.begin(9600);
    config.begin();

    if(rf24.begin()){
        Serial.println("RF OK");
    } else {
        Serial.println("RF fail");
    }
    pinMode(13, OUTPUT);
    pinMode(3, INPUT);
    pinMode(2, INPUT_PULLUP);
    //Notify about connection

    attachInterrupt(digitalPinToInterrupt(3), impulseCatched, FALLING);
    attachInterrupt(digitalPinToInterrupt(2), process, FALLING);
    sleep();
}


void loop(void) {
    sleep();
}
