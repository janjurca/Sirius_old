#include "transmitterRF24.h"
#include "string.h"

TransmitterRF24::TransmitterRF24(RF24 * rf24, uint64_t addr, uint8_t ch) : rf24(rf24), addr(addr),ch(ch) {
}

bool TransmitterRF24::begin(){
    if (rf24->begin()) {
        rf24->setPALevel(RF24_PA_MIN);
        rf24->openWritingPipe(addr);
        rf24->setDataRate(RF24_250KBPS);
        rf24->setChannel(ch);
        rf24->setAutoAck(1);                     // Ensure autoACK is enabled
        rf24->setRetries(2,2);                  // Optionally, increase the delay between retries & # of retries
        rf24->setCRCLength(RF24_CRC_8);          // Use 8-bit CRC for performance
        return true;
    } else {
        return false;
    }
}

bool TransmitterRF24::send(const char *msg){
    return this->rf24->write(msg, strlen(msg));
}

void TransmitterRF24::awake(){
    this->rf24->powerUp();
    delay(100);
    this->begin();
}

void TransmitterRF24::sleep(){
    this->rf24->flush_tx();
    rf24->powerDown();
}

bool TransmitterRF24::sendData(const void* msg, size_t n){
    return this->rf24->write(msg, n);
}
