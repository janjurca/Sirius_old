#include "Arduino.h"
#include "UniversalController.h"
#include "TrueRandom.h"
#include "libs/modules/sensor.h"
#include "libs/modules/DHT/dht.h"
#include "configuration.h"

Configuration config;
//Support for five sensors
Sensor s[] = {
    Sensor(),Sensor(),Sensor(),Sensor(),Sensor()
};
UniversalController u_controller;

void print(const char *c){
    Serial.print(c);
}

void setup(void) {
    Serial.begin(9600);
    config.begin();
    u_controller.setPrint(print);
    //Notify about connection
    Serial.print("{\"type\":\"connectedController\",\"controller\": \"");
    Serial.print(config.uuid());
    Serial.print("\"}");

    Serial.setTimeout(10);
}

void loop(void) {
    if (Serial.available()) {
        char buff[150] = {0};
        Serial.readBytes( buff, 150);
        u_controller.parseJSON(buff);
    }

}
