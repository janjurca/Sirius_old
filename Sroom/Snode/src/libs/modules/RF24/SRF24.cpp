#include "SRF24.h"

bool SRF24::begin(){
    return RF24::begin();
}

void SRF24::listenMode(uint64_t addr , uint8_t ch ){
    setPALevel(RF24_PA_MAX);
    openWritingPipe(addr);
    setDataRate(RF24_250KBPS);
    setChannel(ch);
    setAutoAck(1);                     // Ensure autoACK is enabled
    setRetries(2,15);                  // Optionally, increase the delay between retries & # of retries
    setCRCLength(RF24_CRC_8);          // Use 8-bit CRC for performance
    openReadingPipe(1,addr);
    startListening();
}

void SRF24::sendMode(uint64_t addr , uint8_t ch ){
    setPALevel(RF24_PA_MAX);
    openWritingPipe(addr);
    setDataRate(RF24_250KBPS);
    setChannel(ch);
    setAutoAck(1);                     // Ensure autoACK is enabled
    setRetries(2,15);                  // Optionally, increase the delay between retries & # of retries
    setCRCLength(RF24_CRC_8);          // Use 8-bit CRC for performance
}


bool SRF24::available(){
    return available();
}

char* SRF24::read(char* target, int len){
    read(target, len);
    return target;
}

void SRF24::sleep(){
    powerDown();
}

void SRF24::awake(){
    powerUp();
    delay(100);
}
