#ifndef SENSOR_H
#define SENSOR_H value
class Sensor {
protected:
    void (*action)(const char*,float ) = 0;
    void (*interupted)() = 0;


public:
    Sensor (){};
    virtual void read(){};
    void onRead(void (*action)(const char*,float)){
        this->action = action;
    }

    /**
     * This function is called on sensors that needs interuptions to its proper work, such as waterflow sensor,
     * it is basicaly for some supporting functions such as sleep after reading and so on because the interuption should awake proccesor
     */
    virtual void onInterrupted(void (*interupted)()){
        this->interupted = interupted;
    }

};

#endif
