class Receiver {
public:
    virtual bool available();
    virtual char* read(char* target, int len);
    virtual void sleep();
    virtual void awake();
    virtual bool begin();
};
