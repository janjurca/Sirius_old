#ifndef WATERFLOW_H
#define WATERFLOW_H value

#include "../sensor.h"
#include "Arduino.h"

class SWaterflow : public Sensor {
private:
    float calibrationFactor = 4.5;
    float pulse_count = 0;
public:
    SWaterflow (int8_t pin, float calibrationFactor = 4.5);
    virtual ~SWaterflow ();
};

#endif
