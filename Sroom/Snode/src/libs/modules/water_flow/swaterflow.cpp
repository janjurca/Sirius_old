#include "swaterflow.h"

SWaterflow::SWaterflow (int8_t pin, float calibrationFactor): calibrationFactor(calibrationFactor){
    pinMode(pin,INPUT_PULLUP);
    digitalWrite(pin, HIGH);
    // The Hall-effect sensor is connected to pin  which uses interrupt.
    // Configured to trigger on a FALLING state change (transition from HIGH
    // state to LOW state)
    attachInterrupt(pin, [=](){
        if (interupted != 0) {
            interupted();
        }
        pulse_count++;
    }, FALLING);

};


void SWaterflow::read(){
    if (action == 0) {
        return;
    }

    static int old_time = 0;
    int new_time = micros();
    int delay = fabs(new_time-old_time);

    double flowRate = ((1000.0 / (delay) * pulseCount) / calibrationFactor;
    oldTime = micros();

    // Divide the flow rate in litres/minute by 60 to determine how many litres have
    // passed through the sensor in this 1 second interval, then multiply by 1000 to
    // convert to millilitres.
    flowMilliLitres = (flowRate / 60) * 1000;

    // Add the millilitres passed in this second to the cumulative total
    totalMilliLitres += flowMilliLitres;

    unsigned int frac;

    // Print the flow rate for this second in litres / minute
    Serial.print("Flow rate: ");
    Serial.print(int(flowRate));  // Print the integer part of the variable
    Serial.print("L/min");
    Serial.print("\t"); 		  // Print tab space

    // Print the cumulative total of litres flowed since starting
    Serial.print("Output Liquid Quantity: ");
    Serial.print(totalMilliLitres);
    Serial.println("mL");
    Serial.print("\t"); 		  // Print tab space
   Serial.print(totalMilliLitres/1000);
   Serial.print("L");


    // Reset the pulse counter so we can start incrementing again
    pulseCount = 0;


    action("waterflow",litres_flowed);
    litres_flowed = 0;
}
