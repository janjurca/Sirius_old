#include "UniversalController.h"
#include "configuration.h"

extern Configuration config;
//#define DEBUG
void UniversalController::parseJSON(char *json){
    StaticJsonBuffer<400> jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(json);

    // Test if parsing succeeds.
    if (!root.success()) {
        print("{\"type\":\"error\",\"text\":\"Cannot parse msg\"}\n");
        return;
    }
    const char * controller_uid = root["controller"];
    if (strcmp(config.uuid(), controller_uid) != 0) {
        return;
    }

    const char* type = root["type"];
    if (strcmp(type, "deviceSet") == 0) {
        int pin = atoi(root["pin"]);
        const char* mode = root["mode"];
        const char* role = root["role"];
        int value = atoi(root["value"]);
        setPin(pin, mode, role, value);
    } else if (strcmp(type, "deviceRead") == 0) {
        const char* pin = root["pin"];
        const char* mode = root["mode"];
        int value = readPin(atoi(pin), mode);
        char buff[150] = {};
        snprintf(buff, 150, "{\"controller\":\"%s\",\"type\":\"deviceReaded\",\"pin\":\"%s\",\"mode\":\"%s\",\"value\":\"%d\"}\n",config.uuid(),pin,mode,value);
        print(buff);
    }


}

int UniversalController::readPin(int pin, const char *mode){

#ifdef DEBUG
    Serial.print("pin: ");
    Serial.println(pin);
    Serial.print("mode: ");
    Serial.println(mode);
#endif
    if (strcmp(mode,"DIGITAL") == 0) {
      return digitalRead(pin);
    } else {
      return analogRead(pin + MAX_PIN_NUMBER);
    }
}

void UniversalController::setPin(int pin, const char *mode, const char *role, int value){

#ifdef DEBUG
    Serial.print("pin: ");
    Serial.println(pin);
    Serial.print("mode: ");
    Serial.println(mode);
    Serial.print("role: ");
    Serial.println(role);
    Serial.print("value: ");
    Serial.println(value);
#endif
    if (strcmp(role,"OUTPUT") == 0) { // normal output role
      pinMode(pin,OUTPUT);
  } else if(strcmp(role,"INPUT") == 0){ // input role
      pinMode(pin, INPUT);
  } else if (strcmp(role,"INPUT_PULLUP") == 0) { // pullup input mode
      pinMode(pin, INPUT_PULLUP);
    }

    if(strcmp(mode, "DIGITAL") == 0 ){ // digitalWrite
      if (value == 0) {
        digitalWrite(pin,LOW);
      } else {
        digitalWrite(pin, HIGH);
      }
    } else { // analog write part
      analogWrite(pin, value);
    }
}
