#include <Arduino.h>
#include "LowPower.h"

void setup() {
    pinMode(3,OUTPUT);
    digitalWrite(3,HIGH);
}

void loop() {
    static unsigned counter = 0;
    if (counter > 225) {
        counter = 0;
        digitalWrite(3, LOW);
        delay(100);
        digitalWrite(3, HIGH);
    }
    counter++;
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
}
