#include "configuration.h"

void Configuration::begin(){
    uuidSetup();
}

void Configuration::uuidSetup(){
    m_uuid = "3C632364-FECC-4C8E-9B38-FC35C6D4E680";
    return;
    //UUID INITITALIZATION
    Serial.print("Index");
    Serial.println(prom.indexOf("UUID"));
    if (prom.indexOf("UUID") == -1) {
        unsigned char hex_id[16];
        ESP8266TrueRandom.uuid(hex_id);
        uuidToChar(hex_id, m_uuid);
        prom.insertValue("UUID", m_uuid);
    } else {
        prom.getValue("UUID", m_uuid, 37);
    }
}


char* Configuration::uuidToChar(uint8_t source[16],char target[37]) {
  int index = 0;
  for (int i = 0; i < 16; i++) {
    if (i == 4 || i == 6 || i == 8 || i == 10){
        target[index++] = '-';
    }
    uint8_t topDigit = source[i] >> 4;
    uint8_t bottomDigit = source[i] & 0x0f;
    // Print high hex digit
    target[index++] = "0123456789ABCDEF"[topDigit];
    // Low hex digit
    target[index++] = "0123456789ABCDEF"[bottomDigit];

  }
  target[index] = '\0';
  return target;
}
