#ifndef UniversalController_H
#define UniversalController_H value
#include <string.h>
#include "Arduino.h"
#include <ArduinoJson.h>



#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
#define MAX_PIN_NUMBER 13
#elif defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
#define MAX_PIN_NUMBER 49
#else
#define MAX_PIN_NUMBER 9
#endif

class UniversalController {
private:
    void (*print)(const char*);

public:
    UniversalController (){};

    /**
     * Function that parse command json and make all needed work
    Sample messages are here:
    https://gitlab.com/kohoutovice/Sirius/tree/master/jsons_templates
     * @param json json text
     */
    void parseJSON(char* json);

    /**
     * Read digital value of pin and return its value
     * @param  pin pin number
     * @param  mode pin mode such as digital/analog
     * @return     1 == HIGH | 0 == LOW | value ==  analog
     */
    int readPin(int pin, const char* mode);

    /**
     * Set pin state
     * @param pin   pin number
     * @param mode  pin mode such as digital/analog
     * @param role  output/input/input_pullup
     * @param value value to write
     */
    void setPin(int pin, const char* mode, const char* role,int value);

    /**
     * Function that should be called in loop to handle registered notification pins
     */
    void update();

    /**
     * Set the method that is called when someting is printed inside of controller, so some controll/error massages and so on.
     * @param f Pointer to function that gets the string and do whatever you want with it, most common will be probably to print it somewhere (serial,nrfl,ethrnet...)
     */
    void setPrint(void (*f)(const char *)){
        print = f;
    };

};

#endif
