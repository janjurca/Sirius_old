#ifndef CONFIGURATION_H
#define CONFIGURATION_H value

#include "highprom.h"
#include "ESP8266TrueRandom.h"

class Configuration {
private:
    char *m_uuid = {0};
    Highprom prom = Highprom(1000);

public:
    Configuration (){};
    void begin();
    void erase(){
        prom.init();
    }
    char* uuid(){return m_uuid; };
private:
    void uuidSetup();
    char* uuidToChar(uint8_t source[16],char target[37]);
};

#endif
