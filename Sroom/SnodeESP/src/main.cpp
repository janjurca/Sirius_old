#include <SPI.h>
#include <Wire.h>
#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <stdio.h>
#include "UniversalController.h"
#include "configuration.h"
#define DEBUG


Configuration config;
UniversalController u_controller;

void UDPsend(const char *message);
WiFiUDP Udp;

#ifndef INGOING_ARDUINO_PORT
#define INGOING_ARDUINO_PORT  4547
#endif
#ifndef OUTGOING_ARDUINO_PORT
#define OUTGOING_ARDUINO_PORT  4548
#endif

const char* ssid = "JJ_2";
const char* password = "12345678900987654321abcdef";
void startWifi();
void sendPacket(const char *str,int port);
void OTA_set();

void setup() {
    pinMode(0, OUTPUT);
    digitalWrite(0, LOW);
  Serial.begin(9600);
  config.begin();
  u_controller.setPrint([](const char* m){sendPacket(m, 4547);});
  Udp.begin(INGOING_ARDUINO_PORT);
  startWifi();
  #ifdef DEBUG
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  #endif

  char buff[200] = {0};
  snprintf(buff, 200, "{\"type\":\"connectedController\",\"controller\": \"%s\"}",config.uuid());
  sendPacket(buff,4547);
  //spusteni Oty a zmena jmena
  OTA_set();
}

void loop() {
  ArduinoOTA.handle();
  if(!WiFi.isConnected()){
    startWifi();
  }
  int pktSize = Udp.parsePacket();


  if (pktSize) {
    char pktBuf[pktSize+1];
    for (size_t i = 0; i < pktSize+1; i++) {
      pktBuf[i] = 0;
    }
    /*Serial.print(Udp.remoteIP());
    Serial.print(":");
    Serial.println(Udp.remotePort());*/
    Udp.read(pktBuf, pktSize);
    Udp.stop();
    Udp.begin(INGOING_ARDUINO_PORT);
    Serial.println(pktBuf);
    u_controller.parseJSON(pktBuf);

  }

}

void startWifi(){
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
      Serial.println("Connection Failed! Rebooting...");
    delay(600000);
    ESP.restart();
  }
}


void sendPacket(const char *str,int port){
  #ifdef DEBUG
  Serial.print("UDPsend:");
  Serial.println(str);
  #endif
  Udp.beginPacketMulticast(IPAddress(192,168,1,255), port, WiFi.localIP());
  Udp.write(str);
  Udp.endPacket();
}

void UDPsend(const char *message){
  // send a reply, to the IP address and port that sent us the packet we received
  delay(1000);
  sendPacket(message, OUTGOING_ARDUINO_PORT);
  delay(1000);

}

void OTA_set(){
  // Port defaults to 8266
 // ArduinoOTA.setPort(8266);

 // Hostname defaults to esp8266-[ChipID]
 ArduinoOTA.setHostname("Sirius_node");

 // No authentication by default
 // ArduinoOTA.setPassword((const char *)"123");
 ArduinoOTA.begin();
}
