# Sirius
Tool for home automatisation, created on Qt Framework in c++ with client side application, serverside application and Arduino/ESP8244 sensor nodes or things controllers.

# Installation
## Build from source:
```
git clone https://gitlab.com/kohoutovice/Sirius.git
cd Sirius
qmake
make
```
This will build every part of Sirius, including client app, server, network console(Siriuscom) and Databse manager that is used mainly for development.


## From repository (repository is hosted on [Cloudsmith.io](https://cloudsmith.io)):
#### Debian/Ubuntu:
```
curl -sLf 'https://dl.cloudsmith.io/public/jan-jurca/sirius/cfg/install/bash.deb.sh' | sudo bash
apt install siriusserver
```
Actualy is packaged just server side application and supported is just x86_64. I hope it will change soon.
#### Fedora/RHEL/CentOS:
```
curl -sLf 'https://dl.cloudsmith.io/public/jan-jurca/sirius/cfg/install/bash.rpm.sh' | sudo bash
dnf install siriusserver || yum install siriusserver
```
Actualy is packaged just server side application and we support just x86_64. Hope it will change soon.

## Docker:
Docker image with sirius server is available for x86_64 and armv7 architecture. In future I hope I will provide docker-compose file to set the database too, but now the database must be installed manualy by you.
#### x86_64:
```
docker run -it registry.gitlab.com/kohoutovice/sirius:x86_64
```
#### armv7:
```
docker run -it registry.gitlab.com/kohoutovice/sirius:armv7
```

# Usage
For correct usage there must be installed mysql server. So please install it:
##### Debian/Ubuntu guide: https://www.itzgeek.com/how-tos/linux/ubuntu-how-tos/install-mariadb-on-ubuntu-16-04.html
##### Fedora guide: https://fedoraproject.org/wiki/MariaDB

After database is installed there must be set server in sirius configuration and the database schema must be prepared. I now preparing mechanism that make it easy for user. So please wait for it. Or if you are eager to do that on your own then follow next lines.

- Create db user that you want to use for sirius and under this user apply db scheme from root repo folder ```schema.sql```
- Then in ```~/.Sirius/sirius.json``` file create basic configuration:
```
{
    "DBAdress": "ip-address-or-domain-name",
    "DBUsername": "sirius",
    "DBPassword": "Password"
}
```
- Now you should be able to run siriusserver. Expected output is something like:
```
Remote DB connected
```

Now you can use full power of siriusserver. In next few months I will try to writedown some docs and how to guides.
